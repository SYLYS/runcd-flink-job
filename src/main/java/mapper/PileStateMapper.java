package mapper;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.thread.ThreadUtil;
import com.google.common.cache.*;
import dto.*;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;

import java.sql.*;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class PileStateMapper extends RichAsyncFunction<PileStateDTO, PileStateDTO> {
    private transient Connection connection;
    private transient LoadingCache<String, OperatorOds> operatorOds;
    private transient LoadingCache<Integer, StationOds> stationOds;
    private transient LoadingCache<String, PileOds> pileods;
    private transient LoadingCache<String, GunOds> gunOds;

    @Override
    public void asyncInvoke(PileStateDTO pileStateDTO, ResultFuture<PileStateDTO> resultFuture) {

        final Future<PileStateDTO> result = ThreadUtil.execAsync(() -> wrapperPileStateDTO(pileStateDTO));

        CompletableFuture.supplyAsync(() -> {
            try {
                return result.get();
            } catch (Exception e) {
                try {
                    initConnection("runcd_mysql");
                } catch (ClassNotFoundException | SQLException ex) {
                    throw new RuntimeException(ex);
                }
                return new PileStateDTO();
            }
        }).thenAccept((PileStateDTO p) -> resultFuture.complete(Collections.singleton(p)));
    }

    private PileStateDTO wrapperPileStateDTO(PileStateDTO pileStateDTO) throws ExecutionException {
        GunOds gunOds = new GunOds();
        StationOds stationOds = new StationOds();
        OperatorOds operatorOds = new OperatorOds();
        PileOds pileOds = this.pileods.get(pileStateDTO.getPileCode());
        if (pileStateDTO.getGunCode() != null) {
            gunOds = this.gunOds.get(pileStateDTO.getGunCode());
        }
        if (pileOds != null) {
            if (pileOds.getStationId() != null) {
                stationOds = this.stationOds.get(pileOds.getStationId());
            }
            if (pileOds.getOperatorId() != null) {
                operatorOds = this.operatorOds.get(pileOds.getOperatorId());
            }
        }

        return PileStateDTO.builder()
                .operatorId(operatorOds.getId())
                .operatorName(operatorOds.getName())
                .stationId(stationOds.getId() == null ? 0 : stationOds.getId())
                .stationName(stationOds.getStationName())
                .pileCode(pileStateDTO.getPileCode())
                .gunCode(gunOds.getGunCode() == null ? null : gunOds.getGunCode())
                .state(pileStateDTO.getState())
                .stateMessage(pileStateDTO.getStateMessage())
                .logTime(pileStateDTO.getLogTime())
                .gmtCreate(DateUtil.now())
                .build();
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        initConnection("runcd_mysql");
        operatorOds = CacheBuilder.newBuilder()
                //最多缓存个数，超过了就根据最近最少使用算法来移除缓存
                .maximumSize(5000)
                //在更新后的指定时间后就回收
                .expireAfterWrite(1, TimeUnit.DAYS)
                //指定移除通知
                .removalListener(new RemovalListener<String, OperatorOds>() {
                    @Override
                    public void onRemoval(RemovalNotification<String, OperatorOds> removalNotification) {
//                        System.out.println(removalNotification.getKey() + "被移除了，值为：" + JSON.toJSONString(removalNotification.getValue()));
                    }
                })
                .build(
                        //指定加载缓存的逻辑
                        new CacheLoader<String, OperatorOds>() {
                            @Override
                            public OperatorOds load(String operatorId) throws Exception {
                                OperatorOds operator = findOperatorById(operatorId);
                                return operator;
                            }
                        }
                );

        stationOds = CacheBuilder.newBuilder()
                //最多缓存个数，超过了就根据最近最少使用算法来移除缓存
                .maximumSize(5000)
                //在更新后的指定时间后就回收
                .expireAfterWrite(1, TimeUnit.DAYS)
                //指定移除通知
                .removalListener(new RemovalListener<Integer, StationOds>() {
                    @Override
                    public void onRemoval(RemovalNotification<Integer, StationOds> removalNotification) {
//                        System.out.println(removalNotification.getKey() + "被移除了，值为：" + JSON.toJSONString(removalNotification.getValue()));
                    }
                })
                .build(
                        //指定加载缓存的逻辑
                        new CacheLoader<Integer, StationOds>() {
                            @Override
                            public StationOds load(Integer stationId) throws Exception {
                                return findStationById(stationId);
                            }
                        }
                );
        pileods = CacheBuilder.newBuilder()
                //最多缓存个数，超过了就根据最近最少使用算法来移除缓存
                .maximumSize(5000)
                //在更新后的指定时间后就回收
                .expireAfterWrite(1, TimeUnit.DAYS)
                //指定移除通知
                .removalListener(new RemovalListener<String, PileOds>() {
                    @Override
                    public void onRemoval(RemovalNotification<String, PileOds> removalNotification) {
//                        System.out.println(removalNotification.getKey() + "被移除了，值为：" + JSON.toJSONString(removalNotification.getValue()));
                    }
                })
                .build(
                        //指定加载缓存的逻辑
                        new CacheLoader<String, PileOds>() {
                            @Override
                            public PileOds load(String pileId) throws Exception {
                                PileOds pile = findPileById(pileId);
                                return pile;
                            }
                        }
                );
        gunOds = CacheBuilder.newBuilder()
                //最多缓存个数，超过了就根据最近最少使用算法来移除缓存
                .maximumSize(1000)
                //在更新后的指定时间后就回收
                .expireAfterWrite(1, TimeUnit.DAYS)
                //指定移除通知
                .removalListener(new RemovalListener<String, GunOds>() {
                    @Override
                    public void onRemoval(RemovalNotification<String, GunOds> removalNotification) {
//                        System.out.println(removalNotification.getKey() + "被移除了，值为：" + JSON.toJSONString(removalNotification.getValue()));
                    }
                })
                .build(
                        //指定加载缓存的逻辑
                        new CacheLoader<String, GunOds>() {
                            @Override
                            public GunOds load(String gunCode) throws Exception {
                                return findGunByCode(gunCode);
                            }
                        }
                );
    }


    @Override
    public void close() throws Exception {
        super.close();
        // 关闭连接池
        connection.close();
        operatorOds.cleanUp();
        stationOds.cleanUp();
        pileods.cleanUp();
        gunOds.cleanUp();
    }

    private PileOds findPileById(String pileId) throws SQLException {
        String sql = "SELECT `id`,`name`,`pile_code`,`operator_id`,`station_id` from ods_pile where `pile_code` = ? and `deleted` = 'N'";

        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, pileId);
        preparedStatement.addBatch();

        ResultSet resultSet = preparedStatement.executeQuery();
        PileOds pile = new PileOds();
        while (resultSet.next()) {
            pile.setId(resultSet.getInt(1));
            pile.setName(resultSet.getString(2));
            pile.setPileCode(resultSet.getString(3));
            pile.setOperatorId(resultSet.getString(4));
            pile.setStationId(resultSet.getInt(5));
        }
        return pile;
    }

    private GunOds findGunByCode(String gunCode) throws SQLException {
        String sql = "SELECT `id`,`station_id`,`pile_id`,`gun_code`,`operator_id` from ods_gun where `gun_code` = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, gunCode);
        preparedStatement.addBatch();

        ResultSet resultSet = preparedStatement.executeQuery();
        GunOds gun = new GunOds();
        while (resultSet.next()) {
            gun.setId(resultSet.getInt(1));
            gun.setStationId(resultSet.getInt(2));
            gun.setPileId(resultSet.getInt(3));
            gun.setGunCode(resultSet.getString(4));
            gun.setOperatorId(resultSet.getString(5));
        }
        return gun;
    }

    private StationOds findStationById(Integer stationId) throws SQLException {
        String sql = "SELECT `id`,`station_code`,`station_name`,`province`,`city`,`area` from ods_station where `id` = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1, stationId);
        preparedStatement.addBatch();

        ResultSet resultSet = preparedStatement.executeQuery();
        StationOds station = new StationOds();
        while (resultSet.next()) {
            station.setId(resultSet.getInt(1));
            station.setStationCode(resultSet.getString(2));
            station.setStationName(resultSet.getString(3));
            station.setProvince(resultSet.getString(4));
            station.setCity(resultSet.getString(5));
            station.setArea(resultSet.getString(6));
        }
        return station;
    }

    private OperatorOds findOperatorById(String operatorId) throws SQLException {
        String sql = "SELECT `id`,`name` from ods_operator where `id` = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, operatorId);
        preparedStatement.addBatch();

        ResultSet resultSet = preparedStatement.executeQuery();
        OperatorOds operator = new OperatorOds();
        while (resultSet.next()) {
            operator.setId(resultSet.getString(1));
            operator.setName(resultSet.getString(2));
        }
        return operator;
    }

    public void initConnection(String dataBase) throws ClassNotFoundException, SQLException {
        ExecutionConfig config = getRuntimeContext().getExecutionConfig();
        String profile = config.getGlobalJobParameters().toMap().get("profile");
        if ("prod".equals(profile)) {
            Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
            String url = "jdbc:clickhouse://172.16.16.4:8123/" + dataBase;
            connection = DriverManager.getConnection(url, "default", "34p9KgvXDBG!DmEhsn%F6cmAUgf$JS");
        } else {
            Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
            String url = "jdbc:clickhouse://119.29.28.134:8123/" + dataBase;
            connection = DriverManager.getConnection(url, "default", "runcdcktest1234!@#$");
        }
    }

    @Override
    public void timeout(PileStateDTO input, ResultFuture<PileStateDTO> resultFuture) {
        System.out.println("PileStateMapper 执行超时");
        resultFuture.complete(Collections.singleton(new PileStateDTO()));
    }
}
