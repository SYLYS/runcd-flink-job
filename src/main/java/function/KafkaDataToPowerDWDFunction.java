//package function;
//
//import com.alibaba.fastjson.JSON;
//import com.google.common.cache.*;
//import dto.*;
//import org.apache.flink.api.common.functions.RichMapFunction;
//import org.apache.flink.configuration.Configuration;
//
//import java.math.BigDecimal;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.text.SimpleDateFormat;
//import java.util.concurrent.TimeUnit;
//
///**
// * @author: niehy
// * @createDate: 2023/3/23
// * @Description:
// */
//public class KafkaDataToPowerDWDFunction extends RichMapFunction<KafkaDataDTO, RealTimePowerDWD> {
//    LoadingCache<String, OperatorOds> operatorOds;
//    LoadingCache<Integer, StationOds> stationOds;
//    LoadingCache<Integer, PileOds> pileods;
//    LoadingCache<String, GunOds> gunOds;
//
//    Connection connection = null;
//
//    @Override
//    public RealTimePowerDWD map(KafkaDataDTO kafkaDataDTO) throws Exception {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//
//        CMD104 cmd104 = JSON.parseObject(kafkaDataDTO.getDataJson(), CMD104.class);
//        GunOds gunOds = this.gunOds.get(cmd104.getGunCode());
//        PileOds pileOds = this.pileods.get(gunOds.getPileId());
//        StationOds stationOds = this.stationOds.get(gunOds.getStationId());
//        OperatorOds operatorOds = this.operatorOds.get(gunOds.getOperatorId());
//
//        RealTimePowerDWD realTimePowerDWD = RealTimePowerDWD.builder()
//                .operator_id(operatorOds.getId()).operator_name(operatorOds.getName())
//                .station_id(stationOds.getId()).station_name(stationOds.getStationName())
//                .pile_code(pileOds.getPileCode()).gun_code(gunOds.getGunCode())
//                .province(stationOds.getProvince()).city(stationOds.getCity()).area(stationOds.getArea())
//                .v(BigDecimal.valueOf(cmd104.getAcVA()).divide(BigDecimal.TEN))
//                .i(BigDecimal.valueOf(cmd104.getAcIC()).divide(BigDecimal.TEN))
//                .power(BigDecimal.valueOf(cmd104.getChargePower()).divide(BigDecimal.valueOf(100)))
//                .dateT(cmd104.getUpdateTime().substring(0,16))
//                .build();
//        return realTimePowerDWD;
//    }
//
//    @Override
//    public void open(Configuration parameters) throws Exception {
//        connection = ClickUtils.getConnection("runcd_mysql");
//
//        operatorOds = CacheBuilder.newBuilder()
//                //最多缓存个数，超过了就根据最近最少使用算法来移除缓存
//                .maximumSize(1000)
//                //在更新后的指定时间后就回收
//                .expireAfterWrite(10, TimeUnit.MINUTES)
//                //指定移除通知
//                .removalListener(new RemovalListener<String, OperatorOds>() {
//                    @Override
//                    public void onRemoval(RemovalNotification<String, OperatorOds> removalNotification) {
////                        System.out.println(removalNotification.getKey() + "被移除了，值为：" + JSON.toJSONString(removalNotification.getValue()));
//                    }
//                })
//                .build(
//                        //指定加载缓存的逻辑
//                        new CacheLoader<String, OperatorOds>() {
//                            @Override
//                            public OperatorOds load(String operatorId) throws Exception {
//                                OperatorOds operator = findOperatorById(operatorId);
//                                return operator;
//                            }
//                        }
//                );
//
//        stationOds = CacheBuilder.newBuilder()
//                //最多缓存个数，超过了就根据最近最少使用算法来移除缓存
//                .maximumSize(1000)
//                //在更新后的指定时间后就回收
//                .expireAfterWrite(10, TimeUnit.MINUTES)
//                //指定移除通知
//                .removalListener(new RemovalListener<Integer, StationOds>() {
//                    @Override
//                    public void onRemoval(RemovalNotification<Integer, StationOds> removalNotification) {
////                        System.out.println(removalNotification.getKey() + "被移除了，值为：" + JSON.toJSONString(removalNotification.getValue()));
//                    }
//                })
//                .build(
//                        //指定加载缓存的逻辑
//                        new CacheLoader<Integer, StationOds>() {
//                            @Override
//                            public StationOds load(Integer stationId) throws Exception {
//                                StationOds station = findStationById(stationId);
//                                return station;
//                            }
//                        }
//                );
//        pileods = CacheBuilder.newBuilder()
//                //最多缓存个数，超过了就根据最近最少使用算法来移除缓存
//                .maximumSize(1000)
//                //在更新后的指定时间后就回收
//                .expireAfterWrite(10, TimeUnit.MINUTES)
//                //指定移除通知
//                .removalListener(new RemovalListener<Integer, PileOds>() {
//                    @Override
//                    public void onRemoval(RemovalNotification<Integer, PileOds> removalNotification) {
////                        System.out.println(removalNotification.getKey() + "被移除了，值为：" + JSON.toJSONString(removalNotification.getValue()));
//                    }
//                })
//                .build(
//                        //指定加载缓存的逻辑
//                        new CacheLoader<Integer, PileOds>() {
//                            @Override
//                            public PileOds load(Integer pileId) throws Exception {
//                                PileOds pile = finfPileById(pileId);
//                                return pile;
//                            }
//                        }
//                );
//        gunOds = CacheBuilder.newBuilder()
//                //最多缓存个数，超过了就根据最近最少使用算法来移除缓存
//                .maximumSize(1000)
//                //在更新后的指定时间后就回收
//                .expireAfterWrite(10, TimeUnit.MINUTES)
//                //指定移除通知
//                .removalListener(new RemovalListener<String, GunOds>() {
//                    @Override
//                    public void onRemoval(RemovalNotification<String, GunOds> removalNotification) {
////                        System.out.println(removalNotification.getKey() + "被移除了，值为：" + JSON.toJSONString(removalNotification.getValue()));
//                    }
//                })
//                .build(
//                        //指定加载缓存的逻辑
//                        new CacheLoader<String, GunOds>() {
//                            @Override
//                            public GunOds load(String gunCode) throws Exception {
//                                GunOds gun = findGunByCode(gunCode);
//                                return gun;
//                            }
//                        }
//                );
//    }
//
//    @Override
//    public void close() throws Exception {
//        super.close();
//        if(connection!=null){
//            connection.close();
//        }
//    }
//
//    private PileOds finfPileById(Integer pileId) throws SQLException {
//        String sql = "SELECT `id`,`name`,`pile_code`,`operator_id`,`station_id` from ods_pile where `id` = ?";
//
//        PreparedStatement preparedStatement = connection.prepareStatement(sql);
//        preparedStatement.setInt(1,pileId);
//        preparedStatement.addBatch();
//
//        ResultSet resultSet = preparedStatement.executeQuery();
//        PileOds pile = new PileOds();
//        while(resultSet.next()) {
//            pile.setId(resultSet.getInt(1));
//            pile.setName(resultSet.getString(2));
//            pile.setPileCode(resultSet.getString(3));
//            pile.setOperatorId(resultSet.getString(4));
//            pile.setStationId(resultSet.getInt(5));
//        }
//        return pile;
//    }
//
//    private GunOds findGunByCode(String gunCode) throws SQLException {
//        String sql = "SELECT `id`,`station_id`,`pile_id`,`gun_code`,`operator_id` from ods_gun where `gun_code` = ?";
//
//        PreparedStatement preparedStatement = connection.prepareStatement(sql);
//        preparedStatement.setString(1,gunCode);
//        preparedStatement.addBatch();
//
//        ResultSet resultSet = preparedStatement.executeQuery();
//        GunOds gun = new GunOds();
//        while(resultSet.next()) {
//            gun.setId(resultSet.getInt(1));
//            gun.setStationId(resultSet.getInt(2));
//            gun.setPileId(resultSet.getInt(3));
//            gun.setGunCode(resultSet.getString(4));
//            gun.setOperatorId(resultSet.getString(5));
//        }
//        return gun;
//    }
//
//    private StationOds findStationById(Integer stationId) throws SQLException {
//        String sql = "SELECT `id`,`station_code`,`station_name`,`province`,`city`,`area` from ods_station where `id` = ?";
//
//        PreparedStatement preparedStatement = connection.prepareStatement(sql);
//        preparedStatement.setInt(1,stationId);
//        preparedStatement.addBatch();
//
//        ResultSet resultSet = preparedStatement.executeQuery();
//        StationOds station = new StationOds();
//        while(resultSet.next()) {
//            station.setId(resultSet.getInt(1));
//            station.setStationCode(resultSet.getString(2));
//            station.setStationName(resultSet.getString(3));
//            station.setProvince(resultSet.getString(4));
//            station.setCity(resultSet.getString(5));
//            station.setArea(resultSet.getString(6));
//        }
//        return station;
//    }
//
//    private OperatorOds findOperatorById(String operatorId) throws SQLException {
//        String sql = "SELECT `id`,`name` from ods_operator where `id` = ?";
//
//        PreparedStatement preparedStatement = connection.prepareStatement(sql);
//        preparedStatement.setString(1,operatorId);
//        preparedStatement.addBatch();
//
//        ResultSet resultSet = preparedStatement.executeQuery();
//        OperatorOds operator = new OperatorOds();
//        while(resultSet.next()) {
//            operator.setId(resultSet.getString(1));
//            operator.setName(resultSet.getString(2));
//        }
//        return operator;
//    }
//
//}
