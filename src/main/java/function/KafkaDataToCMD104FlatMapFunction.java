package function;

import com.alibaba.fastjson.JSON;
import dto.CMD104;
import dto.KafkaDataDTO;
import dto.PowerDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @Description TODO
 * @Author Niehy
 * @Date 2022-06-27 10:35
 */
@Slf4j
public class KafkaDataToCMD104FlatMapFunction implements FlatMapFunction<ArrayList<KafkaDataDTO>, PowerDTO> {

    @Override
    public void flatMap(ArrayList<KafkaDataDTO> kafkaDataDTOS, Collector<PowerDTO> out) {
        try {
            if (CollectionUtils.isEmpty(kafkaDataDTOS)) {
                log.info("binlog为空");
                return;
            }
            kafkaDataDTOS.stream().filter(Objects::nonNull).forEach(kafkaDataDTO -> {
//                out.collect(kafkaDataDTO);
                if (kafkaDataDTO.getTable().contains("t_cmd104")){
                    CMD104 cmd104 = JSON.parseObject(kafkaDataDTO.getDataJson(), CMD104.class);
                    PowerDTO powerDTO = new PowerDTO();
                    powerDTO.setGun_code(cmd104.getGunCode());
                    powerDTO.setPower(cmd104.getChargePower());
                    powerDTO.setTimestamp(System.currentTimeMillis());
                    out.collect(powerDTO);
                }
            });
        } catch (Exception e) {
            log.info("脏数据：{}",kafkaDataDTOS);
            log.info("异常信息：{}",e.getMessage());
        }
    }


    public static void main(String[] args) {
        String str = "t_cmd104_1";
        System.out.println(str.contains("t_cmd104"));
    }
}
