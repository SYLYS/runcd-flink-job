//package function;
//
//import dto.RealTimePowerDWD;
//import org.apache.flink.configuration.Configuration;
//import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//
//public class RealTimeDataClickhouseFunction extends RichSinkFunction<RealTimePowerDWD> {
//    Connection connection = null;
//
//    String sql;
//
//    public RealTimeDataClickhouseFunction(String sql) {
//        this.sql = sql;
//    }
//
//    @Override
//    public void open(Configuration parameters) throws Exception {
//        super.open(parameters);
//        connection = ClickUtils.getConnection("runcd_res");
//    }
//
//    @Override
//    public void close() throws Exception {
//        super.close();
//        if(connection!=null){
//            connection.close();
//        }
//    }
//
//    @Override
//    public void invoke(RealTimePowerDWD value, Context context) throws Exception {
//        PreparedStatement preparedStatement = connection.prepareStatement(sql);
//        preparedStatement.setString(1,value.operator_id);
//        preparedStatement.setString(2,value.operator_name);
//        preparedStatement.setInt(3,value.station_id);
//        preparedStatement.setString(4,value.station_name);
//        preparedStatement.setString(5,value.pile_code);
//        preparedStatement.setString(6,value.gun_code);
//        preparedStatement.setString(7,value.province);
//        preparedStatement.setString(8,value.city);
//        preparedStatement.setString(9,value.area);
//        preparedStatement.setBigDecimal(10,value.v);
//        preparedStatement.setBigDecimal(11,value.i);
//        preparedStatement.setBigDecimal(12,value.power);
//        preparedStatement.setString(13,value.dateT);
//        preparedStatement.setDate(14,value.gmt_create);
//        preparedStatement.addBatch();
//
//        long startTime = System.currentTimeMillis();
//        int[] ints = preparedStatement.executeBatch();
////        connection.commit();
//        long endTime = System.currentTimeMillis();
////        System.out.println("批量插入完毕用时：" + (endTime - startTime) + " -- 插入数据 = " + ints.length);
//        System.out.println("数据插入成功："+value);
//    }
//}
