package function;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import dto.KafkaDataDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import utli.DateUtil;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description TODO
 * @Author Niehy
 * @Date 2022-06-27 10:22
 */
@Slf4j
public class KafkaDataMapFunctionSink implements MapFunction<ConsumerRecord<String, byte[]>, ArrayList<KafkaDataDTO>> {

    @Override
    public ArrayList<KafkaDataDTO> map(ConsumerRecord<String, byte[]> record) throws Exception {
        ByteArrayOutputStream completeMsg = new ByteArrayOutputStream();
        SubscribeDataProto.Envelope envelope = SubscribeDataProto.Envelope.parseFrom(record.value());
        if (1 != envelope.getVersion()) {
            throw new IllegalStateException(String.format("unsupported version: %d", envelope.getVersion()));
        }
        //Envelope的Data字段对应Entries二进制序列，Entries包含一个或多个Event。在封装消费的过程中，
        //当一个Event的大小超过单个kafka消息大小限制时，Entries过大，需要将Entries二进制序列分割为多段，
        //每段装入一个Envelope，total和index字段分别记录总段数和分段序号，所以这里需要将分段的Data做拼接。
        if (0 == envelope.getIndex()) {
            completeMsg = new ByteArrayOutputStream();
            envelope.getData().writeTo(completeMsg);
        } else {
            envelope.getData().writeTo(completeMsg);
        }

        if (envelope.getIndex() < envelope.getTotal() - 1) {
            return new ArrayList<>(0);
        }
        // 将Envelope.Data反序列化为Entries
        SubscribeDataProto.Entries entries;
        try {
            entries = SubscribeDataProto.Entries.parseFrom(completeMsg.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            log.error(e.getMessage());
            return new ArrayList<>(0);
        }
        // 遍历Entries中的Entry，在onEntry函数中处理消费到的每个Entry
        ArrayList<KafkaDataDTO> kafkaDataDTOS = new ArrayList<>();
        for (SubscribeDataProto.Entry entry : entries.getItemsList()) {
            KafkaDataDTO kafkaDataDTO = onEntry(entry);
            if (kafkaDataDTO == null) {
                continue;
            }
            kafkaDataDTOS.add(kafkaDataDTO);
        }
        return kafkaDataDTOS;
    }

    private static KafkaDataDTO onEntry(SubscribeDataProto.Entry entry) throws Exception {
        return trans2SQL(entry);
    }

    private static KafkaDataDTO trans2SQL(SubscribeDataProto.Entry entry) {
//        log.info("---entry: {}", JSONObject.toJSONString(entry));
        SubscribeDataProto.Header header = entry.getHeader();
//        log.info(String.format("--> [%s:%d], happenedAt: %s",
//                header.getFileName(), header.getPosition(),
//                LocalDateTime.ofInstant(Instant.ofEpochSecond(header.getTimestamp()), TimeZone.getDefault().toZoneId())));
        KafkaDataDTO kafkaDataDTO = null;

        switch (entry.getHeader().getMessageType()) {
            case HEARTBEAT:
//                log.info("heartbeat: " + entry.getEvent().getHeartbeatEvent().getEpoch());
                break;
            case CHECKPOINT:
                SubscribeDataProto.CheckpointEvent evt = entry.getEvent().getCheckpointEvent();
//                log.info("Checkpoint: gtid=" + evt.getSyncedGtid() + ", file=" + evt.getFileName() + ", position=" + evt.getPosition());
                break;
            case BEGIN:
//                log.info("BEGIN");
                break;
            case COMMIT:
//                log.info("COMMIT");
                break;
            case ROLLBACK:
//                log.info("ROLLBACK");
                break;
            case DDL:
//                log.info(entry.getEvent().getDdlEvent().getSql());
                break;
            case DML:
                kafkaDataDTO = new KafkaDataDTO();
                kafkaDataDTO.setDmlType(entry.getEvent().getDmlEvent().getDmlEventType().getNumber());
                kafkaDataDTO.setDataBase(header.getSchemaName());
                kafkaDataDTO.setTable(header.getTableName());
                kafkaDataDTO.setDataJson(transJson(entry));
                break;
            default:
                throw new IllegalStateException("unsupported message type");
        }
        return kafkaDataDTO;
    }

    /**
     * @Author: lyhong
     * @Date: 2022-06-24 10:39
     * @Description: 数据转对象json
     **/
    private static String transJson(SubscribeDataProto.Entry entry) {
        SubscribeDataProto.DMLEvent dmlEvent = entry.getEvent().getDmlEvent();

        List<Map<String, Object>> resultList = new ArrayList<>(10);
        List<Map<String, Object>> eventList;
        Map<String, Object> eventMap;
        switch (entry.getEvent().getDmlEvent().getDmlEventType()) {
            case UPDATE:
                eventList = new ArrayList<>(10);

                for (int j = 0; j < dmlEvent.getRowsList().size(); j++) {
                    eventMap = new HashMap<>(16);
                    SubscribeDataProto.RowChange row = dmlEvent.getRowsList().get(j);
                    for (int i = 0; i < row.getNewColumnsCount(); i++) {
                        SubscribeDataProto.Data col = row.getNewColumns(i);
                        if (col.getDataType() != SubscribeDataProto.DataType.NA && col.getDataType() != SubscribeDataProto.DataType.NIL) {
                            eventMap.put(dmlEvent.getColumns(i).getName(), deValue(col, dmlEvent.getColumns(i).getOriginalType()));
                        }
                    }
                    eventList.add(eventMap);
                }
                resultList.addAll(eventList);
                break;
            case INSERT:
                eventMap = new HashMap<>(16);
                for (int i = 0; i < dmlEvent.getColumnsList().size(); i++) {
                    eventMap.put(dmlEvent.getColumns(i).getName(), deValue(dmlEvent.getRows(0).getNewColumns(i), dmlEvent.getColumns(i).getOriginalType()));
                }
                resultList.add(eventMap);
                break;
            default:
                break;
        }

        return JSON.toJSONString(resultList);
    }


    /**
     * 转换结果值
     *
     * @param data         订阅数据
     * @param originalType 数据库中数据类型
     * @return 转换后数据
     */
    private static String deValue(SubscribeDataProto.Data data, String originalType) {
        switch (data.getDataType()) {
            case INT8:
            case INT16:
            case INT32:
            case INT64:
            case UINT8:
            case UINT16:
            case UINT32:
            case UINT64:
            case DECIMAL:
            case FLOAT32:
            case FLOAT64:
                return data.getSv();
            case STRING:
                String value = new String(data.getBv().toByteArray());
                if (StringUtils.equals(originalType, "timestamp")) {
                    return parseUTC2GMT(value);
                }
                return value;
            case BYTES:
                return DatatypeConverter.printHexBinary(data.getBv().toByteArray());
            case NIL:
                return null;
            case NA:
            default:
                return "";
        }
    }

    private static void transInsert(SubscribeDataProto.Entry entry) throws Exception {
        SubscribeDataProto.DMLEvent dmlEvt = entry.getEvent().getDmlEvent();
        System.out.print(String.format("INSERT INTO `%s`.`%s` VALUES ", entry.getHeader().getSchemaName(), entry.getHeader().getTableName()));
        String rowSep = "";
        for (SubscribeDataProto.RowChange row : dmlEvt.getRowsList()) {
            System.out.print(rowSep);
            System.out.print('(');
            String sep = "";
            for (SubscribeDataProto.Data col : row.getNewColumnsList()) {
                System.out.print(sep);
                System.out.print(decode(col));
                sep = ", ";
            }
            System.out.print(')');
            rowSep = ", ";
        }
    }

    private static void transDelete(SubscribeDataProto.Entry entry) throws Exception {
        SubscribeDataProto.DMLEvent dmlEvt = entry.getEvent().getDmlEvent();
        String header = String.format("DELETE FROM `%s`.`%s` WHERE ", entry.getHeader().getSchemaName(), entry.getHeader().getTableName());
        for (SubscribeDataProto.RowChange row : dmlEvt.getRowsList()) {
            System.out.print(header);
            String sep = "";
            for (int i = 0; i < row.getOldColumnsCount(); i++) {
                SubscribeDataProto.Data col = row.getOldColumns(i);
                if (col.getDataType() == SubscribeDataProto.DataType.NA) {
                    continue;
                }

                System.out.print(sep);
                SubscribeDataProto.Column colDef = dmlEvt.getColumns(i);
                System.out.print(String.format("`%s`", colDef.getName()));
                if (col.getDataType() == SubscribeDataProto.DataType.NIL) {
                    System.out.print(" IS NULL");
                } else {
                    System.out.print(String.format(" = %s", decode(col)));
                }
                sep = " AND ";
            }
            log.info(" LIMIT 1");
        }
    }

    private static void transUpdate(SubscribeDataProto.Entry entry) throws Exception {
        SubscribeDataProto.DMLEvent dmlEvt = entry.getEvent().getDmlEvent();
        String header = String.format("UPDATE `%s`.`%s` SET ", entry.getHeader().getSchemaName(), entry.getHeader().getTableName());
        for (SubscribeDataProto.RowChange row : dmlEvt.getRowsList()) {
            System.out.print(header);
            String sep = "";
            for (int i = 0; i < row.getNewColumnsCount(); i++) {
                SubscribeDataProto.Data col = row.getNewColumns(i);
                if (col.getDataType() == SubscribeDataProto.DataType.NA) {
                    continue;
                }

                System.out.print(sep);
                SubscribeDataProto.Column colDef = dmlEvt.getColumns(i);
                System.out.print(String.format("`%s`", colDef.getName()));
                if (col.getDataType() == SubscribeDataProto.DataType.NIL) {
                    System.out.print(" IS NULL");
                } else {
                    System.out.print(String.format(" = %s", decode(col)));
                }
                sep = ", ";
            }

            System.out.print(" WHERE ");
            sep = "";
            for (int i = 0; i < row.getOldColumnsCount(); i++) {
                SubscribeDataProto.Data col = row.getOldColumns(i);
                if (col.getDataType() == SubscribeDataProto.DataType.NA) {
                    continue;
                }

                System.out.print(sep);
                SubscribeDataProto.Column colDef = dmlEvt.getColumns(i);
                System.out.print(String.format("`%s`", colDef.getName()));
                if (col.getDataType() == SubscribeDataProto.DataType.NIL) {
                    System.out.print(" IS NULL");
                } else {
                    System.out.print(String.format(" = %s", decode(col)));
                }
                sep = " AND ";
            }
            log.info(" LIMIT 1");
        }
    }

    private static String decode(SubscribeDataProto.Data data) throws Exception {
        switch (data.getDataType()) {
            case INT8:
            case INT16:
            case INT32:
            case INT64:
            case UINT8:
            case UINT16:
            case UINT32:
            case UINT64:
            case DECIMAL:
            case FLOAT32:
            case FLOAT64:
                return data.getSv();
            case STRING:
                return "_binary'" + new String(data.getBv().toByteArray()) + "'";
            case BYTES:
                return "x'" + DatatypeConverter.printHexBinary(data.getBv().toByteArray()) + "'";
            case NA:
                return "DEFAULT";
            case NIL:
                return "NULL";
            default:
                throw new IllegalStateException("unsupported data type");
        }
    }

    /**
     * UTC/CST 时间转换 GMT时间
     *
     * @param utcTime UTC/CST 时间
     * @return GMT时间
     */
    public static String parseUTC2GMT(String utcTime) {
        SimpleDateFormat utcFormat = new SimpleDateFormat(DateUtil.Pattern.DATETIME, Locale.US);
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        SimpleDateFormat gmtFormat = new SimpleDateFormat(DateUtil.Pattern.DATETIME);
        try {
            Date date = utcFormat.parse(utcTime);
            return gmtFormat.format(date);
        } catch (ParseException e) {
            log.error("时间转换异常", e);
        }
        return "";
    }
}
