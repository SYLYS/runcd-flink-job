//package function;
//
//import dto.PowerDTO;
//import org.apache.flink.configuration.Configuration;
//import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//
//public class ClickhouseFunction extends RichSinkFunction<PowerDTO> {
//    Connection connection = null;
//
//    String sql;
//
//    public ClickhouseFunction(String sql) {
//        this.sql = sql;
//    }
//
//    @Override
//    public void open(Configuration parameters) throws Exception {
//        super.open(parameters);
//        connection = ClickUtils.getConnection("runcd_res");
//    }
//
//    @Override
//    public void close() throws Exception {
//        super.close();
//        if(connection!=null){
//            connection.close();
//        }
//    }
//
//    @Override
//    public void invoke(PowerDTO value, Context context) throws Exception {
//        PreparedStatement preparedStatement = connection.prepareStatement(sql);
//        preparedStatement.setLong(1,value.timestamp);
//        preparedStatement.setString(2,value.gun_code);
//        preparedStatement.setLong(3,value.power);
//        preparedStatement.addBatch();
//
//        long startTime = System.currentTimeMillis();
//        int[] ints = preparedStatement.executeBatch();
////        connection.commit();
//        long endTime = System.currentTimeMillis();
////        System.out.println("批量插入完毕用时：" + (endTime - startTime) + " -- 插入数据 = " + ints.length);
//        System.out.println("数据插入成功："+value);
//    }
//}
