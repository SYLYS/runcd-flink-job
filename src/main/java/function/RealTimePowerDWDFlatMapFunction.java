//package function;
//
//import com.alibaba.fastjson.JSON;
//import dto.CMD104;
//import dto.KafkaDataDTO;
//import dto.PowerDTO;
//import dto.RealTimePowerDWD;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.collections.CollectionUtils;
//import org.apache.flink.api.common.functions.FlatMapFunction;
//import org.apache.flink.util.Collector;
//
//import java.util.ArrayList;
//import java.util.Objects;
//
///**
// * @author: niehy
// * @createDate: 2023/3/23
// * @Description:
// */
//@Slf4j
//public class RealTimePowerDWDFlatMapFunction implements FlatMapFunction<ArrayList<KafkaDataDTO>, RealTimePowerDWD> {
//
//    @Override
//    public void flatMap(ArrayList<KafkaDataDTO> kafkaDataDTOS, Collector<RealTimePowerDWD> out) throws Exception {
//        try {
//            if (CollectionUtils.isEmpty(kafkaDataDTOS)) {
//                log.info("binlog为空");
//                return;
//            }
//            kafkaDataDTOS.stream().filter(Objects::nonNull).forEach(kafkaDataDTO -> {
////                out.collect(kafkaDataDTO);
//                if (kafkaDataDTO.getTable().contains("t_cmd104")){
//                    CMD104 cmd104 = JSON.parseObject(kafkaDataDTO.getDataJson(), CMD104.class);
//                    RealTimePowerDWD powerDWD = new RealTimePowerDWD();
//
//                    powerDWD.setGun_code(cmd104.getGunCode());
//
//                    out.collect(powerDWD);
//                }
//            });
//        } catch (Exception e) {
//            log.info("脏数据：{}",kafkaDataDTOS);
//            log.info("异常信息：{}",e.getMessage());
//        }
//    }
//}
