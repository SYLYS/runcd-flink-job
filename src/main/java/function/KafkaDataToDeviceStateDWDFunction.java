//package function;
//
//import com.google.common.cache.*;
//import dto.*;
//import org.apache.flink.api.common.functions.RichMapFunction;
//import org.apache.flink.configuration.Configuration;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.time.LocalDateTime;
//import java.time.ZoneId;
//import java.time.ZonedDateTime;
//import java.time.format.DateTimeFormatter;
//import java.util.concurrent.TimeUnit;
//
///**
// * @author: niehy
// * @createDate: 2023/3/23
// * @Description:
// */
//public class KafkaDataToDeviceStateDWDFunction extends RichMapFunction<PileStateDTO, PileStateDTO> {
//    LoadingCache<String, OperatorOds> operatorOds;
//    LoadingCache<Integer, StationOds> stationOds;
//    LoadingCache<String, PileOds> pileods;
//    LoadingCache<String, GunOds> gunOds;
//
//    Connection connection = null;
//
//    @Override
//    public PileStateDTO map(PileStateDTO pileStateDTO) throws Exception {
//        GunOds gunOds = new GunOds();
//        StationOds stationOds = new StationOds();
//        OperatorOds operatorOds = new OperatorOds();
//        PileOds pileOds = this.pileods.get(pileStateDTO.getPileCode());
//        if (pileStateDTO.getGunCode() != null) {
//            gunOds = this.gunOds.get(pileStateDTO.getGunCode());
//        }
//        if (pileOds != null) {
//            if (pileOds.getStationId() != null) {
//                stationOds = this.stationOds.get(pileOds.getStationId());
//            }
//            if (pileOds.getOperatorId() != null) {
//                operatorOds = this.operatorOds.get(pileOds.getOperatorId());
//            }
//        }
//        // 获取当前日期
//        LocalDateTime currentDate = LocalDateTime.now();
//
//        // 转换为指定时区的时间
//        ZonedDateTime zonedDateTime = currentDate.atZone(ZoneId.of("Asia/Shanghai"));
//        // 创建日期格式化对象
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//
//        // 将日期格式化为字符串
//        String formattedDate = formatter.format(zonedDateTime);
//
//        PileStateDTO realTimePowerDWD = PileStateDTO.builder()
//                .operatorId(operatorOds.getId())
//                .operatorName(operatorOds.getName())
//                .stationId(stationOds.getId() == null ? 0 : stationOds.getId())
//                .stationName(stationOds.getStationName())
//                .pileCode(pileStateDTO.getPileCode())
//                .gunCode(gunOds.getGunCode() == null ? null : gunOds.getGunCode())
//                .state(pileStateDTO.getState())
//                .stateMessage(pileStateDTO.getStateMessage())
//                .logTime(pileStateDTO.logTime)
//                .gmt_create(formattedDate)
//                .build();
//        return realTimePowerDWD;
//    }
//
//    @Override
//    public void open(Configuration parameters) throws Exception {
//        connection = ClickUtils.getConnection("runcd_mysql");
//
//        operatorOds = CacheBuilder.newBuilder()
//                //最多缓存个数，超过了就根据最近最少使用算法来移除缓存
//                .maximumSize(5000)
//                //在更新后的指定时间后就回收
//                .expireAfterWrite(1, TimeUnit.DAYS)
//                //指定移除通知
//                .removalListener(new RemovalListener<String, OperatorOds>() {
//                    @Override
//                    public void onRemoval(RemovalNotification<String, OperatorOds> removalNotification) {
////                        System.out.println(removalNotification.getKey() + "被移除了，值为：" + JSON.toJSONString(removalNotification.getValue()));
//                    }
//                })
//                .build(
//                        //指定加载缓存的逻辑
//                        new CacheLoader<String, OperatorOds>() {
//                            @Override
//                            public OperatorOds load(String operatorId) throws Exception {
//                                OperatorOds operator = findOperatorById(operatorId);
//                                return operator;
//                            }
//                        }
//                );
//
//        stationOds = CacheBuilder.newBuilder()
//                //最多缓存个数，超过了就根据最近最少使用算法来移除缓存
//                .maximumSize(5000)
//                //在更新后的指定时间后就回收
//                .expireAfterWrite(1, TimeUnit.DAYS)
//                //指定移除通知
//                .removalListener(new RemovalListener<Integer, StationOds>() {
//                    @Override
//                    public void onRemoval(RemovalNotification<Integer, StationOds> removalNotification) {
////                        System.out.println(removalNotification.getKey() + "被移除了，值为：" + JSON.toJSONString(removalNotification.getValue()));
//                    }
//                })
//                .build(
//                        //指定加载缓存的逻辑
//                        new CacheLoader<Integer, StationOds>() {
//                            @Override
//                            public StationOds load(Integer stationId) throws Exception {
//                                StationOds station = findStationById(stationId);
//                                return station;
//                            }
//                        }
//                );
//        pileods = CacheBuilder.newBuilder()
//                //最多缓存个数，超过了就根据最近最少使用算法来移除缓存
//                .maximumSize(5000)
//                //在更新后的指定时间后就回收
//                .expireAfterWrite(1, TimeUnit.DAYS)
//                //指定移除通知
//                .removalListener(new RemovalListener<String, PileOds>() {
//                    @Override
//                    public void onRemoval(RemovalNotification<String, PileOds> removalNotification) {
////                        System.out.println(removalNotification.getKey() + "被移除了，值为：" + JSON.toJSONString(removalNotification.getValue()));
//                    }
//                })
//                .build(
//                        //指定加载缓存的逻辑
//                        new CacheLoader<String, PileOds>() {
//                            @Override
//                            public PileOds load(String pileId) throws Exception {
//                                PileOds pile = findPileById(pileId);
//                                return pile;
//                            }
//                        }
//                );
//        gunOds = CacheBuilder.newBuilder()
//                //最多缓存个数，超过了就根据最近最少使用算法来移除缓存
//                .maximumSize(1000)
//                //在更新后的指定时间后就回收
//                .expireAfterWrite(10, TimeUnit.MINUTES)
//                //指定移除通知
//                .removalListener(new RemovalListener<String, GunOds>() {
//                    @Override
//                    public void onRemoval(RemovalNotification<String, GunOds> removalNotification) {
////                        System.out.println(removalNotification.getKey() + "被移除了，值为：" + JSON.toJSONString(removalNotification.getValue()));
//                    }
//                })
//                .build(
//                        //指定加载缓存的逻辑
//                        new CacheLoader<String, GunOds>() {
//                            @Override
//                            public GunOds load(String gunCode) throws Exception {
//                                GunOds gun = findGunByCode(gunCode);
//                                return gun;
//                            }
//                        }
//                );
//    }
//
//    @Override
//    public void close() throws Exception {
//        super.close();
//        if (connection != null) {
//            connection.close();
//        }
//    }
//
//    private PileOds findPileById(String pileId) throws SQLException {
//        String sql = "SELECT `id`,`name`,`pile_code`,`operator_id`,`station_id` from ods_pile where `pile_code` = ? and `deleted` = 'N'";
//
//        PreparedStatement preparedStatement = connection.prepareStatement(sql);
//        preparedStatement.setString(1, pileId);
//        preparedStatement.addBatch();
//
//        ResultSet resultSet = preparedStatement.executeQuery();
//        PileOds pile = new PileOds();
//        while (resultSet.next()) {
//            pile.setId(resultSet.getInt(1));
//            pile.setName(resultSet.getString(2));
//            pile.setPileCode(resultSet.getString(3));
//            pile.setOperatorId(resultSet.getString(4));
//            pile.setStationId(resultSet.getInt(5));
//        }
//        return pile;
//    }
//
//    private GunOds findGunByCode(String gunCode) throws SQLException {
//        String sql = "SELECT `id`,`station_id`,`pile_id`,`gun_code`,`operator_id` from ods_gun where `gun_code` = ?";
//
//        PreparedStatement preparedStatement = connection.prepareStatement(sql);
//        preparedStatement.setString(1, gunCode);
//        preparedStatement.addBatch();
//
//        ResultSet resultSet = preparedStatement.executeQuery();
//        GunOds gun = new GunOds();
//        while (resultSet.next()) {
//            gun.setId(resultSet.getInt(1));
//            gun.setStationId(resultSet.getInt(2));
//            gun.setPileId(resultSet.getInt(3));
//            gun.setGunCode(resultSet.getString(4));
//            gun.setOperatorId(resultSet.getString(5));
//        }
//        return gun;
//    }
//
//    private StationOds findStationById(Integer stationId) throws SQLException {
//        String sql = "SELECT `id`,`station_code`,`station_name`,`province`,`city`,`area` from ods_station where `id` = ?";
//
//        PreparedStatement preparedStatement = connection.prepareStatement(sql);
//        preparedStatement.setInt(1, stationId);
//        preparedStatement.addBatch();
//
//        ResultSet resultSet = preparedStatement.executeQuery();
//        StationOds station = new StationOds();
//        while (resultSet.next()) {
//            station.setId(resultSet.getInt(1));
//            station.setStationCode(resultSet.getString(2));
//            station.setStationName(resultSet.getString(3));
//            station.setProvince(resultSet.getString(4));
//            station.setCity(resultSet.getString(5));
//            station.setArea(resultSet.getString(6));
//        }
//        return station;
//    }
//
//    private OperatorOds findOperatorById(String operatorId) throws SQLException {
//        String sql = "SELECT `id`,`name` from ods_operator where `id` = ?";
//
//        PreparedStatement preparedStatement = connection.prepareStatement(sql);
//        preparedStatement.setString(1, operatorId);
//        preparedStatement.addBatch();
//
//        ResultSet resultSet = preparedStatement.executeQuery();
//        OperatorOds operator = new OperatorOds();
//        while (resultSet.next()) {
//            operator.setId(resultSet.getString(1));
//            operator.setName(resultSet.getString(2));
//        }
//        return operator;
//    }
//}
