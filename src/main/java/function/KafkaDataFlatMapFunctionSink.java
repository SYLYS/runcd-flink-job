package function;

import dto.KafkaDataDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

/**
 * @Description TODO
 * @Author Niehy
 * @Date 2022-06-27 10:35
 */
@Slf4j
public class KafkaDataFlatMapFunctionSink implements FlatMapFunction<ArrayList<KafkaDataDTO>, KafkaDataDTO> {

    @Override
    public void flatMap(ArrayList<KafkaDataDTO> kafkaDataDTOS, Collector<KafkaDataDTO> out) {
        try {
            if (CollectionUtils.isEmpty(kafkaDataDTOS)) {
                return;
            }
            kafkaDataDTOS.stream().filter(Objects::nonNull).forEach(kafkaDataDTO -> {
                out.collect(kafkaDataDTO);
            });
        } catch (Exception e) {
            log.info("脏数据：{}",kafkaDataDTOS);
            log.info("异常信息：{}",e.getMessage());
        }
    }
}
