package function;

import dto.PileOds;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MysqlDataSourceTest extends RichSourceFunction<PileOds> {
    PreparedStatement preparedStatement;
    private Connection connection;
    public MysqlDataSourceTest() {
        super();
    }
    @Override
    public void open(Configuration parameters) throws Exception {
        super.open( parameters );
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://gz-cdb-f3as145r.sql.tencentcdb.com:58965/runcd_res?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&allowMultiQueries=true";
        String username = "runcd";
        String password = "rcdtest2021";
        Class.forName( driver );
        connection = DriverManager.getConnection( url, username, password );
        String sql = "select * from t_pile;";
        preparedStatement = connection.prepareStatement( sql );
    }
    @Override
    public void close() throws Exception {
        super.close();
        if (connection != null) {
            connection.close();
        }
        if (preparedStatement != null) {
            preparedStatement.close();
        }
    }
    @Override
    public void run(SourceContext<PileOds> sourceContext) throws Exception {
        try {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PileOds pileOds = new PileOds();
                pileOds.setId(resultSet.getInt("id"));
                pileOds.setPileCode(resultSet.getString("pile_code"));
                pileOds.setName(resultSet.getString("name"));
                pileOds.setOperatorId(resultSet.getString("operator_id"));
                pileOds.setStationId(resultSet.getInt("station_id"));
                pileOds.setPileTypeId(resultSet.getInt("pile_type_id"));
                sourceContext.collect( pileOds );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void cancel() {
    }
}
