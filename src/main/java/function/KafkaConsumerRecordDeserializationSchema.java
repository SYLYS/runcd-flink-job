package function;

import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema;
import org.apache.kafka.clients.consumer.ConsumerRecord;

/**
 * @Description TODO
 * @Author Niehy
 * @Date 2022-06-23 22:52
 */
public class KafkaConsumerRecordDeserializationSchema implements KafkaDeserializationSchema<ConsumerRecord<String, byte[]>> {

    @Override
    public boolean isEndOfStream(ConsumerRecord<String, byte[]> stringConsumerRecord) {
        return false;
    }

    @Override
    public ConsumerRecord<String, byte[]> deserialize(ConsumerRecord<byte[], byte[]> record) throws Exception {
        return new ConsumerRecord<String, byte[]>(
                record.topic(),
                record.partition(),
                record.offset(),
                record.key() != null ? new String(record.key()) : null,
                record.value() != null ? record.value() : null);
    }

    @Override
    public TypeInformation<ConsumerRecord<String, byte[]>> getProducedType() {
        return TypeInformation.of(new TypeHint<ConsumerRecord<String, byte[]>>() {
        });
    }
}
