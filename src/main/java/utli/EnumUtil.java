package utli;

/**
 * @description: TODO
 * @author: NieHY
 * @date: 2023/10/26 22:39
 * @version: 1.0
 */
public class EnumUtil {

    public static String queryStationStatus(String stationStatusId){
        String stationStatus = "";
        switch(stationStatusId){
            case "0":stationStatus = "未知";break;
            case "1":stationStatus = "建设中";break;
            case "5":stationStatus = "关闭下线";break;
            case "6":stationStatus = "维护中";break;
            case "7":stationStatus = "维保到期";break;
            case "50":stationStatus = "正常使用";break;
            default:stationStatus = "未知";break;
        }
        return stationStatus;
    }

    public static String queryStationAddressType(String stationStatusId){
        String stationStatus = "";
        switch(stationStatusId){
            case "1":stationStatus = "居民区";break;
            case "2":stationStatus = "公共机构";break;
            case "3":stationStatus = "企业事业单位";break;
            case "4":stationStatus = "写字楼";break;
            case "5":stationStatus = "工业园区";break;
            case "6":stationStatus = "交通枢纽";break;
            case "7":stationStatus = "大型文体设施";break;
            case "8":stationStatus = "城市绿地";break;
            case "9":stationStatus = "大型建筑配建停车场";break;
            case "10":stationStatus = "路边停车位";break;
            case "11":stationStatus = "城际高速服务区";break;
            case "100":stationStatus = "新能源充电站";break;
            case "201":stationStatus = "测试用站点";break;
            default:stationStatus = "未知";break;
        }
        return stationStatus;
    }

    public static String queryStationBusinessType(String stationStatusId){
        String stationStatus = "";
        switch(stationStatusId){
            case "OWN":stationStatus = "自营";break;
            case "AGENT":stationStatus = "代理运营";break;
            case "OTHER":stationStatus = "独立运营";break;
            case "REPLACE":stationStatus = "代收款";break;
            default:stationStatus = "未知";break;
        }
        return stationStatus;
    }
}
