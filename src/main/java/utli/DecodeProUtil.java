package utli;

import dto.ACStateUp;
import dto.DirectStateUp;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;

/**
 * @author: niehy
 * @createDate: 2023/3/28
 * @Description:
 */
@Slf4j
public class DecodeProUtil {




    private static void initCMD258(byte[] request, int index, Object object) throws Exception {
        LinkedHashMap<String, Integer> gunStateMap = new LinkedHashMap<>();
        LinkedHashMap<String, Integer> gunConnStateMap = new LinkedHashMap<>();
        ACStateUp acStateUp = (ACStateUp) object;
        for (int i = 1; i <= acStateUp.getCount(); i++) {
            byte[] byteArray = new byte[1];
            System.arraycopy(request, index, byteArray, 0, 1);
            Method method = DecodeProUtil.class.getMethod("byte1int", byte[].class);
            Integer info = (Integer) method.invoke(null, byteArray);
            String codenum = acStateUp.getCode() + acStateUp.getGunType() + i;
            gunStateMap.put(codenum, info);
            gunConnStateMap.put(codenum, Integer.parseInt(String.valueOf(acStateUp.getConnState().charAt(16 - i))));
            index++;
        }
        acStateUp.setGunStateMap(gunStateMap);
        acStateUp.setGunConnStateMap(gunConnStateMap);
    }

    private static void initCMD514(byte[] request, int index, Object object) throws Exception {
        LinkedHashMap<String, Integer> gunStateMap = new LinkedHashMap<>();
        LinkedHashMap<String, Integer> gunConnStateMap = new LinkedHashMap<>();
        DirectStateUp directStateUp = (DirectStateUp) object;
        for (int i = 1; i <= directStateUp.getCount(); i++) {
            byte[] byteArray = new byte[1];
            System.arraycopy(request, index, byteArray, 0, 1);
            Method method = DecodeProUtil.class.getMethod("byte1int", byte[].class);
            Integer info = (Integer) method.invoke(null, byteArray);
            String codenum = directStateUp.getCode() + directStateUp.getGunType() + i;
            gunStateMap.put(codenum, info);
            gunConnStateMap.put(codenum, Integer.parseInt(String.valueOf(directStateUp.getConnState().charAt(16 - i))));
            index++;
        }
        directStateUp.setGunStateMap(gunStateMap);
        directStateUp.setGunConnStateMap(gunConnStateMap);
    }
}
