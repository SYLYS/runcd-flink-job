//package utli;
//
//import org.apache.kafka.clients.consumer.KafkaConsumer;
//import org.apache.kafka.clients.producer.KafkaProducer;
//import org.apache.kafka.clients.producer.ProducerConfig;
//import org.apache.kafka.clients.producer.ProducerRecord;
//import org.apache.kafka.common.serialization.StringSerializer;
//
//import java.util.Properties;
//
///**
// * @author: niehy
// * @createDate: 2023/3/30
// * @Description:
// */
//public class KafkaUtil {
//
//
//    private static volatile KafkaProducer<String, String> instance;
//    private static volatile KafkaConsumer<String, String> consumerInstance;
//
//    public static KafkaProducer<String, String> getInstance() {
//        if (instance == null) {
//            synchronized (KafkaUtil.class) {
//                if (instance == null) {
//                    Properties props = new Properties();
//                    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, EnvConfig.kafkaEvnConf.getBrokers());
//                    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//                    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//                    instance = new KafkaProducer<>(props);
//                }
//            }
//        }
//        return instance;
//    }
//
//    public static KafkaConsumer<String, String> getConsumerInstancee() {
//        if (consumerInstance == null) {
//            synchronized (KafkaUtil.class) {
//                if (consumerInstance == null) {
//                    Properties props = new Properties();
//                    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, EnvConfig.kafkaEvnConf.getBrokers());
//                    props.put("group.id", "device_offline");
//                    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
//                    props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
//                    consumerInstance = new KafkaConsumer<>(props);
//                }
//            }
//        }
//        return consumerInstance;
//    }
//
//    public static void send(String topic, String key, String value) {
//        ProducerRecord<String, String> record = new ProducerRecord<>(topic, key, value);
//        getInstance().send(record);
//    }
//
//
//}
