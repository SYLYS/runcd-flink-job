package utli;

import com.alibaba.fastjson.JSONObject;
import decode.IotDecode;
import dto.HeartBeatDTO;
import dto.PileStateDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.map.HashedMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author: niehy
 * @createDate: 2023/3/28
 * @Description:
 */
@Slf4j
public class MessageAnalysisUtil {
    private static final String GUN_CODE = "guncode";
    private static final String MESSAGE = "message";
    private static final String TIME = "logtime";
    private static final String AC_STATE = "交流枪状态";
    private static final String DC_STATE = "直流枪状态";
    private static final String AC_REAL_TIME_DATA = "交流实时数据";
    private static final String DC_REAL_TIME_DATA = "直流实时数据";
    private static final String DEVICE_ONLINE_PREFIX = "创建连接";
    private static final String DEVICE_OFFLINE_PREFIX = "移除管道";

    private static final String DEVICE_HEART_BEAT = "心跳";


    public static List<PileStateDTO> dataMap(String jsonValue) {
        if (!jsonValue.contains(AC_STATE) && !jsonValue.contains(DC_STATE)
                && !jsonValue.contains(DEVICE_ONLINE_PREFIX) && !jsonValue.contains(DEVICE_OFFLINE_PREFIX)
                && !jsonValue.contains(DEVICE_HEART_BEAT)
        ) {
            return null;
        }

        List<PileStateDTO> pileStateDtoList = new ArrayList<>();
        JSONObject jsonObject = JSONObject.parseObject(jsonValue);
        if (jsonObject != null) {
            String pileCode = jsonObject.getString(GUN_CODE);
            String message = jsonObject.getString(MESSAGE);
            message = message.substring(message.lastIndexOf("】") + 1).trim();
            String time = jsonObject.getString(TIME);
            if (jsonValue.contains(DEVICE_ONLINE_PREFIX) || jsonValue.contains(DEVICE_OFFLINE_PREFIX)) {
                PileStateDTO pileStateDTO = new PileStateDTO();
                pileStateDTO.setPileCode(pileCode);
                pileStateDTO.setState(jsonValue.contains(DEVICE_ONLINE_PREFIX) ? 98 : 99);
                pileStateDTO.setStateMessage(getStateMessageByCode(jsonValue.contains(DEVICE_ONLINE_PREFIX) ? 98 : 99));
                pileStateDtoList.add(pileStateDTO);
                pileStateDTO.setLogTime(time);
                return pileStateDtoList;
            } else if (jsonValue.contains(DEVICE_HEART_BEAT)) {
                PileStateDTO pileStateDTO = new PileStateDTO();
                pileStateDTO.setPileCode(pileCode);
                pileStateDTO.setState(100);
                pileStateDTO.setStateMessage("心跳");
                pileStateDtoList.add(pileStateDTO);
                pileStateDTO.setLogTime(time);
                return pileStateDtoList;
            }

            pileStateDtoList = dealWithMessage(pileCode, message, time);

        }
        return pileStateDtoList;
    }

    private static void getHeader(String message) {
    }


    public Map<String, Object> dataMapOrder(String jsonValue) {
        if (!jsonValue.contains("交流充电订单") && !jsonValue.contains("直流充电订单")) {
            return null;
        }
        JSONObject jsonObject = JSONObject.parseObject(jsonValue);
        if (jsonObject != null) {
            String pileCode = jsonObject.getString(GUN_CODE);
            String message = jsonObject.getString(MESSAGE);
            message = message.substring(message.lastIndexOf("】") + 1).trim();
            String time = jsonObject.getString(TIME);

            byte[] bytes = EncodeUtil.hexStringToByteArray(message);
            if (jsonValue.contains("直流充电订单")) {
                return DecodeUtil.protocol2Map(IotDecode.DC_ORDER_PROPERTIES, bytes);
            } else if (jsonValue.contains("交流充电订单")) {
                return DecodeUtil.protocol2Map(IotDecode.AC_ORDER_PROPERTIES, bytes);
            }
        }
        return null;
    }

    public static Map<String, Object> dataToRealData(String jsonValue) {
        if (!jsonValue.contains("交流实时数据") && !jsonValue.contains("直流实时数据1")) {
            return null;
        }
        Map<String, Object> objectMap = new HashedMap();
        JSONObject jsonObject = JSONObject.parseObject(jsonValue);
        if (jsonObject != null) {
            String pileCode = jsonObject.getString(GUN_CODE);
            String message = jsonObject.getString(MESSAGE);
            message = message.substring(message.lastIndexOf("】") + 1).trim();
            String time = jsonObject.getString(TIME);

            objectMap.put("pileCode", pileCode);
            objectMap.put("time", time);
            byte[] bytes = EncodeUtil.hexStringToByteArray(message);
            if (jsonValue.contains("直流实时数据1")) {
                Map<String, Object> protocol2Map = DecodeUtil.protocol2Map(IotDecode.PILE_DC_REAL_DATA_1, bytes);
                objectMap.putAll(protocol2Map);
                return objectMap;
            } else if (jsonValue.contains("交流实时数据")) {
                Map<String, Object> protocol2Map = DecodeUtil.protocol2Map(IotDecode.PILE_AC_REAL_DATA, bytes);
                objectMap.putAll(protocol2Map);
                return objectMap;
            }
        }
        return null;
    }

    public List<HeartBeatDTO> dateToHeartBeat(String jsonValue) {
        if (!jsonValue.contains("心跳")) {
            return null;
        }
        JSONObject jsonObject = JSONObject.parseObject(jsonValue);
        List<HeartBeatDTO> result = new ArrayList<>();
        if (jsonObject != null) {
            String time = jsonObject.getString(TIME);
            String pileCode = jsonObject.getString(GUN_CODE);
            String message = jsonObject.getString(MESSAGE);
            message = message.substring(message.lastIndexOf("】") + 1).trim();

            result = heartBeatWithMessage(pileCode, message, time);
        }
        return result;
    }

    private List<HeartBeatDTO> heartBeatWithMessage(String pileCode, String message, String time) {
        List<HeartBeatDTO> heartBeatDtoList = new ArrayList<>();
        byte[] bytes = EncodeUtil.hexStringToByteArray(message);

        return heartBeatDtoList;
    }


    private static List<PileStateDTO> dealWithMessage(String pileCode, String message, String time) {
        List<PileStateDTO> pileStateDtoList = new ArrayList<>();

        byte[] bytes = EncodeUtil.hexStringToByteArray(message);
        byte[] gunNumB = new byte[1];
        byte[] gunTypeB = new byte[1];
        byte[] carStateB = new byte[2];
        byte[] gunStateB = new byte[1];

        int gunNum, gunType;
        String carState;

        System.arraycopy(bytes, 19, gunTypeB, 0, 1);
        gunType = DecodeUtil.byte1int(gunTypeB);

        System.arraycopy(bytes, 25, gunNumB, 0, 1);
        gunNum = DecodeUtil.byte1int(gunNumB);

        System.arraycopy(bytes, 26, carStateB, 0, 2);
        carState = DecodeUtil.byte2binaryString(carStateB);

        for (int i = 0; i < gunNum; i++) {
            String gunCode = pileCode + gunType + (i + 1);
            System.arraycopy(bytes, 28 + i, gunStateB, 0, 1);
            int gunState = DecodeUtil.byte1int(gunStateB);
            int gunConnState = Integer.parseInt(String.valueOf(carState.charAt(15 - i)));

            PileStateDTO pileStateDTO = new PileStateDTO();
            pileStateDTO.setGunCode(gunCode);
            pileStateDTO.setPileCode(pileCode);
            pileStateDTO.setLogTime(time);

            if (gunState == 0 && gunConnState == 1) {
                pileStateDTO.setState(1);
                pileStateDTO.setStateMessage(getStateMessageByCode(1));
            } else if (gunState == 0 && gunConnState == 0) {
                pileStateDTO.setState(0);
                pileStateDTO.setStateMessage(getStateMessageByCode(0));
            } else if (gunState == 1) {
                pileStateDTO.setState(6);
                pileStateDTO.setStateMessage(getStateMessageByCode(6));
            } else {
                pileStateDTO.setState(gunState);
                pileStateDTO.setStateMessage(getStateMessageByCode(gunState));
            }
            pileStateDtoList.add(pileStateDTO);
        }
        return pileStateDtoList;
    }

    private static String getStateMessageByCode(int gunState) {
        String str;
        switch (gunState) {
            case 0:
                str = "空闲";
                break;
            case 1:
                str = "插枪";
                break;
            case 2:
                str = "充电中";
                break;
            case 3:
                str = "停止服务";
                break;
            case 4:
                str = "预约";
                break;
            case 6:
                str = "故障";
                break;
            case 98:
                str = DEVICE_ONLINE_PREFIX;
                break;
            case 99:
                str = DEVICE_OFFLINE_PREFIX;
                break;
            default:
                str = "状态错误";
                break;
        }
        return str;
    }
}
