package utli;


import cn.hutool.core.util.StrUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by gosin on 11/23/16.
 */
public class EncodeUtil {

    public static byte[] int4hex(long num) {

        byte[] hex = new byte[4];

        for (int i = 0; i < 4; i++) {
            hex[i] = (byte) ((num >> (i * 8)) & 0xff);
        }

        return DecodeUtil.heighToLow(hex);
    }

    public static byte[] int3hex(int num) {

        byte[] hex = new byte[3];

        for (int i = 0; i < 3; i++) {
            hex[i] = (byte) ((num >> (i * 8)) & 0xff);
        }

        return DecodeUtil.heighToLow(hex);
    }


    public static byte[] int2hex(int num) {

        byte[] hex = new byte[2];

        for (int i = 0; i < 2; i++) {
            hex[i] = (byte) ((num >> (i * 8)) & 0xff);
        }

        return DecodeUtil.heighToLow(hex);
    }

    public static byte[] int7hex(String num) {
        long lg = Long.parseLong(num);
        byte[] hex = new byte[7];

        for (int i = 0; i < 7; i++) {
            hex[6 - i] = (byte) ((lg >> (i * 8)) & 0xff);
        }

        return DecodeUtil.heighToLow(hex);
    }

    public static byte[] int8hex(String num) {
        long lg = Long.parseLong(num);
        byte[] hex = new byte[8];

        for (int i = 0; i < 8; i++) {
            hex[7 - i] = (byte) ((lg >> (i * 8)) & 0xff);
        }

        return DecodeUtil.heighToLow(hex);
    }

    public static byte[] int16hex(String num) {
        long lg = Long.parseLong(num);
        byte[] hex = new byte[16];

        for (int i = 0; i < 16; i++) {
            hex[15 - i] = (byte) ((lg >> (i * 8)) & 0xff);
        }

        return DecodeUtil.heighToLow(hex);
    }

    public static byte[] inthex(int num, int length) {
        byte[] hex = new byte[length];

        for (int i = 0; i < length; i++) {
            hex[i] = (byte) ((num >> (i * 8)) & 0xff);
        }

        return DecodeUtil.heighToLow(hex);
    }


    public static byte[] int1hex(int num) {

        byte[] hex = new byte[1];

        for (int i = 0; i < 1; i++) {
            hex[i] = (byte) ((num >> (i * 8)) & 0xff);
        }

        return hex;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * 计算CRC16校验码
     *
     * @param bytes 字节数组
     * @param end   结束位
     * @return {@link String} 校验码
     * @since 1.0
     */
    public static byte[] getCRC(byte[] bytes, int end) {
        int CRC = 0x0000ffff;
        int POLYNOMIAL = 0x0000a001;
        int i, j;
        for (i = 0; i < end; i++) {
            CRC ^= ((int) bytes[i] & 0x000000ff);
            for (j = 0; j < 8; j++) {
                if ((CRC & 0x00000001) != 0) {
                    CRC >>= 1;
                    CRC ^= POLYNOMIAL;
                } else {
                    CRC >>= 1;
                }
            }
        }
        return DecodeUtil.heighToLow(int2hex(CRC));
    }


    public static byte[] hexStringToByteArray(String s) {
        s = StrUtil.cleanBlank(s);
        s = StrUtil.removeAll(s, '-');
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * 16进制表示的字符串转换为字节数组
     *
     * @param s 16进制表示的字符串
     * @return byte[] 字节数组
     */
    public static int[] hexStringToIntArray(String s) {
        int len = s.length();
        int[] b = new int[len / 2];
        for (int i = 0; i < len; i += 2) {
            // 两位一组，表示一个字节,把这样表示的16进制字符串，还原成一个字节
            b[i / 2] = (int) ((Character.digit(s.charAt(i), 16) << 4) + Character
                    .digit(s.charAt(i + 1), 16));
        }
        return b;
    }

    /**
     * 16进制的字符串表示转成字节数组
     *
     * @param hexString 16进制格式的字符串
     * @return 转换后的字节数组
     **/

    public static int[] toIntArray(String hexString) {
        if (hexString.isEmpty()) {

        }

        hexString = hexString.toLowerCase();
        final int[] byteArray = new int[hexString.length() / 2];
        int k = 0;
        for (int i = 0; i < byteArray.length; i++) {// 因为是16进制，最多只会占用4位，转换成字节需要两个16进制的字符，高位在先
            int high = (int) (Character.digit(hexString.charAt(k), 16) & 0xff);
            int low = (int) (Character.digit(hexString.charAt(k + 1), 16) & 0xff);
            byteArray[i] = (int) (high << 4 | low);
            k += 2;
        }
        return byteArray;
    }

    /**
     * 时标CP56Time2a解析
     *
     * @param b 时标CP56Time2a（长度为7 的int数组）
     * @return 解析结果
     */
    @SuppressWarnings("unused")
    public static String timeScale(int b[]) {

        String str = "";
        int year = b[6] & 0x7F;
        int month = b[5] & 0x0F;
        int day = b[4] & 0x1F;
        int week = (b[4] & 0xE0) / 32;
        int hour = b[3] & 0x1F;
        int minute = b[2] & 0x3F;
        int second = (b[1] << 8) + b[0];

        str += "20" + year + "-"
                + String.format("%02d", month) + "-"
                + String.format("%02d", day) + " " + hour + ":" + minute + ":"
                + second / 1000 /*+ "." + second % 1000*/;
        return str + "\n";
    }


    /**
     * 时间转CP56Time2a
     *
     * @param date
     * @return 解析结果
     */
    @SuppressWarnings("deprecation")
    public static byte[] scaleTime(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yy");
        String str = "";
        long millseconds = getMillseconds(date);
        long rand = rand(millseconds);
        int r = (int) (rand % 1000);
        System.out.println("r:" + r);
        str += bytesToHex(DecodeUtil.heighToLow(int2hex(date.getSeconds() * 1000 + r)));
        str += bytesToHex(int1hex(date.getMinutes()));
        str += bytesToHex(int1hex(date.getHours()));
        str += bytesToHex(int1hex(date.getDate()));
        str += bytesToHex(int1hex(date.getMonth() + 1));
        str += bytesToHex(int1hex(Integer.parseInt(dateFormat.format(date))));
        return hexStringToByteArray(str);
    }

    public static long getMillseconds(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int ms_hour = calendar.get(Calendar.HOUR_OF_DAY);
        int ms_minute = calendar.get(Calendar.MINUTE);
        int ms_second = calendar.get(Calendar.SECOND);
        System.out.println(calendar.getTime());
        int ms_before = ms_hour * 3600 + ms_minute * 60 + ms_second;
        long rand = rand(ms_before);
        return rand;
    }

    public static long rand(long ms_before) {
        int s1 = 0x6D;
        int s2 = 0x8FBE;

        long ms_after = ms_before * s1 + s2;
        return ms_after;
    }

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException, ParseException {
    }
}
