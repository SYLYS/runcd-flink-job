package utli;

import decode.ProtocolProperty;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by gosin on 11/14/16.
 */
public class DecodeUtil {
    public static Map<String, Function<byte[], Object>> decodeMap = new HashMap<String, Function<byte[], Object>>() {
        {
            put("String", (b) -> new String(b).trim());
            put("byte3int", DecodeUtil::byte3int);
            put("byte3intH", DecodeUtil::byte3intH);
            put("byte2int", DecodeUtil::byte2int);
            put("byte2intH", DecodeUtil::byte2intH);
            put("byte1int", DecodeUtil::byte1int);
            put("byte4int", DecodeUtil::byte4int);
            put("byte4String", DecodeUtil::byte4String);
            put("byte8int", DecodeUtil::byte8int);
//            put("hex2decimal", DecodeUtil::hex2decimal);
            put("getXOR", DecodeUtil::getXOR);
//            put("hexString2binaryString", DecodeUtil::hexString2binaryString);
            put("byte2binaryString", DecodeUtil::byte2binaryString);
            put("hexStr2Str", DecodeUtil::hexStr2Str);
            put("bytesToHex", DecodeUtil::bytesToHex);
//            put("unicode2String", DecodeUtil::unicode2String);
            put("timeScale", DecodeUtil::timeScale);
            put("bytesToHexStringEmpty", DecodeUtil::bytesToHexStringEmpty);
            put("bytesToStrasc", DecodeUtil::bytesToStrasc);
            put("bytesToLong", DecodeUtil::bytesToLong);
        }

    };


    public static Map<String, Object> protocol2Map(List<ProtocolProperty> protocolProperties, byte[] bytes) {
        Map<String, Object> result = new HashMap<>();

        protocolProperties.forEach(p -> {
            Object byte2int = decode(bytes, p.getIndex(), p.getLength(), p.getMethod());
            result.put(p.getField(), byte2int);
        });
        return result;
    }

    private static Object decode(byte[] bytes, Integer index, Integer length, String method) {
        byte[] bytes19 = new byte[length];
        System.arraycopy(bytes, index, bytes19, 0, length);
        if (method == null) {
            return new String(bytes19).trim();
        }

        return DecodeUtil.decodeMap.get(method).apply(bytes19);
    }

    public static int byte3int(byte[] ba) {
        ba = heighToLow(ba);
        int v2 = (ba[0] & 0xff);
        int v3 = (ba[1] & 0xff) << 8;
        int v4 = (ba[1] & 0xff) << 16;
        return v2 | v3 | v4;
    }

    public static int byte3intH(byte[] ba) {
        int v2 = (ba[0] & 0xff);
        int v3 = (ba[1] & 0xff) << 8;
        int v4 = (ba[1] & 0xff) << 16;
        return v2 | v3 | v4;
    }


    public static int byte2int(byte[] ba) {
        ba = heighToLow(ba);
        int v2 = (ba[0] & 0xff);
        int v3 = (ba[1] & 0xff) << 8;
        return v2 | v3;
    }

    public static int byte2intH(byte[] ba) {
        int v2 = (ba[0] & 0xff);
        int v3 = (ba[1] & 0xff) << 8;
        return v2 | v3;
    }

    public static int byte1int(byte[] ba) {
        int v2 = (ba[0] & 0xff);

        return v2;
    }

    public static long byte4int(byte[] ba) {
        ba = heighToLow(ba);
        double v1 = (double) (ba[0] & 0xff) * 1L;
        double v2 = (ba[1] & 0xff) * 1L << 8;
        double v3 = (ba[2] & 0xff) * 1L << 16;
        double v4 = (ba[3] & 0xff) * 1L << 24;
        return BigDecimal.valueOf(v1 + v2 + v3 + v4).longValue();
    }

    public static String byte4String(byte[] ba) {
        ba = heighToLow(ba);
        double v1 = (double) (ba[0] & 0xff) * 1L;
        double v2 = (ba[1] & 0xff) * 1L << 8;
        double v3 = (ba[2] & 0xff) * 1L << 16;
        double v4 = (ba[3] & 0xff) * 1L << 24;
        return BigDecimal.valueOf(v1 + v2 + v3 + v4).toPlainString();
    }

    public static String byte7String(byte[] ba) {
        ba = heighToLow(ba);
        double v1 = (double) (ba[0] & 0xff) * 1L;
        double v2 = (ba[1] & 0xff) * 1L << 8;
        double v3 = (ba[2] & 0xff) * 1L << 16;
        double v4 = (ba[3] & 0xff) * 1L << 24;
        double v5 = (ba[4] & 0xff) * 1L << 32;
        double v6 = (ba[5] & 0xff) * 1L << 40;
        double v7 = (ba[6] & 0xff) * 1L << 48;
        return BigDecimal.valueOf(v1 + v2 + v3 + v4 + v5 + v6 + v6).toPlainString();
    }

    public static String byte8int(byte[] ba) {
        int v1 = (ba[0] & 0xff);
        int v2 = (ba[1] & 0xff);
        int v3 = (ba[2] & 0xff);
        int v4 = (ba[3] & 0xff);
        int v5 = (ba[4] & 0xff);
        int v6 = (ba[5] & 0xff);
        int v7 = (ba[6] & 0xff);
        int v8 = (ba[7] & 0xff);

        return hex2decimal(v1) + "" + hex2decimal(v2) + "" + hex2decimal(v3) + "" + hex2decimal(v4) + "" + hex2decimal(v5)
                + "" + hex2decimal(v6) + "" + hex2decimal(v7) + "" + hex2decimal(v8);
    }

    public static String hex2decimal(int i) {
        int v1 = i / 16;
        int v2 = i % 16;
        return v1 + "" + v2;
    }


    public static String getXOR(byte[] datas) {
        int result = 0;
        for (int i = 0; i < datas.length - 2; i++) {
            result ^= datas[i];
        }
        return EncodeUtil.bytesToHex(EncodeUtil.int1hex(result));
    }

    public static String hexString2binaryString(String hexString) {
        String binaryString = "";
        for (int i = 0; i < hexString.length(); i++) {
            //截取hexStr的一位
            String hex = hexString.substring(i, i + 1);
            //通过toBinaryString将十六进制转为二进制
            String binary = Integer.toBinaryString(Integer.parseInt(hex, 16));
            //因为高位0会被舍弃，先补上4个0
            String tmp = "0000" + binary;
            //取最后4位，将多补的0去掉
            binaryString += tmp.substring(tmp.length() - 4);
        }
        return binaryString;
    }


    public static String byte2binaryString(byte[] datas) {
        String hexString = EncodeUtil.bytesToHex(datas);
        String binaryString = "";
        for (int i = 0; i < hexString.length(); i++) {
            //截取hexStr的一位
            String hex = hexString.substring(i, i + 1);
            //通过toBinaryString将十六进制转为二进制
            String binary = Integer.toBinaryString(Integer.parseInt(hex, 16));
            //因为高位0会被舍弃，先补上4个0
            String tmp = "0000" + binary;
            //取最后4位，将多补的0去掉
            binaryString += tmp.substring(tmp.length() - 4);
        }
        return binaryString;
    }

    /**
     * btye数组高低位互换
     *
     * @param src
     * @return
     */
    public static byte[] heighToLow(byte[] src) {
        for (int start = 0, end = src.length - 1; start < end; start++, end--) {
            byte temp = src[end];
            src[end] = src[start];
            src[start] = temp;
        }
        return src;
    }

    /**
     * 16进制直接转换成为字符串(无需Unicode解码)
     *
     * @param ba
     * @return
     */
    public static String hexStr2Str(byte[] ba) {
        String hexStr = EncodeUtil.bytesToHex(ba);
        String str = "0123456789ABCDEF";
        char[] hexs = hexStr.toCharArray();
        byte[] bytes = new byte[hexStr.length() / 2];
        int n;
        for (int i = 0; i < bytes.length; i++) {
            n = str.indexOf(hexs[2 * i]) * 16;
            n += str.indexOf(hexs[2 * i + 1]);
            bytes[i] = (byte) (n & 0xff);
        }
        return new String(bytes);
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * unicode 转字符串
     */
    public static String unicode2String(String unicode) {
        StringBuffer string = new StringBuffer();
        String[] hex = unicode.split("\\\\u");
        for (int i = 1; i < hex.length; i++) {
            // 转换出每一个代码点
            int data = Integer.parseInt(hex[i], 16);
            // 追加成string
            string.append((char) data);
        }
        return string.toString();
    }

    /**
     * 时标CP56Time2a解析
     *
     * @param ba 时标CP56Time2a（长度为7 的int数组）
     * @return 解析结果
     */
    @SuppressWarnings("unused")
    public static String timeScale(byte[] ba) {
        int[] b = EncodeUtil.hexStringToIntArray(EncodeUtil.bytesToHex(ba));
        String str = "";
        int year = b[6] & 0x7F;
        int month = b[5] & 0x0F;
        int day = b[4] & 0x1F;
        int week = (b[4] & 0xE0) / 32;
        int hour = b[3] & 0x1F;
        int minute = b[2] & 0x3F;
        int second = (b[1] << 8) + b[0];
        second = second / 1000;
        str += "20" + year + "-"
                + String.format("%02d", month) + "-"
                + String.format("%02d", day) + " " + (hour >= 10 ? hour : "0" + hour) + ":" + (minute >= 10 ? minute : "0" + minute) + ":"
                + (second >= 10 ? second : "0" + second) /*+ "." + second % 1000*/;
        return str;
    }

    /**
     * @param src
     * @return String
     * @Description: byte 数组 转 十六进制字符串   每个字节后加空格  如：75 72 01...
     */
    public static String bytesToHexStringEmpty(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v).toUpperCase(); // 字母小写转大写在此已转，员函数没有
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv + " ");
        }
        return stringBuilder.toString().trim();
    }

    public static String bytesToStrasc(byte[] chars) {
        StringBuffer sbu = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            sbu.append((int) chars[i]);
        }
        return sbu.toString().trim();
    }


    public static long bytesToLong(byte[] b) {
        long l = ((long) b[0] << 56) & 0xFF00000000000000L;
        // 如果不强制转换为long，那么默认会当作int，导致最高32位丢失
        l |= ((long) b[1] << 48) & 0xFF000000000000L;
        l |= ((long) b[2] << 40) & 0xFF0000000000L;
        l |= ((long) b[3] << 32) & 0xFF00000000L;
        l |= ((long) b[4] << 24) & 0xFF000000L;
        l |= ((long) b[5] << 16) & 0xFF0000L;
        l |= ((long) b[6] << 8) & 0xFF00L;
        l |= (long) b[7] & 0xFFL;
        return l;
    }


    public static void main(String[] args) {
        byte[] b = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08};
        long s = bytesToLong(b);
        String s1 = Long.toBinaryString(s);
        System.out.println(s1);
    }

}
