package utli;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.RandomUtil;
import com.google.common.collect.HashBasedTable;
import config.EnvHolder;
import enums.EnvEnum;
import enums.NotifyGroupEnum;
import lombok.extern.slf4j.Slf4j;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

/**
 * @author xiaokun
 * @date 2023/4/20 17:23:38
 */
@Slf4j
public class NotifyUtil {

    private static final HashBasedTable<EnvEnum, NotifyGroupEnum, List<String>> table = HashBasedTable.create();

    static {
        // 目前所有环境共用消息组
        List<String> defaultHooks = Arrays.asList("https://oapi.dingtalk.com/robot/send?access_token=da17d731dae22c11fb68daab3d895a0473f218d677fb8dbf4ac64eefaff1df4b"
                , "https://oapi.dingtalk.com/robot/send?access_token=09a43322b32c4709d568e853a6f0a43ff58de0312cfcab3bd18e11bcf81d772b", "https://oapi.dingtalk.com/robot/send?access_token=6c229d7518ddce14e6f155943b752e3b50d60e472355d72e351945c35c82f5f1",
                "https://oapi.dingtalk.com/robot/send?access_token=8ee94fd4af93698c760ec04604bb01f974bdea00195389f6a864796f2dced488", "https://oapi.dingtalk.com/robot/send?access_token=067e054c5bd65fdbddcf0f1a8cb4238820d18475b68fe314ddc69d1a8775ff92");

        List<String> gunOrderRepeatUploadHooks = Arrays.asList("https://oapi.dingtalk.com/robot/send?access_token=fdecb1608bb954d9f872afc53ecc49a29f4351dd3b4eec52ae74d67649cddd51",
                "https://oapi.dingtalk.com/robot/send?access_token=f26e7ab8fefc72d453a313334e5429ddfcd77dcd66c4c728545e5467dc77e095");

        table.put(EnvEnum.PROD, NotifyGroupEnum.PILE_STATE_OFFLINE, defaultHooks);
        table.put(EnvEnum.TEST, NotifyGroupEnum.PILE_STATE_OFFLINE, defaultHooks);

        table.put(EnvEnum.PROD, NotifyGroupEnum.GUN_ORDER_REPEAT_UPLOAD, gunOrderRepeatUploadHooks);
        table.put(EnvEnum.TEST, NotifyGroupEnum.GUN_ORDER_REPEAT_UPLOAD, gunOrderRepeatUploadHooks);

    }


    public static void send(NotifyGroupEnum notifyGroup, String content) {
        List<String> hooks = table.get(EnvHolder.getCurrentEnv(), notifyGroup);
        if (CollUtil.isEmpty(hooks)) {
//            log.warn("发送钉钉通知失败,未配置hook | env:{} notifyGroup:{}", EnvHolder.getCurrentEnv(), notifyGroup);
            return;
        }
        // 获取当前
        String webhookUrl = hooks.get(RandomUtil.randomInt(0, hooks.size()));
        int respCode = -1;
        String respMsg = "";
        try {
            URL url = new URL(webhookUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");

            String message = "{\"msgtype\": \"text\",  \"at\": {\"isAtAll\": false}, \"text\": {\"content\": \"" + content + "\"}}";
            byte[] postData = message.getBytes(StandardCharsets.UTF_8);
            conn.setDoOutput(true);
            try (OutputStream os = conn.getOutputStream()) {
                os.write(postData);
                os.flush();
            }
            respCode = conn.getResponseCode();
            respMsg = conn.getResponseMessage();
            log.info("发送钉钉通知 | {} {}", respCode, respMsg);
        } catch (Exception e) {
            log.warn("发送钉钉通知失败 | {} {} | err:{}", respCode, respMsg, e.getMessage());
        }

    }

}
