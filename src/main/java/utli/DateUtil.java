package utli;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期时间工具类
 *
 * @version 1.0
 * @author zsz 2019-7-14
 */
@Slf4j
public class DateUtil {

    /**
     * 日期时间匹配格式
     */
    public interface Pattern {
        //
        // 常规模式
        // ----------------------------------------------------------------------------------------------------
        /**
         * yyyy-MM-dd
         */
        String DATE = "yyyy-MM-dd";
        /**
         * yyyy-MM-dd HH:mm:ss
         */
        String DATETIME = "yyyy-MM-dd HH:mm:ss";
        /**
         * yyyy-MM-dd HH:mm
         */
        String DATETIME_MM = "yyyy-MM-dd HH:mm";
        /**
         * yyyy-MM-dd HH:mm:ss.SSS
         */
        String DATETIME_SSS = "yyyy-MM-dd HH:mm:ss.SSS";
        /**
         * HH:mm
         */
        String TIME = "HH:mm";
        /**
         * HH:mm:ss
         */
        String TIME_SS = "HH:mm:ss";

        String SYS_DATETIME_00="yyyy-MM-dd HH:00";

        //
        // 系统时间格式
        // ----------------------------------------------------------------------------------------------------
        /**
         * yyyy/MM/dd
         */
        String SYS_DATE = "yyyy/MM/dd";
        /**
         * yyyy/MM/dd HH:mm:ss
         */
        String SYS_DATETIME = "yyyy/MM/dd HH:mm:ss";
        /**
         * yyyy/MM/dd HH:mm
         */
        String SYS_DATETIME_MM = "yyyy/MM/dd HH:mm";
        /**
         * yyyy/MM/dd HH:mm:ss.SSS
         */
        String SYS_DATETIME_SSS = "yyyy/MM/dd HH:mm:ss.SSS";

        String SYS_DATETIME_CHINA = "yyyy年MM月dd日";



        //
        // 无连接符模式
        // ----------------------------------------------------------------------------------------------------
        /**
         * yyyy
         */
        String YEAR = "yyyy";
        /**
         * yyyyMMdd
         */
        String NONE_DATE = "yyyyMMdd";
        /**
         * yyyy-MM
         */
        String MONTH_DATE = "yyyy-MM";
        /**
         * yyMMddHHmmss
         */
        String NONE_DATETIME_MM_SS = "yyMMddHHmmss";
        /**
         * yyyyMMddHHmmss
         */
        String NONE_DATETIME = "yyyyMMddHHmmss";
        /**
         * yyyyMMddHHmm
         */
        String NONE_DATETIME_MM = "yyyyMMddHHmm";
        /**
         * yyyyMMddHHmmssSSS
         */
        String NONE_DATETIME_SSS = "yyyyMMddHHmmssSSS";
        /**
         * yyyyMMddHHmmss.SSS
         */
        String NONE_DATETIME_DOT_SSS = "yyyyMMddHHmmss.SSS";
    }

    public static final String DEFAULT_PATTERN = Pattern.DATETIME;
    public static final String NONE_DATETIME = Pattern.NONE_DATETIME;
    public static final String DATETIME = Pattern.DATETIME;
    public static final String DATE = Pattern.DATE;
    public static final String SYS_DATETIME_CHINA = Pattern.SYS_DATETIME_CHINA;
    public static final String TIME = Pattern.TIME;

    public static final String[] PARSE_PATTERNS = new String[]{
            Pattern.DATE,
            Pattern.DATETIME,
            Pattern.DATETIME_MM,
            Pattern.DATETIME_SSS,
            Pattern.SYS_DATE,
            Pattern.SYS_DATETIME,
            Pattern.SYS_DATETIME_MM,
            Pattern.SYS_DATETIME_SSS
    };

    /**
     * 格式化日期时间
     *
     * @param date 日期时间
     *
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String format(Date date) {
        return format(date, DEFAULT_PATTERN);
    }

    /**
     * 格式化日期
     *
     * @param date 日期(时间)
     *
     * @param pattern 匹配模式 参考：{@link Pattern}
     *
     * @return 格式化后的字符串
     */
    public static String format(Date date, String pattern) {
        if (date == null) {
            return null;
        }
        pattern = StringUtils.isNotBlank(pattern) ? pattern : DEFAULT_PATTERN;
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    /**
     * 日期加天数,可以向前加，向后加
     *
     * @param date
     *            日期
     * @param day
     *            天数
     * @return 返回相加后的日期
     */
    public static Date addDay(Date date, int day) {

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(getMillis(date) + ((long) day) * 24 * 3600 * 1000);

        return c.getTime();
    }

    /**
     * 时间加分钟,可以向前加，向后加
     *
     * @param date 日期
     * @param minute 分钟
     * @return
     */
    public static Date addMinute(Date date, int minute) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(getMillis(date) +((long) minute) * 1000 * 60);
        return c.getTime();
    }


    /**
     * 日期加月份,可以向前加，向后加
     *
     * @param date
     *            日期
     * @param month
     *            月份数
     * @return 返回相加后的日期
     */
    public static Date addMonth(Date date, int month) {
        Calendar cl = Calendar.getInstance();
        cl.setTime(date);
        cl.add(Calendar.MONTH, month);
        return cl.getTime();
    }

    public static Date addYear(Date date, int year) {
        Calendar cl = Calendar.getInstance();
        cl.setTime(date);
        cl.add(Calendar.YEAR, year);
        return cl.getTime();
    }

    /**
     * 两个日期date1-date2相减，相差的天数
     *
     * @param date1
     *            日期
     * @param date2
     *            日期
     * @return 返回相减后的日期
     */
    public static int betweenTwoDates(Date date1, Date date2) {
        return (int) ((getMillis(date1) - getMillis(date2)) / (24 * 3600 * 1000));
    }

    /**
     * 两个日期date1-date2相减，相差的小时
     *
     * @param date1
     *            日期
     * @param date2
     *            日期
     * @return 返回相减后的日期
     */
    public static int betweenTwoHour(Date date1, Date date2) {
        return (int) ((getMillis(date1) - getMillis(date2)) / (3600 * 1000));
    }

    /**
     * @Author: zyy
     * @Date: 2021/12/27 17:06
     * @Description:  两个日期date1-date2相减，相差的秒数
     **/
    public static int betweenTwoMinute(Date date1, Date date2) {
        return (int) ((getMillis(date1) - getMillis(date2)) / (60 * 1000));
    }

    /**
     * @Author: lyhong
     * @Date: 2021/11/25 10:19
     * @Description:  两个日期date1-date2相减，相差的秒数
     **/
    public static int betweenTwoSec(Date date1, Date date2) {
        return (int) ((getMillis(date1) - getMillis(date2)) / (1000));
    }


    /**
     * 返回日期代表的毫秒
     *
     * @param date
     *            日期
     * @return 返回毫秒
     */
    public static long getMillis(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.getTimeInMillis();
    }

    /**
     * 解析日期
     *
     * @param date 日期字符串
     *
     * @return 解析后的日期 默认格式：yyyy-MM-dd HH:mm:ss
     */
    public static Date parseDate(String date) {
        if (StringUtils.isBlank(date)) {
            return null;
        }
        try {
            return DateUtils.parseDate(date, PARSE_PATTERNS);
        } catch (ParseException e) {
            log.error("错误:", e);
        }
        return null;
    }

    /**
     * 解析日期
     *
     * @param date 日期
     *
     * @param pattern 格式 参考：{@link Pattern}
     *
     * @return 解析后的日期，默认格式：yyyy-MM-dd HH:mm:ss
     */
    public static Date parseDate(String date, String pattern) {
        if (StringUtils.isBlank(date)) {
            return null;
        }
        String[] parsePatterns;
        parsePatterns = StringUtils.isNotBlank(pattern) ? new String[]{pattern} : PARSE_PATTERNS;
        try {
            return DateUtils.parseDate(date, parsePatterns);
        } catch (ParseException e) {
            log.error("错误:", e);
        }
        return null;
    }

    /**
     * @Author: lyhong
     * @Date: 2022/7/4 22:43
     * @Description: 今年的第一天
     **/
    public static String getNowYear() {
        Date now = new Date();
        String date = format(now, Pattern.YEAR);
        return date + "-01-01";
    }

    public static String parseStringDate(String date, String pattern1 , String pattern2) throws ParseException {
        pattern1 = StringUtils.isNotBlank(pattern1) ? pattern1 : DEFAULT_PATTERN;
        pattern2 = StringUtils.isNotBlank(pattern2) ? pattern2 : DEFAULT_PATTERN;
        SimpleDateFormat sdf1 = new SimpleDateFormat(pattern1);
        SimpleDateFormat sdf2 = new SimpleDateFormat(pattern2);
        Date parseDate = sdf1.parse(date);
        return sdf2.format(parseDate);
    }

    /**
     * 取得指定月份的第一天
     *
     * @param strdate
     *            String
     * @return String
     */
    public static String getMonthBegin(String strdate, String format) {

        return parseDateToString(parseStringToDate(strdate, "yyyy-MM"), format);
    }

    /**
     * 取得指定月份的最后一天
     *
     * @param strdate
     *            String
     * @return String
     */
    public static String getMonthEnd(String strdate, String format) {

        Date date = parseStringToDate(getMonthBegin(strdate, "yyyy-MM"), "yyyy-MM");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DAY_OF_YEAR, -1);

        return parseDateToString(calendar.getTime(), format);
    }

    /**
     * 将字符串转换为日期
     *
     * @param str
     * @return
     * @throws ParseException
     */
    public static Date parseStringToDate(String str, String format) {

        DateFormat formatter = null;
        Date date = null;
        if (StringUtils.isNotBlank(str)) {

            if (StringUtils.isBlank(format)) {
                formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            } else {
                formatter = new SimpleDateFormat(format);
            }

            try {
                date = formatter.parse(str);
            } catch (ParseException e) {
                log.error("错误:", e);
            }
        }

        return date;
    }

    /**
     * 日期转换为字符串
     *
     * @param date
     *            日期
     * @param format
     *            格式
     * @return 返回字符型日期
     */
    public static String parseDateToString(Date date, String format) {
        String result = "";
        DateFormat formatter = null;
        try {
            if (date != null) {
                if (StringUtils.isBlank(format)) {
                    formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                } else {
                    formatter = new SimpleDateFormat(format);
                }
                result = formatter.format(date);
            }
        } catch (Exception e) {
        }

        return result;
    }

    /**
     * 解析前置服务器日期
     * 格式为20190828123422.255
     * @param dateStr 20190828123422.255
     * @return
     */
    public static Date parseGosinDate(String dateStr){
        try {
            if(dateStr.contains(".")){
                return DateUtils.parseDate(dateStr.substring(0, dateStr.indexOf(".")), DateUtil.NONE_DATETIME);
            }
            return null;
        } catch (ParseException e) {
            log.error("错误:", e);
            return null;
        }
    }

    /**
     * 判断2个时间
     *@param date
     *@param date2
     *@param state  1:比较是否是同一个月，2:比较是否是同一天，3:比较是否是同一年
     *@return boolean
     */
    public static boolean matchSameMonth(Date date, Date date2, Integer state) {
        if (null == state) {
            return false;
        } else if (1 == state) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
            String str1 = simpleDateFormat.format(date);
            String str2 = simpleDateFormat.format(date2);
            if (str1.equals(str2)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取当月的天数
     *@param date
     *@param
     *@return int
     */
    public static int getMaxDays(Date date) {
        Calendar a = Calendar.getInstance();
        a.setTime(date);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }

    public static int longOfTwoDate(Date newDate, Date oldDate) {
        /**
         * 计算规则————按自然天数计算
         * 实现方式：
         * 1，获取现在的时间
         * 2，此后传入的时间与此时间求差值，在此后(0, 24]小时区间内不跨到第二天记1天，跨天记2天，以此类推(24, 48]小时区间不跨到第三天记2天，跨天记3天...
         * */
        try {
            long start = oldDate.getTime();
            //需要计算运行天数时，计算差值
            long end = newDate.getTime();
            long days = (end - start) / (1000 * 60 * 60 * 24);
            //运行天数从1开始计数
            long runningDays = days + 1;
            //判断是否跨天，若跨天，运行天数还要+1
            long probableEndMillis = start + (1000 * 60 * 60 * 24) * days;
            if (new Date(probableEndMillis).getDay() != new Date(end).getDay()) {
                runningDays++;
            }
            int day = (int) runningDays;
            return day;

        } catch (Exception e) {
            return 0;
        }
    }

}