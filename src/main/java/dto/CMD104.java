package dto;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Map;

@ToString
@Data
public class CMD104 implements Serializable {

	static final long serialVersionUID = -786347155463819557L;


    String chargingNo;

    String gunCode = "";

    String updateTime;

    Long id;
    String obligate1 = "";
    String obligate2 = "";

    Long gunCount = 0L;
    Long chargingPortNo = 0L;
    Long gunType = 0L;
    Long workStatus = 0L;
    Long currentSOC = 0L;
    Long warningStatus = 0L;
    Long carConnStatus = 0L;
    Long fee = 0L;
    Long IntegerernalVar2 = 0L;
    Long IntegerernalVar3 = 0L;
    Long dcV = 0L;
    Long dcI = 0L;
    Long bmsV = 0L;
    Long bmsI = 0L;
    Long bmsModel = 0L;
    Long acVA = 0L;
    Long acVB = 0L;
    Long acVC = 0L;
    Long acIA = 0L;
    Long acIB = 0L;
    Long acIC = 0L;
    Long restTime = 0L;
    Long chargeTime = 0L;
    Long chargeQuantity = 0L;
    
    Long readingBefCharging = 0L;
    
    Long currentReading = 0L;
    
    Long chargeStartModel = 0L;
    
    Long chargeTactic = 0L;
    
    Long chargeTacticParam = 0L;
    
    Long appoIntegermentSymbo= 0L ;
    
    String appoIntegermentCardNo = "";
    
    Long appoIntegermentOvertime = 0L;
    
    String appoIntegermentBegTime = "";
    
    Long balanceBefCharging = 0L;
    Long upgradeModel = 0L;
    Long chargePower = 0L;
    Long systemVar3 = 0L;
    Long systemVar4 = 0L;
    Long systemVar5 = 0L;
    Long outletTemperature  = 0L;
    Long ambientTemperature = 0L;
    Long gunTemperature  = 0L;

    Long electricAmt;//电费
    Long serviceAmt;//服务费
}
