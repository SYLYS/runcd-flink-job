package dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;


/**
 * @Description:桩
 * @Author: hhc
 * @Date: 2022/12/6 10:48
 */
@Data
@ToString
public class PileOds implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 设备主键id
     */
    @JsonProperty("id")
    private Integer id;

    /**
     * 充电桩编号
     */
    @JsonProperty("pile_code")
    private String pileCode;

    /**
     * 充电桩名称
     */
    @JsonProperty("name")
    private String name;

    /**
     * 所属运营商id
     */
    @JsonProperty("operator_id")
    private String operatorId;

    /**
     * 所属场站
     */
    @JsonProperty("station_id")
    private Integer stationId;

    @JsonProperty("pile_type_id")
    private Integer pileTypeId;

}
