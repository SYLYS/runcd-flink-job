package dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HeartBeatDTO {
    private String pileCode;
    private String timeStr;
//    private String gunCode;
}
