package dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * @author: niehy
 * @createDate: 2023/3/23
 * @Description:
 */
@Data
@Builder
public class RealTimePowerDWD {

    public String operator_id;

    public String operator_name;

    public Integer station_id;

    public String station_name;

    public String pile_code;

    public String gun_code;

    public String province;

    public String city;

    public String area;

    public BigDecimal v;

    public BigDecimal i;

    public BigDecimal power;

    //yyyy-MM-dd HH:mm
    public String dateT;

    public Date gmt_create;
}
