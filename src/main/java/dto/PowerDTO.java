package dto;

import lombok.Data;

/**
 * @author: niehy
 * @createDate: 2023/3/21
 * @Description:
 */
@Data
public class PowerDTO {

    public Long timestamp;

    public String gun_code;

    public Long power;
}
