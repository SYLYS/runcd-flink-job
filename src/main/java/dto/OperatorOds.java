package dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @description: 运营商
 * @author: hhc
 * @create: 2022-12-14 16:01
 **/
@Data
public class OperatorOds implements Serializable {

    private String id;
    private String name;

}
