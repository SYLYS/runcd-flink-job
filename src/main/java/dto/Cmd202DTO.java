package dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Cmd202DTO implements Serializable {
    /**
     * 终端编号
     */
    private String gunCode;

    /**
     * 充电开始时间
     */
    private String chargeBegTime;

    /**
     * 充电结束时间
     */
    private String chargeEndTime;

    private Date begTime;

    private Date endTime;

    /**
     * 主键
     */
    private Long id;

    /**
     * 预留1
     */
    private Integer obligate1;

    /**
     * 预留2
     */
    private Integer obligate2;

    /**
     * 充电枪位置类型
     */
    private Integer gunType;

    /**
     * 充电枪口
     */
    private Integer gunNo;

    /**
     * 终端上传卡号
     */
    private String originalCardNo;

    /**
     * 结算卡号
     */
    private String cardNo;

    /**
     * 充电时长
     */
    private Integer chargeTimeLength;

    /**
     * 开始SOC
     */
    private Integer begSoc;

    /**
     * 结束SOC
     */
    private Integer endSoc;

    /**
     * 结束原因
     */
    private Integer chargeEndReason;

    /**
     * 充电电量（0.01kw）
     */
    private Integer chargeTotalElectricity;

    /**
     * 开始充电电表示数
     */
    private Integer numBeforeCharge;

    /**
     * 结束充电电表示数
     */
    private Integer numAfterCharge;

    /**
     * 本次充电金额（分）
     */
    private Integer fee;

    /**
     * 当前充电记录索引
     */
    private Integer endNoSlot;

    /**
     * 充电前余额
     */
    private Integer balanceBeforeCharge;

    /**
     * 启动模式
     */
    private Integer startModel;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 流水号
     */
    private String chargingNo;

    /**
     * 总电费
     */
    private BigDecimal electricityFee;

    /**
     * 总服务费
     */
    private BigDecimal serviceFee;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 预留
     */
    private Integer obl1;

    /**
     * 预留
     */
    private Integer obl2;

    /**
     * 预留
     */
    private Integer obl3;

    /**
     * 充电策略
     */
    private Integer chargeTac;

    /**
     * 充电策略参数
     */
    private Integer chargeTacParam;

    /**
     * 车辆vin码
     */
    private String vin;

    /**
     * 车牌号
     */
    private String carNo;

    /**
     * 是否支付服务费
     */
    private Integer payState;

    /**
     * 虚拟运营商Id
     */
    private String operatorId;

    /**
     * 尖电量
     */
    private BigDecimal sharpAllElectricity;

    /**
     * 峰期总电量
     */
    private BigDecimal peakAllElectricity;

    /**
     * 平期总电量
     */
    private BigDecimal flatAllElectricity;

    /**
     * 谷期总电量
     */
    private BigDecimal valleyAllElectricity;

    /**
     * 尖总电费
     */
    private BigDecimal sharpAllElectricityFee;

    /**
     * 峰期总电费
     */
    private BigDecimal peakAllElectricityFee;

    /**
     * 平期总电费
     */
    private BigDecimal flatAllElectricityFee;

    /**
     * 谷期总电费
     */
    private BigDecimal valleyAllElectricityFee;

    /**
     * 尖总服务费
     */
    private BigDecimal sharpAllServiceFee;

    /**
     * 峰期总服务费
     */
    private BigDecimal peakAllServiceFee;

    /**
     * 平期总服务费
     */
    private BigDecimal flatAllServiceFee;

    /**
     * 谷期总服务费
     */
    private BigDecimal valleyAllServiceFee;

    /**
     * 损耗电量
     */
    private Integer lossElectricity;

    /**
     * 订单是否正常
     */
    private Integer normal;

    /**
     * 更新时间
     */
    private Date updateTime;


}