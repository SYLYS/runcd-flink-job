package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 充电枪
 * </p>
 *
 * @author zsz
 * @since 2019-07-14
 */
@Data
@ToString
public class GunOds implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键，枪id
     */
    private Integer id;

    /**
     * 所属场站
     */
    @JsonProperty("station_id")
    private Integer stationId;

    /**
     * 所属设备
     */
    @JsonProperty("pile_id")
    private Integer pileId;

    /**
     * 枪编号
     */
    @JsonProperty("gun_code")
    private String gunCode;


    /**
     * 所属运营商id
     */
    @JsonProperty("operator_id")
    private String operatorId;


}
