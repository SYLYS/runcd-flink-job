package dto;

import lombok.Data;

/**
 * @author: niehy
 * @createDate: 2023/3/23
 * @Description:
 */
@Data
public class AreaDTO {

    public String gun_code;

    public String province;

    public String city;

    public String area;
}
