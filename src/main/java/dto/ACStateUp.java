package dto;



import lombok.Data;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * @author zengwq
 * @date 2022/5/12
 * @description 充电枪状态帧
 */
@Data
public class ACStateUp implements Serializable {
    private static final long serialVersionUID = 3163100802019039139L;
    private int start;
    private int version;
    private String code;
    private int gunType;
    private int no;
    private int cmd;
    private int length;
    private int count;
    private String connState;
    private LinkedHashMap<String, Integer> gunStateMap;
    private LinkedHashMap<String, Integer> gunConnStateMap;

}
