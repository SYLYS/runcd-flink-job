package dto.dim;

import lombok.Data;

@Data
public class DIMGunInfo {
    /**
     * 枪code
     */
    private String gunCode;

    /**
     * 枪名称
     */
    private String gunName;

    /**
     * 桩code
     */
    private String pileCode;

    /**
     * 桩名称
     */
    private String pileName;
    

    /**
     * 运营商id
     */
    private String opId;

    /**
     * 运营商名称
     */
    private String opName;

    /**
     * 站点code
     */
    private String stCode;

    /**
     * 站点名称
     */
    private String stName;

    /**
     * 枪状态
     */
    private String isDeleted;
}
