package dto.dim;

import lombok.Data;

@Data
public class DIMAddressInfo {

    /**
     * 地址id
     */
    private String id;

    /**
     * 地址名称
     */
    private String name;

}
