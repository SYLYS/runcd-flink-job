package dto.dim;

import lombok.Data;

@Data
public class DIMStationInfo {

    /**
     * 站点id
     */
    private int stId;

    /**
     * 站点code
     */
    private String stCode;

    /**
     * 站点名称
     */
    private String stName;

    /**
     * 所属运营商id
     */
    private String opId;

    /**
     * 所属运营商名称
     */
    private String opName;

    /**
     * 站点注册省份
     */
    private String stProvince;

    /**
     * 站点注册城市
     */
    private String stCity;

    /**
     * 站点注册区
     */
    private String stArea;

    /**
     * 站点创建时间
     */
    private String stCreateDate;

    /**
     * 站点状态
     */
    private String isDeleted;

    /**
     * 站点开放状态
     */
    private String stStatus;

    /**
     * 站点地区类型
     */
    private String stAddressType;

    /**
     * 站点地区类型
     */
    private String stBusinessType;

}
