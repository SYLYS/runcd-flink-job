package dto.dim;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DIMPileTypeInfo {

    /**
     * 桩类型id
     */
    private int pileTypeId;

    /**
     * 型号名称
     */
    private String typeModel;

    /**
     * 桩厂家
     */
    private String manufacturer;

    /**
     * 电流类型 1:直流；2:交流；3:电单车；4:换电柜；5:分体桩；6:三相交流；7:液冷桩
     */
    private String currentType;

    /**
     * 额定功率
     */
    private BigDecimal ratedPower;

    /**
     * 额定电压
     */
    private BigDecimal ratedV;

    /**
     * 额定电流
     */
    private BigDecimal ratedI;

    /**
     * 桩型号状态
     */
    private String isDeleted;

    /**
     * 桩协议
     */
    private String pileProtocol;

}
