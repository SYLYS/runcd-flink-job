package dto.dim;

import enums.RedisKeyEnum;
import lombok.Data;

@Data
public class DIMOperatorInfo {
    /**
     * 运营商id
     */
    private String opId;

    /**
     * 运营商名称
     */
    private String opName;

//    /**
//     * 运营商注册省份
//     */
//    private String opProvince;
//
//    /**
//     * 运营商注册城市
//     */
//    private String opCity;
//
//    /**
//     * 运营商注册区
//     */
//    private String opArea;

    /**
     * 运营商创建时间
     */
    private String opCreateDate;

    /**
     * 运营商状态
     */
    private String state;

}
