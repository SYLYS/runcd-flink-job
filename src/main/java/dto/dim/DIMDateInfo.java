package dto.dim;

import lombok.Data;

@Data
public class DIMDateInfo {

    /**
     *日期key，例如2023-06-29
     */
    private String dateKey;

    /**
     * 日期id，例如20230629
     */
    private int dateId;

    /**
     * 日期年份，例如2023
     */
    private int dateY;

    /**
     * 日期月份，例如6
     */
    private int dateM;

    /**
     * 日期日，例如29
     */
    private int dateD;

    /**
     * 周数年份，例如2010-01-01是2009年的第53周
     */
    private int dateYW;

    /**
     * yyyy-mm,例如2023-07
     */
    private int dateYM;

    /**
     * 周数，例如26
     */
    private int dateW;

    /**
     * 季度，例如2
     */
    private int dateQ;

    /**
     * 星期的第几天，例如4
     */
    private int dateWD;

    /**
     * 月份名称，例如June
     */
    private String mName;

    /**
     * 星期名称，例如Thursday
     */
    private String wdName;
}
