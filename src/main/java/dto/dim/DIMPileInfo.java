package dto.dim;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DIMPileInfo {

    private long id;
    /**
     * 桩code
     */
    private String pileCode;

    /**
     * 桩名称
     */
    private String pileName;

    /**
     * 桩类型id
     */
    private int pileTypeId;

    /**
     * 运营商id
     */
    private String opId;

    /**
     * 运营商名称
     */
    private String opName;

    /**
     * 站点id
     */
    private int stId;

    /**
     * 站点code
     */
    private String stCode;

    /**
     * 站点名称
     */
    private String stName;

    /**
     * 站点注册省份
     */
    private String stProvince;

    /**
     * 站点注册城市
     */
    private String stCity;

    /**
     * 站点注册区
     */
    private String stArea;

    /**
     * 桩创建时间
     */
    private String pileCreateDate;

    /**
     * 桩状态
     */
    private String isDeleted;

    /**
     * 桩协议
     */
    private String pileProtocol;

    /**
     * 电流类型 1:直流；2:交流；3:电单车；4:换电柜；5:分体桩；6:三相交流；7:液冷桩
     */
    private String currentType;

    /**
     * 额定功率
     */
    private BigDecimal ratedPower;

    /**
     * 额定电压
     */
    private BigDecimal ratedV;

    /**
     * 额定电流
     */
    private BigDecimal ratedI;
}
