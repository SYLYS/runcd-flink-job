package dto.dw;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Data
@AllArgsConstructor
public class DWSPileAdd {
    private String key;
    private String dateYM;
    private int pileCount;
}
