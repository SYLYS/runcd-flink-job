package dto.dw;

import lombok.Data;

@Data
public class DWCustomerInfo {
    /**
     * id
     */
    private int id;
    /**
     * 用户ID
     */
    private String customerId;
    /**
     * 用户手机哈
     */
    private String phone;
    /**
     * 运营商ID
     */
    private String opId;
    /**
     * 运营商ID
     */
    private String opName;
    /**
     * 用户注册来源
     */
    private String registerResource;
    /**
     * 用户注册时间
     */
    private String  registerTime;
}
