package dto.dw;

import lombok.Data;

@Data
public class DWSCustomerAdd {
    private String key;
    private String dateYM;
    private int count;
}
