package dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlateLoadDTO implements Serializable {
    private static final long serialVersionUID = 5629730505902635695L;
    private long id;
    private long timestamp;
    private BigDecimal power;
    private BigDecimal i;
    private int pileType;
    private int chargingCount;
    private String gmtCreated;
}
