package dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 * @author: niehy
 * @createDate: 2023/3/28
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PileStateDTO {
    private Long id;
    private String operatorId;
    private String operatorName;
    private Integer stationId;
    private String stationCode;
    private String stationName;
    private String pileCode;
    private String gunCode;
    private Integer state;
    private String stateMessage;
    private String logTime;
    private String gmtCreate;
    private String currentType;
    private String protocol;

    public static void main(String[] args) {
        String value = "3-10-01 00:00:20.452";
        if (StringUtils.isNotEmpty(value) && value.split("-")[0].length() < 2) {
            value = "2" + value;
        }
        System.out.println(value);
    }

    private static String fitLogTime(String logTime) {
        if (logTime.contains(".")) {
            return logTime.split("\\.")[0];
        }
        return logTime;
    }
}
