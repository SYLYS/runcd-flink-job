package dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 消息数据
 * @Author: lyhong
 * @Date: 2022/6/10 14:17
 **/
@Data
public class KafkaDataDTO implements Serializable {

    public static final Integer UPDATE = 1;

    /**
     * @Description: 操作类型
     * INSERT = 0
     * UPDATE = 1
     * DELETE = 2
     * UNRECOGNIZED = -1
     */
    private int dmlType;

    /**
     * 数据库
     **/
    private String dataBase;

    /**
     * 表
     **/
    private String table;

    /**
     * 数据json
     **/
    private String dataJson;
}
