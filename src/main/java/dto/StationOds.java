package dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 充电站
 * </p>
 *
 * @author zsz
 * @since 2019-07-14
 */
@Data
public class StationOds implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 场站主键ID
     */
    @JsonProperty("id")
    private Integer id;

    /**
     * 场站编码
     */
    @JsonProperty("station_code")
    private String stationCode;

    /**
     * 场站名称
     */
    @JsonProperty("station_name")
    private String stationName;


    /**
     * 省
     */
    @JsonProperty("province")
    private String province;

    /**
     * 市
     */
    @JsonProperty("city")
    private String city;

    /**
     * 区
     */
    @JsonProperty("area")
    private String area;


}
