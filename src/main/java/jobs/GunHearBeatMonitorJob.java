package jobs;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import config.EnvHolder;
import dto.HeartBeatDTO;
import enums.EnvEnum;
import enums.NotifyGroupEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import utli.NotifyUtil;

import java.time.Instant;
import java.util.*;

/**
 * 桩号设置重复检测
 * 利用桩号的心跳机制
 * 根据心跳的时间 时分+桩号为 key，统计次数 超过 n 次触发钉钉告警
 */
@Slf4j
public class GunHearBeatMonitorJob {
    private static String brokers;
    private static String topic;
    private static String group;

    private static void init(String profile) {
        if ("prod".equals(profile)) {
            brokers = "172.16.16.4:9092";
        } else {
            brokers = "106.52.85.243:9092";
        }
        topic = "realtime-event";
        group = "GunHearBeatMonitorJob";
        EnvHolder.setCurrentEnv(EnvEnum.getEnum(profile));
        log.info("当前 kafka 配置 brokers:{} topic:{} group:{}", brokers, topic, group);
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }

        String profile = args[0];
        init(profile);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(120000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(5000);
        env.getCheckpointConfig().setCheckpointTimeout(120000);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.setParallelism(1);
        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, group + "_" + RandomUtil.randomNumbers(8) + "_" + Instant.now().getEpochSecond());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "120000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "60000");

        KafkaSource<String> source = KafkaSource.<String>builder().setBootstrapServers(brokers).setTopics(topic).setGroupId(group).setProperties(props)
                .setDeserializer(KafkaRecordDeserializationSchema.valueOnly(StringDeserializer.class)).setStartingOffsets(OffsetsInitializer.latest()).build();

        DataStreamSource<String> dataStreamSource = env.fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");

        WatermarkStrategy<HeartBeatDTO> watermarkStrategy = WatermarkStrategy
                .<HeartBeatDTO>forMonotonousTimestamps()
                .withTimestampAssigner((event, timestamp) -> System.currentTimeMillis());

        SingleOutputStreamOperator<HeartBeatDTO> streamOperator = dataStreamSource.map(new MapFunction<String, HeartBeatDTO>() {
                    @Override
                    public HeartBeatDTO map(String value) {
                        if (!value.contains("接收") && !value.contains("心跳")) {
                            return null;
                        }

                        JSONObject jsonObject = JSONObject.parseObject(value);
//                        String message = jsonObject.getString("message");
//                        message = message.substring(message.lastIndexOf("】") + 1).trim();
                        String pileCode = jsonObject.getString("guncode");
                        String logTime = jsonObject.getString("logtime");
                        Date date = strToDateTime("20" + logTime);
                        if (date == null) {
                            return null;
                        }
//                        byte[] bytes = EncodeUtil.hexStringToByteArray(message);
//                        Map<String, Object> decodeMap = DecodeUtil.protocol2Map(IotDecode.GUN_HEARTBEAT_STATE, bytes);
//                        String gunCode = (String) decodeMap.getOrDefault("充电桩编号", "") + decodeMap.get("充电枪类型") + decodeMap.get("充电枪序号");
                        return new HeartBeatDTO(pileCode, DateTime.of(date).toTimeStr().substring(0, 5));
                    }
                }).filter(Objects::nonNull)
                .assignTimestampsAndWatermarks(watermarkStrategy)
                .name("数据解析");

        streamOperator.keyBy(key -> key.getPileCode() + key.getTimeStr())
                .countWindow(10)
                .process(new ProcessWindowFunction<HeartBeatDTO, Object, String, GlobalWindow>() {
                    @Override
                    public void process(String s, ProcessWindowFunction<HeartBeatDTO, Object, String, GlobalWindow>.Context context,
                                        Iterable<HeartBeatDTO> elements, Collector<Object> out) {
                        List<HeartBeatDTO> list = (List<HeartBeatDTO>) elements;
                        if (!list.isEmpty()) {
                            NotifyUtil.send(NotifyGroupEnum.GUN_ORDER_REPEAT_UPLOAD, "桩号设置重复：\n" + list.get(0).getPileCode()
                                    + "\n触发时间：" + list.get(0).getTimeStr() +
                                    "\n一分钟内心跳触发 10 次");
                        }
                    }
                })
                .name("一分钟内心跳触发 10 次");

        env.execute("桩号设置重复检测");
    }

    public static Date strToDateTime(String dateTimeStr) {
        try {
            if (dateTimeStr.contains(".") || dateTimeStr.length() == 14) {
                // 20230214092053.255  20230214092053
                return DateUtil.parse(dateTimeStr, DatePattern.PURE_DATETIME_PATTERN);
            }
            if (dateTimeStr.length() == 18) {
                // 202302140920531515
                return DateUtil.parse(dateTimeStr, DatePattern.PURE_DATETIME_MS_PATTERN);
            }
            if (dateTimeStr.contains("-")) {
                // yyyy-MM-dd HH:mm:ss
                return DateUtil.parse(dateTimeStr, DatePattern.NORM_DATETIME_PATTERN);
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }
}
