package jobs;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import config.DataSourceInfoUtil;
import config.EnvHolder;
import config.conf.KafkaConf;
import dto.KafkaDataDTO;
import dto.dw.DWCustomerInfo;
import enums.EnvEnum;
import function.KafkaConsumerRecordDeserializationSchema;
import function.KafkaDataMapFunctionSink;
import jobs.device.DeviceMysqlBinlogToKafka;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.connector.base.DeliveryGuarantee;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.config.SaslConfigs;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.Properties;

/**
 * @author: niehy
 * @createDate: 2023/3/23
 * @Description:
 */
@Slf4j
public class CustomerMysqlBinlogToKafka {

    private static String tencentBrokers;
    private static String tencentTopic;
    private static String tencentGroup;
    private static String testBrokers;
    private static String testTopic;
    //redis参数
    private static String redisHost;
    private static String redisPassword;

    @Data
    static class Account {
        private String username;
        private String password;
    }

    private static Account init(String profile) {
        Account account = new Account();
        if ("prod".equals(profile)) {
            tencentBrokers = "guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129";
            tencentTopic = "topic-subs-dfco31hqn8-cdb-fw6ajmnb";
            tencentGroup = "consumer-grp-subs-dfco31hqn8-cmd202";
            account.setUsername("account-subs-dfco31hqn8-cmd202");
            account.setPassword("runcd");
        } else if ("dev".equals(profile) || "test".equals(profile)) {
            tencentBrokers = "guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129";
            tencentTopic = "topic-subs-7impax4gci-cdb-f3as145r";
            tencentGroup = "consumer-grp-subs-7impax4gci-customer_info";
            account.setUsername("account-subs-7impax4gci-nhy_test");
            account.setPassword("nhy_test");

            testBrokers = "106.52.85.243:9092";
            testTopic = "dwd_device_pile_info_df";
        } else {
            tencentBrokers = "guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129";
            tencentTopic = "topic-subs-7impax4gci-cdb-f3as145r";
            tencentGroup = "consumer-grp-subs-7impax4gci-test-group";
            account.setUsername("account-subs-7impax4gci-runcd");
            account.setPassword("runcd");

            testBrokers = "106.52.85.243:9092";
            testTopic = "dwd_device_pile_info_df";
        }
        EnvHolder.setCurrentEnv(EnvEnum.getEnum(profile));
        log.info("当前 kafka 配置 brokers:{} topic:{} group:{}", tencentBrokers, tencentTopic, tencentGroup);
        return account;
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
        KafkaConf tencentKafkaConf = DataSourceInfoUtil.getTencentKafkaConf(profile);
        //1.创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(5000L);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setCheckpointTimeout(1000);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));
        env.setParallelism(1);

        //2、配置kafka环境
        Properties props = new Properties();
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, tencentGroup + "_" + Instant.now().getEpochSecond());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "60000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");

        KafkaSource<ConsumerRecord<String, byte[]>> source = KafkaSource.<ConsumerRecord<String, byte[]>>builder()
                .setBootstrapServers(tencentKafkaConf.getBrokers())
                .setTopics(tencentKafkaConf.getTopic())
                .setGroupId(tencentKafkaConf.getGroup())
                .setProperties(props)
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setProperty("commit.offsets.on.checkpoint", "true")
                .setProperty("security.protocol", "SASL_PLAINTEXT")
                .setProperty(SaslConfigs.SASL_MECHANISM, "SCRAM-SHA-512")
                .setProperty(SaslConfigs.SASL_JAAS_CONFIG, "org.apache.kafka.common.security.scram.ScramLoginModule required "
                        + "  username=\"" + tencentKafkaConf.getUsername() + "\"" + "  password=\"" + tencentKafkaConf.getPassword() + "\";")
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setDeserializer(KafkaRecordDeserializationSchema.of(new KafkaConsumerRecordDeserializationSchema())).build();

        //3.指定消费的策略
        DataStreamSource<ConsumerRecord<String, byte[]>> text = env.fromSource(source, WatermarkStrategy.noWatermarks(), "kafka source");

        //4.数据转换
        SingleOutputStreamOperator<DWCustomerInfo> outputStreamOperator = text
                .map(new KafkaDataMapFunctionSink()).name("数据转换")
                .flatMap(new FlatMapFunction<ArrayList<KafkaDataDTO>, KafkaDataDTO>() {
                    @Override
                    public void flatMap(ArrayList<KafkaDataDTO> kafkaDataDTOS, Collector<KafkaDataDTO> out) {
                        if (CollectionUtils.isEmpty(kafkaDataDTOS)) {
                            return;
                        }
                        kafkaDataDTOS.stream().filter(Objects::nonNull).forEach(kafkaDataDTO -> {
                            if (StringUtils.equals(kafkaDataDTO.getTable(), "t_customer_app")) {
                                out.collect(kafkaDataDTO);
                            }
                        });
                    }
                }).name("数据过滤")
                .flatMap(new CustomerDetailInfoFlatMapFunction()).name("用户信息存入redis");

        KafkaSink<String> kafkaSink = KafkaSink.<String>builder()
                .setBootstrapServers(testBrokers)
                .setRecordSerializer(KafkaRecordSerializationSchema.builder()
                        .setTopic(testTopic)
                        .setValueSerializationSchema(new SimpleStringSchema())
                        .build()
                )
                .setDeliveryGuarantee(DeliveryGuarantee.AT_LEAST_ONCE)
                .build();

        //将设备信息放入kafka中进行后续消费
        outputStreamOperator.map(JSONObject::toJSONString).sinkTo(kafkaSink);

        String simpleName = CustomerMysqlBinlogToKafka.class.getSimpleName();
        env.execute(simpleName);
    }

    private static class CustomerDetailInfoFlatMapFunction implements FlatMapFunction<KafkaDataDTO, DWCustomerInfo> {

        @Override
        public void flatMap(KafkaDataDTO kafkaDataDTO, Collector<DWCustomerInfo> out) throws Exception {
            log.info("会员信息更新：{}", kafkaDataDTO.getDataJson());
            JSONArray jsonArray = JSON.parseArray(kafkaDataDTO.getDataJson());
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                DWCustomerInfo customerInfo = new DWCustomerInfo();
                customerInfo.setId(jsonObject.getIntValue("id"));
                customerInfo.setCustomerId(jsonObject.getString("customer_id"));
                customerInfo.setPhone(jsonObject.getString("phone"));
                customerInfo.setOpId(jsonObject.getString("phone"));
                customerInfo.setRegisterTime(jsonObject.getString("register_time"));
                customerInfo.setRegisterResource(jsonObject.getString("register_resource"));

                //将新增操作放入kafka进行统计
                if (kafkaDataDTO.getDmlType() == 0) {
                    out.collect(customerInfo);
                }
            }
        }
    }

}
