package jobs.device;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import config.DataSourceInfoUtil;
import config.EnvHolder;
import config.conf.KafkaConf;
import config.conf.RedisConf;
import dto.KafkaDataDTO;
import dto.dim.*;
import enums.EnvEnum;
import enums.RedisKeyEnum;
import function.KafkaConsumerRecordDeserializationSchema;
import function.KafkaDataMapFunctionSink;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.base.DeliveryGuarantee;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.config.SaslConfigs;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import utli.EnumUtil;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * @author: niehy
 * @createDate: 2023/3/23
 * @Description:
 */
@Slf4j
public class DeviceMysqlBinlogToKafka {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
        KafkaConf tencentKafkaConf = DataSourceInfoUtil.getTencentKafkaConf(profile);

        //1.创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(5000L);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setCheckpointTimeout(1000);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));
        env.setParallelism(1);

        //设置重启机制,任务失败三次重启
        env.setRestartStrategy(RestartStrategies.failureRateRestart(3, Time.of(2, TimeUnit.SECONDS),Time.of(2, TimeUnit.SECONDS)));

        //2、配置kafka环境
        Properties props = new Properties();
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, tencentKafkaConf.getGroup() + "_" + Instant.now().getEpochSecond());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "60000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");

        KafkaSource<ConsumerRecord<String, byte[]>> source = KafkaSource.<ConsumerRecord<String, byte[]>>builder()
                .setBootstrapServers(tencentKafkaConf.getBrokers())
                .setTopics(tencentKafkaConf.getTopic())
                .setGroupId(tencentKafkaConf.getGroup())
                .setProperties(props)
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setProperty("commit.offsets.on.checkpoint", "true")
                .setProperty("security.protocol", "SASL_PLAINTEXT")
                .setProperty(SaslConfigs.SASL_MECHANISM, "SCRAM-SHA-512")
                .setProperty(SaslConfigs.SASL_JAAS_CONFIG, "org.apache.kafka.common.security.scram.ScramLoginModule required "
                        + "  username=\"" + tencentKafkaConf.getUsername() + "\"" + "  password=\"" + tencentKafkaConf.getPassword() + "\";")
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setDeserializer(KafkaRecordDeserializationSchema.of(new KafkaConsumerRecordDeserializationSchema())).build();

        //3.指定消费的策略
        DataStreamSource<ConsumerRecord<String, byte[]>> text = env.fromSource(source, WatermarkStrategy.noWatermarks(), "kafka source");

        //4.数据转换
        SingleOutputStreamOperator<ArrayList<KafkaDataDTO>> listSingleOutputStreamOperator = text
                .map(new KafkaDataMapFunctionSink()).name("数据转换");
        SingleOutputStreamOperator<KafkaDataDTO> kafkaDataDTOSingleOutputStreamOperator = listSingleOutputStreamOperator
                .flatMap(new FlatMapFunction<ArrayList<KafkaDataDTO>, KafkaDataDTO>() {
                    @Override
                    public void flatMap(ArrayList<KafkaDataDTO> kafkaDataDTOS, Collector<KafkaDataDTO> out) {
                        if (CollectionUtils.isEmpty(kafkaDataDTOS)) {
                            return;
                        }
                        kafkaDataDTOS.stream().filter(Objects::nonNull).forEach(kafkaDataDTO -> {
                            if (StringUtils.equals(kafkaDataDTO.getTable(), "t_operator")
                                    || StringUtils.equals(kafkaDataDTO.getTable(), "t_station")
                                    || StringUtils.equals(kafkaDataDTO.getTable(), "t_pile_type")
                                    || StringUtils.equals(kafkaDataDTO.getTable(), "t_pile")) {
                                out.collect(kafkaDataDTO);
                            }
                        });
                    }
                }).name("数据过滤");

        //5、将operator维度数据存入redis
        SingleOutputStreamOperator<KafkaDataDTO> operatorFunc = kafkaDataDTOSingleOutputStreamOperator
                .flatMap(new OperatorDetailInfoFlatMapFunction()).name("operator维度信息存入redis");

        //6、将station维度数据存入redis
        SingleOutputStreamOperator<KafkaDataDTO> stationFunc = operatorFunc
                .flatMap(new StationDetailInfoFlatMapFunction()).name("station维度信息存入redis");

        //7、将pileType维度数据存入redis
        SingleOutputStreamOperator<KafkaDataDTO> pileTypeFunc = stationFunc
                .flatMap(new PileTypeDetailInfoFlatMapFunction()).name("pileType维度信息存入redis");

        //8、将pile维度数据存入redis
        SingleOutputStreamOperator<DIMPileInfo> pileFunc = pileTypeFunc
                .flatMap(new PileDetailInfoFlatMapFunction()).name("pile维度信息存入redis");

        KafkaConf kafkaConf = DataSourceInfoUtil.getKafkaConf(profile);
        kafkaConf.setTopic("dwd_device_pile_info_df");
        KafkaSink<String> kafkaSink = KafkaSink.<String>builder()
                .setBootstrapServers(kafkaConf.getBrokers())
                .setRecordSerializer(KafkaRecordSerializationSchema.builder()
                        .setTopic(kafkaConf.getTopic())
                        .setValueSerializationSchema(new SimpleStringSchema())
                        .build()
                )
                .setDeliveryGuarantee(DeliveryGuarantee.AT_LEAST_ONCE)
                .build();

        //将设备信息放入kafka中进行后续消费
        pileFunc.map(JSONObject::toJSONString).sinkTo(kafkaSink);

        String simpleName = DeviceMysqlBinlogToKafka.class.getSimpleName();
        env.execute(simpleName);
    }

    private static class PileDetailInfoFlatMapFunction extends RichFlatMapFunction<KafkaDataDTO, DIMPileInfo> {
        private Jedis jedis;
        private transient int errorCount = 0;
        private RedisConf redisConf;

        @Override
        public void open(Configuration configuration) throws Exception {
            initParameters();
            // 创建JedisPool连接池
            JedisPoolConfig config = new JedisPoolConfig();
            JedisPool jedisPool = new JedisPool(config, redisConf.getHost(), redisConf.getPort(), 3 * 1000, redisConf.getPassword());
            jedis = jedisPool.getResource();
        }

        private void initParameters() {
            ExecutionConfig config = getRuntimeContext().getExecutionConfig();
            String profile = config.getGlobalJobParameters().toMap().get("profile");
            redisConf = DataSourceInfoUtil.getRedisConf(profile);
        }

        @Override
        public void close() throws Exception {
            if (jedis != null) {
                jedis.close();
            }
        }

        @Override
        public void flatMap(KafkaDataDTO kafkaDataDTO, Collector<DIMPileInfo> out) throws Exception {
            if ("t_pile".equals(kafkaDataDTO.getTable())) {
                log.info("桩息更新：{}", kafkaDataDTO.getDataJson());
                try {
                    JSONArray jsonArray = JSON.parseArray(kafkaDataDTO.getDataJson());
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        DIMPileInfo pileInfo = new DIMPileInfo();
                        //1、数据组装
                        pileInfo.setId(jsonObject.getLongValue("id"));
                        pileInfo.setPileCode(jsonObject.getString("pile_code"));
                        pileInfo.setPileProtocol(jsonObject.getString("protocol"));
                        pileInfo.setPileName(jsonObject.getString("name"));
                        pileInfo.setOpId(jsonObject.getString("operator_id"));
                        pileInfo.setStId(jsonObject.getIntValue("station_id"));
                        pileInfo.setPileTypeId(jsonObject.getIntValue("pile_type_id"));
                        pileInfo.setPileCreateDate(jsonObject.getString("create_date"));
                        pileInfo.setIsDeleted(jsonObject.getString("deleted"));
                        //2、数据补齐
                        String opObj = jedis.get(String.format(RedisKeyEnum.DIM_OPERATOR_DETAIL_DF.getKeyPrefix(), pileInfo.getOpId()));
                        String stObj = jedis.get(String.format(RedisKeyEnum.DIM_STATION_ID_DETAIL_DF.getKeyPrefix(), pileInfo.getStId()));
                        String pileTypeObj = jedis.get(String.format(RedisKeyEnum.DIM_PILE_TYPE_DETAIL_DF.getKeyPrefix(), pileInfo.getPileTypeId()));
                        if (opObj != null && stObj != null && pileTypeObj != null) {
                            pileInfo.setOpName(JSON.parseObject(opObj, DIMOperatorInfo.class).getOpName());
                            pileInfo.setStCode(JSON.parseObject(stObj, DIMStationInfo.class).getStCode());
                            pileInfo.setStName(JSON.parseObject(stObj, DIMStationInfo.class).getStName());
                            pileInfo.setStProvince(JSON.parseObject(stObj, DIMStationInfo.class).getStProvince());
                            pileInfo.setStCity(JSON.parseObject(stObj, DIMStationInfo.class).getStCity());
                            pileInfo.setStArea(JSON.parseObject(stObj, DIMStationInfo.class).getStArea());
                            pileInfo.setCurrentType(JSON.parseObject(pileTypeObj, DIMPileTypeInfo.class).getCurrentType());
                            pileInfo.setRatedPower(JSON.parseObject(pileTypeObj, DIMPileTypeInfo.class).getRatedPower());
                            pileInfo.setRatedV(JSON.parseObject(pileTypeObj, DIMPileTypeInfo.class).getRatedV());
                            pileInfo.setRatedI(JSON.parseObject(pileTypeObj, DIMPileTypeInfo.class).getRatedI());
                            //数据存入redis
                            jedis.set(String.format(RedisKeyEnum.DIM_PILE_DETAIL_DF.getKeyPrefix(), pileInfo.getPileCode()), JSON.toJSONString(pileInfo));
                        } else {
                            errorCount++;
                            System.out.println("数据缺失：," + JSON.toJSONString(pileInfo));
                            System.out.println("当前总条数:" + errorCount);
                        }
                        //将新增操作放入kafka进行统计
                        if (kafkaDataDTO.getDmlType() == 0) {
                            out.collect(pileInfo);
                        }
                    }
                } catch (Exception e) {
                    log.info("桩数据清洗报错 | 入参：{},报错信息:{}", JSONObject.toJSONString(kafkaDataDTO), e.getMessage());
                }
            }
        }
    }

    private static class OperatorDetailInfoFlatMapFunction extends RichFlatMapFunction<KafkaDataDTO, KafkaDataDTO> {
        private Jedis jedis;

        private RedisConf redisConf;

        @Override
        public void open(Configuration configuration) throws Exception {
            initParameters();
            // 创建JedisPool连接池
            JedisPoolConfig config = new JedisPoolConfig();
            JedisPool jedisPool = new JedisPool(config, redisConf.getHost(), redisConf.getPort(), 3 * 1000, redisConf.getPassword());
            jedis = jedisPool.getResource();
        }

        private void initParameters() {
            ExecutionConfig config = getRuntimeContext().getExecutionConfig();
            String profile = config.getGlobalJobParameters().toMap().get("profile");
            redisConf = DataSourceInfoUtil.getRedisConf(profile);
        }

        @Override
        public void close() throws Exception {
            if (jedis != null) {
                jedis.close();
            }
        }

        @Override
        public void flatMap(KafkaDataDTO kafkaDataDTO, Collector<KafkaDataDTO> out) throws Exception {
            if ("t_operator".equals(kafkaDataDTO.getTable())) {
                log.info("运营商信息更新：{}", kafkaDataDTO.getDataJson());
                try {
                    JSONArray jsonArray = JSON.parseArray(kafkaDataDTO.getDataJson());
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        DIMOperatorInfo operatorInfo = new DIMOperatorInfo();
                        //1、数据组装
                        operatorInfo.setOpId(jsonObject.getString("id"));
                        operatorInfo.setOpName(jsonObject.getString("name"));
                        operatorInfo.setOpCreateDate(jsonObject.getString("create_date"));
                        operatorInfo.setState(jsonObject.getString("state"));
                        //3、数据存入redis
                        jedis.set(String.format(RedisKeyEnum.DIM_OPERATOR_DETAIL_DF.getKeyPrefix(), operatorInfo.getOpId()), JSON.toJSONString(operatorInfo));
                    }
                } catch (Exception e) {
                    log.info("运营商数据清洗报错 | 入参：{},报错信息:{}", JSONObject.toJSONString(kafkaDataDTO), e.getMessage());
                }
            } else {
                //非运营商信息输出
                out.collect(kafkaDataDTO);
            }
        }
    }

    private static class StationDetailInfoFlatMapFunction extends RichFlatMapFunction<KafkaDataDTO, KafkaDataDTO> {
        private Jedis jedis;
        private RedisConf redisConf;

        @Override
        public void open(Configuration configuration) throws Exception {
            initParameters();
            // 创建JedisPool连接池
            JedisPoolConfig config = new JedisPoolConfig();
            JedisPool jedisPool = new JedisPool(config, redisConf.getHost(), redisConf.getPort(), 3 * 1000, redisConf.getPassword());
            jedis = jedisPool.getResource();
        }

        private void initParameters() {
            ExecutionConfig config = getRuntimeContext().getExecutionConfig();
            String profile = config.getGlobalJobParameters().toMap().get("profile");
            redisConf = DataSourceInfoUtil.getRedisConf(profile);
        }

        @Override
        public void close() throws Exception {
            if (jedis != null) {
                jedis.close();
            }
        }

        @Override
        public void flatMap(KafkaDataDTO kafkaDataDTO, Collector<KafkaDataDTO> out) throws Exception {
            if ("t_station".equals(kafkaDataDTO.getTable())) {
                log.info("站点信息更新：{}", kafkaDataDTO.getDataJson());
                try {
                    JSONArray jsonArray = JSON.parseArray(kafkaDataDTO.getDataJson());
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        DIMStationInfo stationInfo = new DIMStationInfo();
                        //1、数据组装
                        stationInfo.setStId(jsonObject.getIntValue("id"));
                        stationInfo.setStCode(jsonObject.getString("station_code"));
                        stationInfo.setStName(jsonObject.getString("station_name"));
                        stationInfo.setOpId(jsonObject.getString("operator_id"));
                        stationInfo.setStProvince(jsonObject.getString("province"));
                        stationInfo.setStCity(jsonObject.getString("city"));
                        stationInfo.setStArea(jsonObject.getString("area"));
                        stationInfo.setStCreateDate(jsonObject.getString("create_date"));
                        stationInfo.setIsDeleted(jsonObject.getString("deleted"));
                        stationInfo.setStStatus(EnumUtil.queryStationStatus(jsonObject.getString("station_status")));
                        stationInfo.setStAddressType(EnumUtil.queryStationAddressType(jsonObject.getString("address_type")));
                        stationInfo.setStBusinessType(EnumUtil.queryStationBusinessType(jsonObject.getString("business_type")));

                        //2、数据补充
                        String opNameObj = jedis.get(String.format(RedisKeyEnum.DIM_OPERATOR_DETAIL_DF.getKeyPrefix(), stationInfo.getOpId()));
                        if (opNameObj != null) {
                            stationInfo.setOpName(JSON.parseObject(opNameObj, DIMOperatorInfo.class).getOpName());
                        }
                        //3、数据存入redis
                        jedis.set(String.format(RedisKeyEnum.DIM_STATION_DETAIL_DF.getKeyPrefix(), stationInfo.getStCode()), JSON.toJSONString(stationInfo));
                        jedis.set(String.format(RedisKeyEnum.DIM_STATION_ID_DETAIL_DF.getKeyPrefix(), stationInfo.getStId()), JSON.toJSONString(stationInfo));
                    }
                } catch (Exception e) {
                    log.info("运营商数据清洗报错 | 入参：{},报错信息:{}", JSONObject.toJSONString(kafkaDataDTO), e.getMessage());
                }
            } else {
                //非运营商信息输出
                out.collect(kafkaDataDTO);
            }
        }

    }

    public static String queryStationStatus(String stationStatusId) {
        String stationStatus = "";
        switch (stationStatusId) {
            case "0":
                stationStatus = "未知";
                break;
            case "1":
                stationStatus = "建设中";
                break;
            case "5":
                stationStatus = "关闭下线";
                break;
            case "6":
                stationStatus = "维护中";
                break;
            case "50":
                stationStatus = "正常使用";
                break;
            default:
                stationStatus = "未知";
                break;
        }
        return stationStatus;
    }

    public static String getCurrentType(String currentTypeId) {
        String currentType = "";
        switch (currentTypeId) {
            case "1":
                currentType = "直流";
                break;
            case "2":
                currentType = "交流";
                break;
            case "3":
                currentType = "电单车";
                break;
            case "4":
                currentType = "换电柜";
                break;
            case "5":
                currentType = "分体桩";
                break;
            case "6":
                currentType = "三相交流";
                break;
            case "7":
                currentType = "液冷桩";
                break;
            default:
                currentType = "未知";
                break;
        }
        return currentType;
    }

    private static class PileTypeDetailInfoFlatMapFunction extends RichFlatMapFunction<KafkaDataDTO, KafkaDataDTO> {
        private Jedis jedis;

        private RedisConf redisConf;

        @Override
        public void open(Configuration configuration) throws Exception {
            initParameters();
            // 创建JedisPool连接池
            JedisPoolConfig config = new JedisPoolConfig();
            JedisPool jedisPool = new JedisPool(config, redisConf.getHost(), redisConf.getPort(), 3 * 1000, redisConf.getPassword());
            jedis = jedisPool.getResource();
        }

        private void initParameters() {
            ExecutionConfig config = getRuntimeContext().getExecutionConfig();
            String profile = config.getGlobalJobParameters().toMap().get("profile");
            redisConf = DataSourceInfoUtil.getRedisConf(profile);
        }

        @Override
        public void close() throws Exception {
            if (jedis != null) {
                jedis.close();
            }
        }

        @Override
        public void flatMap(KafkaDataDTO kafkaDataDTO, Collector<KafkaDataDTO> out) throws Exception {
            if ("t_pile_type".equals(kafkaDataDTO.getTable())) {
                log.info("设备型号信息更新：{}", kafkaDataDTO.getDataJson());
                JSONArray jsonArray = JSON.parseArray(kafkaDataDTO.getDataJson());
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    DIMPileTypeInfo pileTypeInfo = new DIMPileTypeInfo();
                    //1、组装数据
                    pileTypeInfo.setPileTypeId(jsonObject.getIntValue("id"));
                    pileTypeInfo.setTypeModel(jsonObject.getString("type_model"));
                    pileTypeInfo.setManufacturer(jsonObject.getString("manufactor"));
                    pileTypeInfo.setCurrentType(getCurrentType(jsonObject.getString("current_type")));
                    pileTypeInfo.setRatedPower(jsonObject.getBigDecimal("rated_power"));
                    pileTypeInfo.setRatedV(jsonObject.getBigDecimal("output_voltage_range"));
                    pileTypeInfo.setRatedI(jsonObject.getBigDecimal("output_current_range"));
                    pileTypeInfo.setPileProtocol(jsonObject.getString("protocol"));
                    pileTypeInfo.setIsDeleted(jsonObject.getString("deleted"));
                    //2、存入redis
                    jedis.set(String.format(RedisKeyEnum.DIM_PILE_TYPE_DETAIL_DF.getKeyPrefix(), pileTypeInfo.getPileTypeId()), JSON.toJSONString(pileTypeInfo));
                }
            } else {
                //非运营商信息输出
                out.collect(kafkaDataDTO);
            }
        }
    }
}
