package jobs.device;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import config.EnvHolder;
import dto.dim.DIMPileInfo;
import enums.EnvEnum;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Properties;

@Slf4j
@Data
public class DeviceInfoJob {

    //kafka参数
    private static String brokers;
    private static String topic;
    private static String group;

    //redis参数
    private static String redisHost;
    private static String redisPassword;

    //初始化数据表
    private static boolean isInitTable = false;
    private static boolean isInitRedis = false;


    private static void init(String profile) {
        if ("prod".equals(profile)) {
            brokers = "172.16.16.4:9092";
            topic = "dwd_device_pile_info_df";
            group = "grp-dwd-device-pile-info";

            redisHost = "gz-crs-gw3nq3eq.sql.tencentcdb.com";
            redisPassword = "JSSbDE4b#DufZg*&k%jVKCvse4BQ2!";
        } else if ("test".equals(profile)) {
            brokers = "106.52.85.243:9092";
            topic = "dwd_device_pile_info_df";
            group = "grp-dwd-device-pile-info";

            redisHost = "gz-crs-gw3nq3eq.sql.tencentcdb.com";
            redisPassword = "JSSbDE4b#DufZg*&k%jVKCvse4BQ2!";
        } else {
            brokers = "106.52.85.243:9092";
            topic = "dwd_device_pile_info_df";
            group = "grp-dwd-device-pile-info";

            redisHost = "gz-crs-gw3nq3eq.sql.tencentcdb.com";
            redisPassword = "JSSbDE4b#DufZg*&k%jVKCvse4BQ2!";
        }
        EnvHolder.setCurrentEnv(EnvEnum.getEnum(profile));
        log.info("当前 kafka 配置 brokers:{} topic:{} group:{} redisHost:{} redisPassword:{}", brokers, topic, group,redisHost,redisPassword);
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }else if (args.length == 2){
            isInitRedis = Boolean.parseBoolean(args[1]);
        }else if (args.length == 3){
            isInitRedis = Boolean.parseBoolean(args[2]);
            isInitTable = Boolean.parseBoolean(args[1]);
        }
        String profile = args[0];
        init(profile);

        //1.创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.enableCheckpointing(30000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(5000);
        env.getCheckpointConfig().setCheckpointTimeout(120000);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));
        env.setParallelism(1);

        //2.添加数据源
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, group + "_" + RandomUtil.randomNumbers(8) + "_" + Instant.now().getEpochSecond());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "120000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "10000");
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "10000");
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, "30000");
        props.put(ConsumerConfig.FETCH_MIN_BYTES_CONFIG, "5000000");
        props.put(ConsumerConfig.FETCH_MAX_WAIT_MS_CONFIG, "5000");

        KafkaSource<String> source = KafkaSource.<String>builder()
                .setBootstrapServers(brokers)
                .setTopics(topic)
                .setGroupId(group)
                .setProperties(props)
                .setDeserializer(KafkaRecordDeserializationSchema.valueOnly(StringDeserializer.class))
                .setStartingOffsets(OffsetsInitializer.earliest())
                .build();

        DataStreamSource<String> dataStreamSource = env
                .fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");

        SingleOutputStreamOperator<DIMPileInfo> outputStreamOperator = dataStreamSource
                .filter((FilterFunction<String>) s -> s != null).name("过滤空值")
                .flatMap(new FlatMapFunction<String, DIMPileInfo>() {
                    @Override
                    public void flatMap(String s, Collector<DIMPileInfo> collector) throws Exception {
                        DIMPileInfo pileInfo = JSON.parseObject(s, DIMPileInfo.class);
                        collector.collect(pileInfo);
                    }
                }).name("数据转换")
                .filter(pileInfo -> pileInfo.getId() != 0).name("过滤id为0")
                .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<DIMPileInfo>(Time.seconds(1)) {
                    @Override
                    public long extractTimestamp(DIMPileInfo pileInfo) {
                        LocalDateTime dateTime = LocalDateTime.parse(pileInfo.getPileCreateDate(), DateTimeFormatter.ofPattern(DatePattern.NORM_DATETIME_PATTERN));
                        ZonedDateTime zonedDateTime = dateTime.atZone(ZoneId.systemDefault());
                        return zonedDateTime.toInstant().toEpochMilli();
                    }
                }).name("添加水印");

        SingleOutputStreamOperator<DIMPileInfo> processed = outputStreamOperator
                .map(new MapFunction<DIMPileInfo, Tuple2<String, DIMPileInfo>>() {
                    @Override
                    public Tuple2<String, DIMPileInfo> map(DIMPileInfo pileInfo) throws Exception {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        String f0 = format.format(format.parse(pileInfo.getPileCreateDate()));
                        return new Tuple2<>(f0, pileInfo);
                    }
                }).name("数据转换成tuple2")
                .process(new PileAddProcessFunc()).name("数据存入redis");

        processed.print();

        String jobName = DeviceInfoJob.class.getSimpleName();
        env.execute(jobName);
    }

    private static class PileAddProcessFunc extends ProcessFunction<Tuple2<String, DIMPileInfo>, DIMPileInfo> {
        //声明Redis连接
        private Jedis jedis;

        //声明每个窗口总人数的key
        private String deviceAddDayKey = "DeviceDayAdd";
        private String bitMapKey = "BitMap_deviceNo";

        private String redisHost;
        private String redisPassword;
        private int redisPort;

        @Override
        public void open(Configuration parameters) throws Exception {
            //初始化redis参数
            initParameters();
            // 创建JedisPool连接池
            JedisPoolConfig config = new JedisPoolConfig();
            JedisPool jedisPool = new JedisPool(config, redisHost, redisPort, 3 * 1000, redisPassword);
            jedis = jedisPool.getResource();

            if (isInitRedis){
                jedis.del(deviceAddDayKey);
                jedis.del(bitMapKey);
            }
        }

        private void initParameters() {
            ExecutionConfig config = getRuntimeContext().getExecutionConfig();
            String profile = config.getGlobalJobParameters().toMap().get("profile");
            if (EnvEnum.PROD.toString().equals(profile)) {
                redisHost = "172.16.48.8";
                redisPassword = "nA8xgK&GBPuWHHjgaAQaB%2$qBnZ#K";
                redisPort = 6379;
            } else if (EnvEnum.UAT.toString().equals(profile)) {
                redisHost = "172.16.48.8";
                redisPassword = "nA8xgK&GBPuWHHjgaAQaB%2$qBnZ#K";
                redisPort = 6379;
            } else if (EnvEnum.TEST.toString().equals(profile)) {
                redisHost = "gz-crs-gw3nq3eq.sql.tencentcdb.com";
                redisPassword = "JSSbDE4b#DufZg*&k%jVKCvse4BQ2!";
                redisPort = 28840;
            } else if (EnvEnum.DEV.toString().equals(profile)) {
                redisHost = "gz-crs-gw3nq3eq.sql.tencentcdb.com";
                redisPassword = "JSSbDE4b#DufZg*&k%jVKCvse4BQ2!";
                redisPort = 28840;
            }
        }

        @Override
        public void close() throws Exception {
            super.close();
            // 在 close 方法中关闭 Redis 连接
            if (jedis != null) {
                jedis.close();
            }
        }

        @Override
        public void processElement(Tuple2<String, DIMPileInfo> value, ProcessFunction<Tuple2<String, DIMPileInfo>, DIMPileInfo>.Context ctx, Collector<DIMPileInfo> out) throws Exception {
            //1.取出数据
            DIMPileInfo pileInfo = value.f1;
            //2.提取窗口信息
            String windowEnd = value.f0;
            //3.定义当前窗口的BitMap Key
//            String bitMapKey = bitMapKey;
            //4.查询当前的UID是否已经存在于当前的bitMap中
            Long offset = pileInfo.getId();
            if (offset == null) {
                return;
            }
            Boolean exists = jedis.getbit(bitMapKey, offset);

            //5.根据数据是否存在做下一步操作
            if (!exists) {
                //将对应offset位置改为1
                jedis.setbit(bitMapKey, offset, true);
                //累加当前窗口的综合
                jedis.hincrBy(deviceAddDayKey, windowEnd, 1);
                out.collect(pileInfo);
            }
            //输出数据
//            String hget = jedis.hget(deviceAddDayKey, windowEnd);
        }
    }
}

