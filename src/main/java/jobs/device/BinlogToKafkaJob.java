package jobs.device;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import config.DataSourceInfoUtil;
import config.conf.KafkaConf;
import dto.KafkaDataDTO;
import function.KafkaConsumerRecordDeserializationSchema;
import function.KafkaDataFlatMapFunctionSink;
import function.KafkaDataMapFunctionSink;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.connector.base.DeliveryGuarantee;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.config.SaslConfigs;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;

@Slf4j
public class BinlogToKafkaJob {

    private static String brokers;
    private static String topic;
    private static String group;

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
        KafkaConf kafkaConf = DataSourceInfoUtil.getTencentKafkaConf(profile);

        //1.创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.enableCheckpointing(30000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(5000);
        env.getCheckpointConfig().setCheckpointTimeout(120000);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);

        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));
        env.setParallelism(1);

        //2.添加数据源
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConf.getBrokers());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaConf.getGroup());
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, kafkaConf.getGroup() + "_" + RandomUtil.randomNumbers(8) + "_" + Instant.now().getEpochSecond());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "120000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "10000");
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "10000");
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, "30000");
        props.put(ConsumerConfig.FETCH_MIN_BYTES_CONFIG, "5000000");
        props.put(ConsumerConfig.FETCH_MAX_WAIT_MS_CONFIG, "5000");

        if (StringUtils.isNotBlank(kafkaConf.getUsername()) && StringUtils.isNotBlank(kafkaConf.getPassword())) {
            props.put("security.protocol", "SASL_PLAINTEXT");
            props.put(SaslConfigs.SASL_MECHANISM, "SCRAM-SHA-512");
            props.put(SaslConfigs.SASL_JAAS_CONFIG, "org.apache.kafka.common.security.scram.ScramLoginModule required " + "  username=\"" + kafkaConf.getUsername() + "\"" + "  password=\"" + kafkaConf.getPassword() + "\";");
        }

        log.info("---消费者开始创建 props: {}", JSONObject.toJSONString(props));
        KafkaSource<ConsumerRecord<String, byte[]>> source = KafkaSource.<ConsumerRecord<String, byte[]>>builder()
                .setBootstrapServers(kafkaConf.getBrokers())
                .setTopics(kafkaConf.getTopic())
                .setGroupId(kafkaConf.getGroup())
                .setProperties(props)
                .setStartingOffsets(OffsetsInitializer.committedOffsets())
                .setDeserializer(KafkaRecordDeserializationSchema.of(new KafkaConsumerRecordDeserializationSchema())).build();

        //指定消费的策略
        DataStreamSource<ConsumerRecord<String, byte[]>> text = env.fromSource(source, WatermarkStrategy.noWatermarks(), "kafka source");

        DataStream<ArrayList<KafkaDataDTO>> dataMap = text.map(new KafkaDataMapFunctionSink());

        SingleOutputStreamOperator<KafkaDataDTO> outputStreamOperator = dataMap.flatMap(new KafkaDataFlatMapFunctionSink())
                .filter(new FilterFunction<KafkaDataDTO>() {
                    @Override
                    public boolean filter(KafkaDataDTO kafkaDataDTO) throws Exception {
                        boolean flag = kafkaDataDTO != null && ObjectUtils.allNotNull(kafkaDataDTO);
                        return flag;
                    }
                }).name("数据过滤打平")
                .filter(new FilterFunction<KafkaDataDTO>() {
                    @Override
                    public boolean filter(KafkaDataDTO value) throws Exception {
                        boolean isNotNull = value != null && ObjectUtils.allNotNull(value);
                        boolean flag = isNotNull && ("t_operator".equals(value.getTable()) || "t_station".equals(value.getTable())
                                || "t_pile".equals(value.getTable()) || "t_pile_type".equals(value.getTable()));
                        return isNotNull && flag;
                    }
                }).name("过滤非资源表数据");

        SingleOutputStreamOperator<String> streamOperator = outputStreamOperator
                .flatMap(new FlatMapFunction<KafkaDataDTO, String>() {
                    @Override
                    public void flatMap(KafkaDataDTO kafkaDataDTO, Collector<String> collector) throws Exception {
                        collector.collect(JSONObject.toJSONString(kafkaDataDTO));
                    }
                }).name("数据格式转换");
        streamOperator.print();

        KafkaSink<String> sink = KafkaSink.<String>builder()
                .setBootstrapServers("106.52.85.243:9092")
                .setRecordSerializer(KafkaRecordSerializationSchema.builder()
                        .setTopic("binlog-to-jsonobj-topic")
                        .setValueSerializationSchema(new SimpleStringSchema())
                        .build()
                )
                .setDeliveryGuarantee(DeliveryGuarantee.AT_LEAST_ONCE)
                .build();

        streamOperator.sinkTo(sink);

        String jobName = BinlogToKafkaJob.class.getSimpleName();
        env.execute(jobName);
    }
}
