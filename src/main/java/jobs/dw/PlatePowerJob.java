package jobs.dw;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import config.DataSourceInfoUtil;
import config.conf.ClickHouseConf;
import config.conf.KafkaConf;
import config.conf.RedisConf;
import dto.PlateLoadDTO;
import dto.dim.DIMPileInfo;
import enums.RedisKeyEnum;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.redis.RedisSink;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommand;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommandDescription;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisMapper;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import utli.MessageAnalysisUtil;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

@Slf4j
public class PlatePowerJob {

    private static String group;
    private static String topic;
    private static Snowflake snowflake = new Snowflake(1, 1);

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
//        init(profile);
        //获取redis配置信息
        RedisConf redisConf = DataSourceInfoUtil.getRedisConf(profile);
        KafkaConf kafkaConf = DataSourceInfoUtil.getKafkaConf(profile);
        topic = "realtime-event";
        group = "plate_power_monitor_job";
        kafkaConf.setTopic(topic);
        kafkaConf.setGroup(group);

        //1.创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.enableCheckpointing(30000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(5000);
        env.getCheckpointConfig().setCheckpointTimeout(120000);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));
        env.setParallelism(1);

        //2.添加数据源
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConf.getBrokers());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaConf.getGroup());
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, kafkaConf.getGroup() + "_" + RandomUtil.randomNumbers(8) + "_" + Instant.now().getEpochSecond());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "120000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "10000");
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "10000");
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, "30000");
        props.put(ConsumerConfig.FETCH_MIN_BYTES_CONFIG, "5000000");
        props.put(ConsumerConfig.FETCH_MAX_WAIT_MS_CONFIG, "5000");

        KafkaSource<String> source = KafkaSource.<String>builder()
                .setBootstrapServers(kafkaConf.getBrokers())
                .setTopics(kafkaConf.getTopic())
                .setGroupId(kafkaConf.getGroup())
                .setProperties(props)
                .setDeserializer(KafkaRecordDeserializationSchema.valueOnly(StringDeserializer.class))
                .setStartingOffsets(OffsetsInitializer.latest())
                .build();

        DataStreamSource<String> dataStreamSource = env
                .fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");
        WatermarkStrategy<PlateLoadDTO> watermarkStrategy = WatermarkStrategy.<PlateLoadDTO>forMonotonousTimestamps()
                .withTimestampAssigner((event, timestamp) -> event.getTimestamp());

        //进行数据过滤
        SingleOutputStreamOperator<PlateLoadDTO> outputStreamOperator = dataStreamSource.map(new MapFunction<String, Map<String, Object>>() {
                    @Override
                    public Map<String, Object> map(String jsonvalue) throws ParseException {
                        Map<String, Object> objectMap = MessageAnalysisUtil.dataToRealData(jsonvalue);
                        return objectMap;
                    }
                }).name("数据转换")
                .filter(map -> map != null).name("数据过滤")
                .flatMap(new RichFlatMapFunction<Map<String, Object>, Map<String, Object>>() {
                    private Jedis jedis;

                    private RedisConf redisConf;

                    @Override
                    public void open(Configuration configuration) throws Exception {
                        super.open(configuration);
                        //初始化redis参数
                        initParameters();
                        // 创建JedisPool连接池
                        JedisPoolConfig config = new JedisPoolConfig();
                        JedisPool jedisPool = new JedisPool(config, redisConf.getHost(), redisConf.getPort(), 3 * 1000, redisConf.getPassword());
                        jedis = jedisPool.getResource();
                    }

                    private void initParameters() {
                        ExecutionConfig config = getRuntimeContext().getExecutionConfig();
                        String profile = config.getGlobalJobParameters().toMap().get("profile");
                        redisConf = DataSourceInfoUtil.getRedisConf(profile);
                    }

                    @Override
                    public void close() throws Exception {
                        if (jedis != null) {
                            jedis.close();
                        }
                    }

                    @Override
                    public void flatMap(Map<String, Object> value, Collector<Map<String, Object>> out) throws Exception {
                        String pileCode = value.get("pileCode").toString();
                        String pileStr = jedis.get(String.format(RedisKeyEnum.DIM_PILE_DETAIL_DF.getKeyPrefix(), pileCode));
                        if (StringUtils.isNotBlank(pileStr)) {
                            DIMPileInfo pileInfo = JSON.parseObject(pileStr, DIMPileInfo.class);
                            String pileProtocol = pileInfo.getPileProtocol();
                            if ("RUNCD".equals(pileProtocol)) {
                                out.collect(value);
                            }
                        }
                    }
                }).name("过滤非runcd协议数据")
                .flatMap(new FlatMapFunction<Map<String, Object>, PlateLoadDTO>() {

                    @Override
                    public void flatMap(Map<String, Object> objectMap, Collector<PlateLoadDTO> out) throws Exception {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat parse = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
                        PlateLoadDTO plateLoadDTO = new PlateLoadDTO();
                        plateLoadDTO.setId(snowflake.nextId());
                        plateLoadDTO.setPower(BigDecimal.valueOf((Integer) objectMap.get("充电功率")).divide(BigDecimal.valueOf(100)).setScale(2));
                        plateLoadDTO.setI(BigDecimal.valueOf((Integer) objectMap.get("充电电流")).divide(BigDecimal.valueOf(100)).setScale(2));
                        plateLoadDTO.setPileType((Integer) objectMap.get("充电枪类型"));
                        String gmtCreate = "20" + objectMap.get("time").toString();
                        plateLoadDTO.setGmtCreated(sdf.format(sdf.parse(gmtCreate)));
                        plateLoadDTO.setTimestamp(parse.parse(objectMap.get("time").toString()).getTime());
                        out.collect(plateLoadDTO);
                    }
                }).name("数据转换")
                .assignTimestampsAndWatermarks(watermarkStrategy).name("添加水印");

        SingleOutputStreamOperator<PlateLoadDTO> streamOperator = outputStreamOperator
                .keyBy(p -> p.getPileType())
                .window(TumblingProcessingTimeWindows.of(Time.seconds(15)))
                .allowedLateness(Time.seconds(20))
                .apply(new applyFunction())
                .process(new processFunction());
//                .reduce(new PlateLoadReduceFunction(), new PlateLoadProcessFunction());

        FlinkJedisPoolConfig config = new FlinkJedisPoolConfig.Builder()
                .setHost(redisConf.getHost())
                .setPassword(redisConf.getPassword())
                .setPort(redisConf.getPort())
                .build();

        streamOperator.addSink(new RedisSink<>(config, new RedisMapper<PlateLoadDTO>() {
            @Override
            public RedisCommandDescription getCommandDescription() {
                return new RedisCommandDescription(RedisCommand.SET);
            }

            @Override
            public String getKeyFromData(PlateLoadDTO plateLoadDTO) {
                return String.format("plate:load:%s", plateLoadDTO.getGmtCreated());
            }

            @Override
            public String getValueFromData(PlateLoadDTO plateLoadDTO) {
                return JSONObject.toJSONString(plateLoadDTO);
            }
        }));

        String jobName = PlatePowerJob.class.getSimpleName();
        env.execute(jobName);
    }

    private static class PlateLoadProcessFunction extends ProcessWindowFunction<PlateLoadDTO, PlateLoadDTO, Integer, TimeWindow> {

        @Override
        public void process(Integer integer, ProcessWindowFunction<PlateLoadDTO, PlateLoadDTO, Integer, TimeWindow>.Context context, Iterable<PlateLoadDTO> elements, Collector<PlateLoadDTO> out) throws Exception {
            elements.forEach(out::collect);
        }
    }

    private static class PlateLoadReduceFunction implements ReduceFunction<PlateLoadDTO> {
        @Override
        public PlateLoadDTO reduce(PlateLoadDTO value1, PlateLoadDTO value2) throws Exception {
            return new PlateLoadDTO(value1.getId(), value1.getTimestamp(), value1.getPower().add(value2.getPower()), value1.getI().add(value2.getI()), value1.getPileType(), 0, value1.getGmtCreated());
        }
    }

    public static class applyFunction implements WindowFunction<PlateLoadDTO, PlateLoadDTO, Integer, TimeWindow> {
        @Override
        public void apply(Integer key, TimeWindow window, Iterable<PlateLoadDTO> input, Collector<PlateLoadDTO> out) throws Exception {
            PlateLoadDTO plateLoadDTO = new PlateLoadDTO(0, 0, BigDecimal.ZERO, BigDecimal.ZERO, key, 0, null);
            input.forEach(p -> {
                plateLoadDTO.setId(p.getId());
                plateLoadDTO.setTimestamp(window.getEnd());
                plateLoadDTO.setPower(p.getPower().add(plateLoadDTO.getPower()));
                plateLoadDTO.setI(p.getI().add(plateLoadDTO.getI()));
                plateLoadDTO.setChargingCount(plateLoadDTO.getChargingCount() + 1);
                plateLoadDTO.setGmtCreated(p.getGmtCreated());
            });
//            log.info("聚合后plateLoadDTO:{}", plateLoadDTO);
            out.collect(plateLoadDTO);
        }
    }

    public static class processFunction extends ProcessFunction<PlateLoadDTO, PlateLoadDTO> {
        private ClickHouseConf ckConf;
        private transient Connection connection;

        @Override
        public void processElement(PlateLoadDTO value, ProcessFunction<PlateLoadDTO, PlateLoadDTO>.Context ctx, Collector<PlateLoadDTO> out) throws Exception {
            savePlateLoad(value);
            out.collect(value);
        }


        @Override
        public void open(Configuration parameters) throws Exception {
            super.open(parameters);
            log.info("dwd_plate_load_i_rt 数据库连接");
            //初始化ck参数
            initParameters();
            //创建连接
            initConnection();
        }

        private void initParameters() {
            ExecutionConfig config = getRuntimeContext().getExecutionConfig();
            String profile = config.getGlobalJobParameters().toMap().get("profile");
            ckConf = DataSourceInfoUtil.getClickHouseSingleConf(profile, "_dw");
        }

        private void savePlateLoad(PlateLoadDTO elements) throws SQLException {
            String sql = "INSERT INTO dwd_plate_load_i_rt (id, timestamp,power, i, pile_type,charging_count,gmt_created) VALUES (?,?,?,?,?,?,?)";
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(sql);
            if (elements != null) {
                preparedStatement.setLong(1, elements.getId());
                preparedStatement.setLong(2, elements.getTimestamp());
                preparedStatement.setBigDecimal(3, elements.getPower());
                preparedStatement.setBigDecimal(4, elements.getI());
                preparedStatement.setInt(5, elements.getPileType());
                preparedStatement.setInt(6, elements.getChargingCount());
                preparedStatement.setString(7, elements.getGmtCreated());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        }

        public void initConnection() throws ClassNotFoundException, SQLException {
            Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
            connection = DriverManager.getConnection(ckConf.getUrl(), ckConf.getUsername(), ckConf.getPassword());
        }

        @Override
        public void close() throws Exception {
            connection.close();
            super.close();
        }
    }
}
