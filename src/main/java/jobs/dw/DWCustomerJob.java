package jobs.dw;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import config.EnvHolder;
import dto.dim.DIMOperatorInfo;
import dto.dw.DWCustomerInfo;
import enums.EnvEnum;
import enums.RedisKeyEnum;
import lombok.Cleanup;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;
import org.apache.flink.util.Collector;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class DWCustomerJob {
    private static String brokers;
    private static String topic;
    private static String group;

    private static Snowflake snowflake = IdUtil.getSnowflake(1, 2);

    @Data
    static class Account {
        private String username;
        private String password;
    }

    private static Account init(String profile) {
        Account account = new Account();
        if ("prod".equals(profile)) {
            brokers = "guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129";
            topic = "topic-subs-dfco31hqn8-cdb-fw6ajmnb";
            group = "consumer-grp-subs-dfco31hqn8-cmd202";
            account.setUsername("account-subs-dfco31hqn8-cmd202");
            account.setPassword("runcd");
        } else if ("test".equals(profile)||"dev".equals(profile)) {
            brokers = "106.52.85.243:9092";
            topic = "binlog-to-jsonobj-topic";
            group = "grp-binlog-to-jsonobj";
        } else {
            brokers = "guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129";
            topic = "topic-subs-7impax4gci-cdb-f3as145r";
            group = "consumer-grp-subs-7impax4gci-test-group";
            account.setUsername("account-subs-7impax4gci-runcd");
            account.setPassword("runcd");
        }
        EnvHolder.setCurrentEnv(EnvEnum.getEnum(profile));
        log.info("当前 kafka 配置 brokers:{} topic:{} group:{}", brokers, topic, group);
        return account;
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
        Account account = init(profile);

        //获取flink的运行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //checkpoint配置
        env.enableCheckpointing(1000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setCheckpointTimeout(1000);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));
        env.setParallelism(1);

        DataStreamSource<DWCustomerInfo> customerInfoDataStreamSource = env.addSource(new MySQLSourceFunction());

        SingleOutputStreamOperator<DWCustomerInfo> outputStreamOperator = customerInfoDataStreamSource.process(new RedisProcessFunction()).name("数据清洗补充");

        SingleOutputStreamOperator<DWCustomerInfo> singleOutputStreamOperator = outputStreamOperator.process(new ProcessFunction<DWCustomerInfo, DWCustomerInfo>() {
            private transient Connection connection;
            private transient PreparedStatement preparedStatement;
//            private transient Snowflake snowflake;

            private transient int count = 0;
            private transient int totalCount = 0;
            private transient int sqlCount = 0;



            List<DWCustomerInfo> customerInfos = new ArrayList<>();

            @Override
            public void open(Configuration parameters) throws Exception {
                initConnection("dw");
                log.info("dwd_device_pile_info_i_rt 数据库连接");
//                snowflake = IdUtil.getSnowflake(1, 2);
                super.open(parameters);
            }

            @Override
            public void close() throws Exception {
                if (!customerInfos.isEmpty()){
                    savePileToODS(customerInfos);
                    customerInfos.clear();
                }
                System.out.println("剩余待插入条数："+count);
                System.out.println("计数条数："+totalCount);
                System.out.println("总插入条数："+sqlCount);
                connection.close();
                preparedStatement.close();
                super.close();
            }

            @Override
            public void processElement(DWCustomerInfo customerInfo, ProcessFunction<DWCustomerInfo, DWCustomerInfo>.Context ctx, Collector<DWCustomerInfo> out) throws Exception {
                count++;
                totalCount++;
                customerInfos.add(customerInfo);
                System.out.println("当前条数:"+count);
                if (count >= 100){
                    savePileToODS(customerInfos);
                    customerInfos.clear();
                    count = 0;
                }
                out.collect(customerInfo);
            }

            private void savePileToODS(List<DWCustomerInfo> customerInfos) throws SQLException {
                try {
                    String sql = "INSERT INTO dwd_customer_app_register_df_proxy " +
                            "(id,customer_id,phone,op_id,op_name,register_resource,register_time) " +
                            "VALUES (?,?,?,?,?,?,?)";
                    preparedStatement = connection.prepareStatement(sql);
                    sqlCount = sqlCount + customerInfos.size();
                    for (DWCustomerInfo customerInfo : customerInfos) {
                        preparedStatement.setLong(1, snowflake.nextId());
                        preparedStatement.setString(2,customerInfo.getCustomerId());
                        preparedStatement.setString(3,customerInfo.getPhone());
                        preparedStatement.setString(4,customerInfo.getOpId());
                        preparedStatement.setString(5,customerInfo.getOpName());
                        preparedStatement.setString(6,customerInfo.getRegisterResource());
                        preparedStatement.setString(7,customerInfo.getRegisterTime());
                        preparedStatement.addBatch();
                    }
                    preparedStatement.executeBatch();
                } catch (SQLException e) {
                    System.out.println("插入报错");
                    throw new RuntimeException(e);
                }
            }


            public void initConnection(String dataBase) throws ClassNotFoundException, SQLException {
                ExecutionConfig config = getRuntimeContext().getExecutionConfig();
                String profile = config.getGlobalJobParameters().toMap().get("profile");
                dataBase = profile + "_" + dataBase;
                if ("prod".equals(profile)) {
                    Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
                    String url = "jdbc:clickhouse://172.16.16.4:8123/" + dataBase;
                    connection = DriverManager.getConnection(url, "default", "34p9KgvXDBG!DmEhsn%F6cmAUgf$JS");
                } else if ("uat".equals(profile)) {
                    Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
                    String url = "jdbc:clickhouse://172.16.16.5:8123/" + dataBase;
                    connection = DriverManager.getConnection(url, "default", "d9jE!r4UPZE3t7XWLUMPQQa9!xtNdV");
                } else {
                    Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
                    String url = "jdbc:clickhouse://119.29.28.134:18123/" + dataBase;
                    connection = DriverManager.getConnection(url, "default", "H6kamt7Fhr");
                }
            }

        }).name("数据存入ck");

        String jobName = DWCustomerJob.class.getSimpleName();
        env.execute(jobName);
    }

    public static class MySQLSourceFunction extends RichSourceFunction<DWCustomerInfo> {
        PreparedStatement preparedStatement;
        private Connection connection;

        @Override
        public void open(Configuration parameters) throws Exception {
            super.open(parameters);
            String driver = "com.mysql.cj.jdbc.Driver";
            String url = "jdbc:mysql://gz-cdb-f3as145r.sql.tencentcdb.com:58965/runcd_member?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&allowMultiQueries=true";
            String username = "runcd";
            String password = "rcdtest2021";
            Class.forName(driver);
            connection = DriverManager.getConnection(url, username, password);
            String sql = "select * from t_customer_app;";
            preparedStatement = connection.prepareStatement(sql);
        }

        @Override
        public void close() throws Exception {
            super.close();
            if (connection != null) {
                connection.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }

        @Override
        public void run(SourceContext<DWCustomerInfo> sourceContext) throws Exception {
            try {
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.getConcurrency();
                while (resultSet.next()) {
                    DWCustomerInfo customerInfo = new DWCustomerInfo();
                    customerInfo.setCustomerId(resultSet.getString("customer_id"));
                    customerInfo.setPhone(resultSet.getString("phone"));
                    customerInfo.setOpId(resultSet.getString("operator_id"));
                    customerInfo.setRegisterResource(resultSet.getString("register_resource"));
                    customerInfo.setRegisterTime(resultSet.getString("register_time"));
                    if (resultSet.isLast()){
                        System.out.println("总条数："+resultSet.getRow());
                    }
                    sourceContext.collect(customerInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void cancel() {
        }
    }

    public static class RedisProcessFunction extends ProcessFunction<DWCustomerInfo,DWCustomerInfo> {
        private Jedis jedis;

        @Override
        public void open(Configuration configuration) throws Exception {
            // 创建JedisPool连接池
            JedisPoolConfig config = new JedisPoolConfig();
            JedisPool jedisPool = new JedisPool(config, "gz-crs-gw3nq3eq.sql.tencentcdb.com", 28840, 3 * 1000, "JSSbDE4b#DufZg*&k%jVKCvse4BQ2!");
            jedis = jedisPool.getResource();
        }

        @Override
        public void close() throws Exception {
            if (jedis != null) {
                jedis.close();
            }
        }

        @Override
        public void processElement(DWCustomerInfo customerInfo, ProcessFunction<DWCustomerInfo, DWCustomerInfo>.Context ctx, Collector<DWCustomerInfo> out) throws Exception {
            if (StringUtils.isNotBlank(customerInfo.getOpId())) {
                String opObj = jedis.get(String.format(RedisKeyEnum.DIM_OPERATOR_DETAIL_DF.getKeyPrefix(), customerInfo.getOpId()));
                if (opObj != null){
                    customerInfo.setOpName(JSON.parseObject(opObj, DIMOperatorInfo.class).getOpName());
                }
            }
            out.collect(customerInfo);
        }
    }

}
