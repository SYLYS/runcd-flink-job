//package jobs;
//
//import dto.KafkaDataDTO;
//import dto.RealTimePowerDWD;
//import function.KafkaConsumerRecordDeserializationSchema;
//import function.KafkaDataMapFunctionSink;
//import function.KafkaDataToPowerDWDFunction;
//import function.RealTimeDataClickhouseFunction;
//import org.apache.commons.collections.CollectionUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.flink.api.common.eventtime.WatermarkStrategy;
//import org.apache.flink.api.common.functions.FlatMapFunction;
//import org.apache.flink.connector.kafka.source.KafkaSource;
//import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
//import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
//import org.apache.flink.streaming.api.CheckpointingMode;
//import org.apache.flink.streaming.api.datastream.DataStreamSource;
//import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
//import org.apache.flink.streaming.api.environment.CheckpointConfig;
//import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
//import org.apache.flink.util.Collector;
//import org.apache.kafka.clients.consumer.ConsumerConfig;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.apache.kafka.common.config.SaslConfigs;
//
//import java.time.Instant;
//import java.util.ArrayList;
//import java.util.Objects;
//import java.util.Properties;
//
///**
// * @author: niehy
// * @createDate: 2023/3/23
// * @Description:
// */
//public class Cmd104ToPowerDWDJob {
//
//    public static void main(String[] args) throws Exception {
//        String brokers = EnvConfig.kafkaEvnConf.getBrokers();
//        String userName = EnvConfig.kafkaEvnConf.getUsername();
//        String password = EnvConfig.kafkaEvnConf.getPassword();
//        String topic = "topic-subs-7impax4gci-cdb-f3as145r";
//        String group = "consumer-grp-subs-7impax4gci-test-group";
//
//        //1.创建环境
//        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
//        env.enableCheckpointing(5000L);
//        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
//        env.getCheckpointConfig().setCheckpointTimeout(1000);
//        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
//        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
//        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
//        env.setParallelism(1);
//
//        //2.添加数据源
//        Properties props = new Properties();
//        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
//        props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
//        props.put(ConsumerConfig.CLIENT_ID_CONFIG, group + "_" + Instant.now().getEpochSecond());
//        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
//        props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
//        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
//        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
//        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
//        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "60000");
//        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
//
//        KafkaSource<ConsumerRecord<String, byte[]>> source = KafkaSource.<ConsumerRecord<String, byte[]>>builder()
//                .setBootstrapServers(brokers)
//                .setTopics(topic)
//                .setGroupId(group)
//                .setProperties(props)
//                .setProperty("security.protocol", "SASL_PLAINTEXT")
//                .setProperty(SaslConfigs.SASL_MECHANISM, "SCRAM-SHA-512")
//                .setProperty(SaslConfigs.SASL_JAAS_CONFIG, "org.apache.kafka.common.security.scram.ScramLoginModule required "
//                        + "  username=\"" + userName + "\"" + "  password=\"" + password + "\";")
//                .setStartingOffsets(OffsetsInitializer.committedOffsets())
//                .setDeserializer(KafkaRecordDeserializationSchema.of(new KafkaConsumerRecordDeserializationSchema())).build();
//
//        //3.指定消费的策略
//        DataStreamSource<ConsumerRecord<String, byte[]>> text = env.fromSource(source, WatermarkStrategy.noWatermarks(), "kafka source");
//
//        //4.数据转换
//        SingleOutputStreamOperator<KafkaDataDTO> singleOutputStreamOperator = text.map(new KafkaDataMapFunctionSink()).flatMap(new FlatMapFunction<ArrayList<KafkaDataDTO>, KafkaDataDTO>() {
//            @Override
//            public void flatMap(ArrayList<KafkaDataDTO> kafkaDataDTOS, Collector<KafkaDataDTO> out) {
//                if (CollectionUtils.isEmpty(kafkaDataDTOS)) {
//                    return;
//                }
//                kafkaDataDTOS.stream().filter(Objects::nonNull).forEach(kafkaDataDTO -> {
//                    if (StringUtils.contains(kafkaDataDTO.getTable(), "t_cmd104")) {
//                        out.collect(kafkaDataDTO);
//                    }
//                });
//            }
//        });
//
//        SingleOutputStreamOperator<RealTimePowerDWD> streamOperator = singleOutputStreamOperator.map(new KafkaDataToPowerDWDFunction());
//
//        String sql = "INSERT INTO dwd_real_time_power (`operator_id`,`operator_name`,`station_id`,`station_name`, `pile_code` ,`gun_code`,`province`,`city`,`area`,`v` ,`i` ,`power` ,`dateT` ,`gmt_create`) " +
//                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
////
//        streamOperator.addSink(new RealTimeDataClickhouseFunction(sql));
//
//        streamOperator.print();
//        env.execute();
//    }
//
//}
