package jobs;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import config.EnvHolder;
import dto.Cmd202DTO;
import dto.KafkaDataDTO;
import enums.EnvEnum;
import enums.NotifyGroupEnum;
import function.KafkaConsumerRecordDeserializationSchema;
import function.KafkaDataMapFunctionSink;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.config.SaslConfigs;
import utli.NotifyUtil;

import java.time.Instant;
import java.util.*;

/**
 * 桩订单重复上传检测
 * 3 分钟内，如果桩的 cmd202 订单累计，进行循环对比，如果发现充电开始时间和结束时间有重叠则触发钉钉告警
 */
@Slf4j
public class OrderRepeatMonitorJob {
    private static String brokers;
    private static String topic;
    private static String group;

    @Data
    static class Account {
        private String username;
        private String password;
    }

    private static Account init(String profile) {
        Account account = new Account();
        if ("prod".equals(profile)) {
            brokers = "guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129";
            topic = "topic-subs-dfco31hqn8-cdb-fw6ajmnb";
            group = "consumer-grp-subs-dfco31hqn8-cmd202";
            account.setUsername("account-subs-dfco31hqn8-cmd202");
            account.setPassword("runcd");
        } else {
            brokers = "guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129";
            topic = "topic-subs-7impax4gci-cdb-f3as145r";
            group = "consumer-grp-subs-7impax4gci-test-group";
            account.setUsername("account-subs-7impax4gci-runcd");
            account.setPassword("runcd");
        }
        EnvHolder.setCurrentEnv(EnvEnum.getEnum(profile));
        log.info("当前 kafka 配置 brokers:{} topic:{} group:{}", brokers, topic, group);
        return account;
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
        Account init = init(profile);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(5000L);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setCheckpointTimeout(1000);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.setParallelism(1);

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, group + "_" + Instant.now().getEpochSecond());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "60000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");

        KafkaSource<ConsumerRecord<String, byte[]>> source = KafkaSource.<ConsumerRecord<String, byte[]>>builder()
                .setBootstrapServers(brokers)
                .setTopics(topic)
                .setGroupId(group)
                .setProperties(props)
                .setProperty("security.protocol", "SASL_PLAINTEXT")
                .setProperty(SaslConfigs.SASL_MECHANISM, "SCRAM-SHA-512")
                .setProperty(SaslConfigs.SASL_JAAS_CONFIG, "org.apache.kafka.common.security.scram.ScramLoginModule required "
                        + "  username=\"" + init.getUsername() + "\"" + "  password=\"" + init.getPassword() + "\";")
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setDeserializer(KafkaRecordDeserializationSchema.of(new KafkaConsumerRecordDeserializationSchema())).build();

        DataStreamSource<ConsumerRecord<String, byte[]>> text = env.fromSource(source, WatermarkStrategy.noWatermarks(), "kafka source");
        WatermarkStrategy<Cmd202DTO> watermarkStrategy = WatermarkStrategy.<Cmd202DTO>forMonotonousTimestamps().withTimestampAssigner((event, timestamp) -> System.currentTimeMillis());

        SingleOutputStreamOperator<Cmd202DTO> streamOperator = text.map(new KafkaDataMapFunctionSink()).name("binlog 数据转换").flatMap(new FlatMapFunction<ArrayList<KafkaDataDTO>, KafkaDataDTO>() {
                    @Override
                    public void flatMap(ArrayList<KafkaDataDTO> kafkaDataDTOS, Collector<KafkaDataDTO> out) {
                        if (CollectionUtils.isEmpty(kafkaDataDTOS)) {
                            return;
                        }
                        kafkaDataDTOS.stream().filter(Objects::nonNull).forEach(kafkaDataDTO -> {
                            if (StringUtils.contains(kafkaDataDTO.getTable(), "t_cmd202")) {
                                out.collect(kafkaDataDTO);
                            }
                        });
                    }
                }).name("过滤出 t_cmd202 表")
                .flatMap(new FlatMapFunction<KafkaDataDTO, Cmd202DTO>() {
                    @Override
                    public void flatMap(KafkaDataDTO value, Collector<Cmd202DTO> out) {
                        List<Cmd202DTO> cmd202DTOS = JSONArray.parseArray(value.getDataJson(), Cmd202DTO.class);
                        for (Cmd202DTO cmd202DTO : cmd202DTOS) {
                            Date endTime = strToDateTime(cmd202DTO.getChargeEndTime());
                            Date begTime = strToDateTime(cmd202DTO.getChargeBegTime());
                            cmd202DTO.setBegTime(begTime);
                            cmd202DTO.setEndTime(endTime);
                            out.collect(cmd202DTO);
                        }

                    }
                }).name("json 转实体")
                .assignTimestampsAndWatermarks(watermarkStrategy);

        streamOperator
                .keyBy(Cmd202DTO::getGunCode)
                .window(TumblingEventTimeWindows.of(Time.minutes(3)))
                .process(new ProcessWindowFunction<Cmd202DTO, Cmd202DTO, String, TimeWindow>() {
                    @Override
                    public void process(String s, ProcessWindowFunction<Cmd202DTO, Cmd202DTO, String, TimeWindow>.Context context, Iterable<Cmd202DTO> elements, Collector<Cmd202DTO> out) {
                        List<Cmd202DTO> list = (List<Cmd202DTO>) elements;
                        int count = 0;
                        for (Cmd202DTO cmd202DTO : list) {
                            for (Cmd202DTO dto : list) {
                                if (!cmd202DTO.equals(dto)) {
                                    if ((cmd202DTO.getEndTime() == null && cmd202DTO.getBegTime() == null) ||
                                            DateUtil.isOverlap(cmd202DTO.getBegTime(), cmd202DTO.getEndTime(), dto.getBegTime(), dto.getEndTime())) {
                                        count++;
                                    }
                                }
                            }
                        }
                        if (count >= 1 && !list.isEmpty()) {
                            String msg = String.format("枪号：%s \n 重复订单次数：%d", list.get(0).getGunCode(), count);
                            NotifyUtil.send(NotifyGroupEnum.GUN_ORDER_REPEAT_UPLOAD, msg);
                        }
                    }
                })
                .name("检测是否时间重叠");

        env.execute("桩订单重复上传检测");

    }

    public static Date strToDateTime(String dateTimeStr) {
        try {
            if (dateTimeStr.contains(".") || dateTimeStr.length() == 14) {
                // 20230214092053.255  20230214092053
                return DateUtil.parse(dateTimeStr, DatePattern.PURE_DATETIME_PATTERN);
            }
            if (dateTimeStr.length() == 18) {
                // 202302140920531515
                return DateUtil.parse(dateTimeStr, DatePattern.PURE_DATETIME_MS_PATTERN);
            }
            if (dateTimeStr.contains("-")) {
                // yyyy-MM-dd HH:mm:ss
                return DateUtil.parse(dateTimeStr, DatePattern.NORM_DATETIME_PATTERN);
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }
}
