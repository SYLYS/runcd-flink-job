//package jobs;
//
//import cn.hutool.core.date.DateUtil;
//import cn.hutool.core.util.RandomUtil;
//import com.alibaba.fastjson.JSONObject;
//import decode.IotDecode;
//import dto.OrderDTO;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.flink.api.common.eventtime.WatermarkStrategy;
//import org.apache.flink.api.common.functions.FilterFunction;
//import org.apache.flink.api.common.functions.FlatMapFunction;
//import org.apache.flink.api.common.functions.MapFunction;
//import org.apache.flink.api.java.utils.ParameterTool;
//import org.apache.flink.connector.kafka.source.KafkaSource;
//import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
//import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
//import org.apache.flink.streaming.api.CheckpointingMode;
//import org.apache.flink.streaming.api.datastream.DataStream;
//import org.apache.flink.streaming.api.datastream.DataStreamSource;
//import org.apache.flink.streaming.api.environment.CheckpointConfig;
//import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
//import org.apache.flink.util.Collector;
//import org.apache.kafka.clients.consumer.ConsumerConfig;
//import org.apache.kafka.common.serialization.StringDeserializer;
//import utli.DecodeUtil;
//import utli.EncodeUtil;
//
//import java.time.Instant;
//import java.util.Collections;
//import java.util.Map;
//import java.util.Properties;
//
//@Slf4j
//public class OrderRealtimeMonitorJob {
//    private static String brokers;
//    private static String topic;
//    private static String group;
//
//    private static void init(String profile) {
//        if ("prod".equals(profile)) {
//            brokers = "172.16.16.4:9092";
//        } else {
//            brokers = "106.52.85.243:9092";
//        }
//        topic = "realtime-event";
//        group = "order_monitor_job";
//        log.info("当前 kafka 配置 brokers:{} topic:{} group:{}", brokers, topic, group);
//    }
//
//    public static void main(String[] args) throws Exception {
//        if (args.length == 0) {
//            log.error("请配置启动环境");
//            return;
//        }
//        String profile = args[0];
//        init(profile);
//
//        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
//        env.enableCheckpointing(120000);
//        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
//        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(5000);
//        env.getCheckpointConfig().setCheckpointTimeout(120000);
//        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
//        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
//        env.setParallelism(1);
//        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));
//
//        Properties props = new Properties();
//        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
//        props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
//        props.put(ConsumerConfig.CLIENT_ID_CONFIG, group + "_" + RandomUtil.randomNumbers(8) + "_" + Instant.now().getEpochSecond());
//        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
//        props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
//        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
//        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
//        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
//        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "120000");
//        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "60000");
//
//        KafkaSource<String> source = KafkaSource.<String>builder().setBootstrapServers(brokers).setTopics(topic).setGroupId(group).setProperties(props).setDeserializer(KafkaRecordDeserializationSchema.valueOnly(StringDeserializer.class)).setStartingOffsets(OffsetsInitializer.earliest()).build();
//
//        DataStreamSource<String> dataStreamSource = env.fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");
//        WatermarkStrategy<OrderDTO> watermarkStrategy = WatermarkStrategy.<OrderDTO>forMonotonousTimestamps().withTimestampAssigner((event, timestamp) -> System.currentTimeMillis());
//        DataStream<OrderDTO> accessEventDataStream = dataStreamSource.map(new MapFunction<String, OrderDTO>() {
//                    @Override
//                    public OrderDTO map(String jsonValue) {
//                        if (!jsonValue.contains("接收") || (!jsonValue.contains("交流充电订单") && !jsonValue.contains("直流充电订单"))) {
//                            return null;
//                        }
//                        JSONObject jsonObject = JSONObject.parseObject(jsonValue);
//                        if (jsonObject != null) {
//                            OrderDTO orderDTO = new OrderDTO();
//                            String message = jsonObject.getString("message");
//                            message = message.substring(message.lastIndexOf("】") + 1).trim();
//                            Map<String, Object> decodeMap = null;
//                            if (jsonValue.contains("直流充电订单") && jsonValue.contains("接收")) {
//                                byte[] bytes = EncodeUtil.hexStringToByteArray(message);
//                                decodeMap = DecodeUtil.protocol2Map(IotDecode.DC_ORDER_PROPERTIES, bytes);
//                            } else if (jsonValue.contains("交流充电订单") && jsonValue.contains("接收")) {
//                                byte[] bytes = EncodeUtil.hexStringToByteArray(message);
//                                decodeMap = DecodeUtil.protocol2Map(IotDecode.AC_ORDER_PROPERTIES, bytes);
//                            }
//                            if (decodeMap != null) {
//                                orderDTO.setOrderNo((String) decodeMap.get("业务流水号"));
//                                orderDTO.setUserAccount((String) decodeMap.get("用户账户"));
//                                orderDTO.setPileCode((String) decodeMap.get("充电桩编号"));
//                                orderDTO.setGunCode((String) decodeMap.get("充电桩编号") + decodeMap.get("充电枪类型") + decodeMap.get("充电枪序号"));
//                                orderDTO.setLogTime(jsonObject.getString("logtime"));
//                                return orderDTO;
//                            }
//                        }
//                        return null;
//                    }
//                }).name("数据解析").filter(new FilterFunction<OrderDTO>() {
//                    @Override
//                    public boolean filter(OrderDTO value) {
//                        return value != null;
//                    }
//                }).name("非空过滤").flatMap(new FlatMapFunction<OrderDTO, OrderDTO>() {
//                    @Override
//                    public void flatMap(OrderDTO value, Collector<OrderDTO> out) {
//                        out.collect(value);
//                    }
//                }).assignTimestampsAndWatermarks(watermarkStrategy)
//                .name("订单报文数据转换");
//
//        env.execute();
//    }
//
//
//}
