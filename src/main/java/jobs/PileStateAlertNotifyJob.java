//package jobs;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import config.EnvConfig;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.flink.api.common.eventtime.WatermarkStrategy;
//import org.apache.flink.connector.kafka.source.KafkaSource;
//import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
//import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
//import org.apache.flink.streaming.api.CheckpointingMode;
//import org.apache.flink.streaming.api.datastream.DataStreamSource;
//import org.apache.flink.streaming.api.environment.CheckpointConfig;
//import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
//import org.apache.flink.streaming.api.functions.sink.SinkFunction;
//import org.apache.kafka.clients.consumer.ConsumerConfig;
//import org.apache.kafka.common.config.SaslConfigs;
//import org.apache.kafka.common.serialization.StringDeserializer;
//
//import java.io.IOException;
//import java.io.OutputStream;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.nio.charset.StandardCharsets;
//import java.time.Instant;
//import java.util.List;
//import java.util.Properties;
//
///**
// * 桩离线告警
// */
//public class PileStateAlertNotifyJob {
//    public static void main(String[] args) throws Exception {
//        EnvConfig.init(args[0]);
//
//        String brokers = "172.16.16.4:9092";
//        String topic = "device_offline";
//        String group = "device_offline_flink_job";
//        String username = EnvConfig.kafkaEvnConf.getUsername();
//        String password = EnvConfig.kafkaEvnConf.getPassword();
//
//        //1.创建环境
//        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
//        env.enableCheckpointing(5000L);
//        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
//        env.getCheckpointConfig().setCheckpointTimeout(1000);
//        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
//        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
//        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
//        env.setParallelism(1);
//
//        //2.添加数据源
//        Properties props = new Properties();
//        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
//        props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
//        props.put(ConsumerConfig.CLIENT_ID_CONFIG, group + "_" + Instant.now().getEpochSecond());
//        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
//        props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
//        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
//        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
//        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
//        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "60000");
//        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
//
//        KafkaSource<String> source = KafkaSource.<String>builder()
//                .setBootstrapServers(brokers)
//                .setTopics(topic)
//                .setGroupId(group)
//                .setProperties(props)
//                .setDeserializer(KafkaRecordDeserializationSchema.valueOnly(StringDeserializer.class))
//                .setProperty(SaslConfigs.SASL_MECHANISM, "SCRAM-SHA-512")
//                .setProperty(SaslConfigs.SASL_JAAS_CONFIG, "org.apache.kafka.common.security.scram.ScramLoginModule required "
//                        + "  username=\"" + username + "\"" + "  password=\"" + password + "\";")
//                .setStartingOffsets(OffsetsInitializer.committedOffsets()).build();
//
//        DataStreamSource<String> text = env.fromSource(source, WatermarkStrategy.noWatermarks(), "kafka source");
//
//
//        text.addSink(new SinkFunction<String>() {
//            @Override
//            public void invoke(String value, Context context) {
//                List<JSONObject> jsonObjects = JSON.parseArray(value, JSONObject.class);
//                jsonObjects.forEach(e -> {
//                    String gmtCreate = (String) e.getOrDefault("gmt_create", "");
//                    String operatorName = (String) e.getOrDefault("operatorName", "");
//                    String stationName = (String) e.getOrDefault("stationName", "");
//                    String pileCode = (String) e.getOrDefault("pileCode", "");
//                    if (StringUtils.isNoneEmpty(operatorName) && StringUtils.isNotEmpty(stationName)) {
//                        String msg = "告警名称：离线故障告警\n" +
//                                "运营名称：" + operatorName + "\n" +
//                                "站点名称：" + stationName + "\n" +
//                                "离线桩号：" + pileCode + "\n" +
//                                "告警时间：" + gmtCreate + "\n" +
//                                "告警详情：设备离线超时五分钟";
//                        try {
//                            exec(msg);
//                        } catch (IOException ex) {
//                            throw new RuntimeException(ex);
//                        }
//                    }
//                });
//            }
//        });
//
//
//        env.execute();
//    }
//
//
//    private static void exec(String content) throws IOException {
//        String webhookUrl = EnvConfig.alertHookConf.getUrl();
//
//        URL url = new URL(webhookUrl);
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestMethod("POST");
//        conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
//
//        String message = "{\"msgtype\": \"text\",  \"at\": {\"isAtAll\": true}, \"text\": {\"content\": \"" + content + "\"}}";
//        byte[] postData = message.getBytes(StandardCharsets.UTF_8);
//        conn.setDoOutput(true);
//        try (OutputStream os = conn.getOutputStream()) {
//            os.write(postData);
//            os.flush();
//        }
//
//        conn.getResponseCode();
//    }
//
//}
