package jobs.dim;

import com.alibaba.fastjson.JSONObject;
import config.EnvHolder;
import dto.dim.DIMDateInfo;
import enums.EnvEnum;
import enums.RedisKeyEnum;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;
import org.apache.flink.streaming.connectors.redis.RedisSink;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommand;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommandDescription;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisMapper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;

@Slf4j
public class DIMDateJob {
    private static String brokers;
    private static String topic;
    private static String group;

    @Data
    static class Account {
        private String username;
        private String password;
    }

    private static Account init(String profile) {
        Account account = new Account();
        if ("prod".equals(profile)) {
            brokers = "guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129";
            topic = "topic-subs-dfco31hqn8-cdb-fw6ajmnb";
            group = "consumer-grp-subs-dfco31hqn8-cmd202";
            account.setUsername("account-subs-dfco31hqn8-cmd202");
            account.setPassword("runcd");
        } else if ("test".equals(profile)) {
            brokers = "106.52.85.243:9092";
            topic = "binlog-to-jsonobj-topic";
            group = "grp-binlog-to-jsonobj";
        } else {
            brokers = "guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129";
            topic = "topic-subs-7impax4gci-cdb-f3as145r";
            group = "consumer-grp-subs-7impax4gci-test-group";
            account.setUsername("account-subs-7impax4gci-runcd");
            account.setPassword("runcd");
        }
        EnvHolder.setCurrentEnv(EnvEnum.getEnum(profile));
        log.info("当前 kafka 配置 brokers:{} topic:{} group:{}", brokers, topic, group);
        return account;
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
        Account account = init(profile);

        //获取flink的运行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //checkpoint配置
        env.enableCheckpointing(1000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setCheckpointTimeout(1000);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));
        env.setParallelism(1);

        DataStreamSource<DIMDateInfo> dateInfoDataStreamSource = env.addSource(new RichSourceFunction<DIMDateInfo>() {
            PreparedStatement preparedStatement;
            private Connection connection;

            @Override
            public void open(Configuration parameters) throws Exception {
                super.open(parameters);
                String driver = "com.mysql.cj.jdbc.Driver";
                String url = "jdbc:mysql://gz-cdb-f3as145r.sql.tencentcdb.com:58965/runcd_data?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&allowMultiQueries=true";
                String username = "runcd";
                String password = "rcdtest2021";
                Class.forName(driver);
                connection = DriverManager.getConnection(url, username, password);
                String sql = "select * from time_dimension;";
                preparedStatement = connection.prepareStatement(sql);
            }

            @Override
            public void close() throws Exception {
                super.close();
                if (connection != null) {
                    connection.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            }

            @Override
            public void run(SourceContext<DIMDateInfo> sourceContext) throws Exception {
                try {
                    ResultSet resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        DIMDateInfo dateInfo = new DIMDateInfo();
                        dateInfo.setDateKey(resultSet.getString("date_key"));
                        dateInfo.setDateId(resultSet.getInt("date_id"));
                        dateInfo.setDateY(resultSet.getInt("date_y"));
                        dateInfo.setDateM(resultSet.getInt("date_m"));
                        dateInfo.setDateD(resultSet.getInt("date_d"));
                        dateInfo.setDateYW(resultSet.getInt("date_yw"));
                        dateInfo.setDateW(resultSet.getInt("date_w"));
                        dateInfo.setDateQ(resultSet.getInt("date_q"));
                        dateInfo.setDateWD(resultSet.getInt("date_wd"));
                        dateInfo.setMName(resultSet.getString("m_name"));
                        dateInfo.setWdName(resultSet.getString("wd_name"));
                        sourceContext.collect(dateInfo);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void cancel() {
            }
        });

        dateInfoDataStreamSource.print();

        FlinkJedisPoolConfig config = new FlinkJedisPoolConfig.Builder()
                .setHost("gz-crs-gw3nq3eq.sql.tencentcdb.com")
                .setPassword("JSSbDE4b#DufZg*&k%jVKCvse4BQ2!")
                .setPort(28840)
                .build();
        dateInfoDataStreamSource.addSink(new RedisSink<>(config, new RedisMapper<DIMDateInfo>() {
            @Override
            public RedisCommandDescription getCommandDescription() {
                return new RedisCommandDescription(RedisCommand.SET);
            }

            @Override
            public String getKeyFromData(DIMDateInfo dateInfo) {
                return String.format(RedisKeyEnum.DIM_DATE_DETAIL_DF.getKeyPrefix(),dateInfo.getDateKey());
            }

            @Override
            public String getValueFromData(DIMDateInfo dateInfo) {
                return JSONObject.toJSONString(dateInfo);
            }
        })).name("数据存入redis");

        String jobName = DIMDateJob.class.getSimpleName();
        env.execute(jobName);
    }
}
