package jobs.dim;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import config.DataSourceInfoUtil;
import config.conf.MysqlConf;
import config.conf.RedisConf;
import dto.dim.DIMAddressInfo;
import dto.dim.DIMOperatorInfo;
import enums.RedisKeyEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;
import org.apache.flink.streaming.connectors.redis.RedisSink;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommand;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommandDescription;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisMapper;
import org.apache.flink.util.Collector;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;

@Slf4j
public class DIMOperatorJob {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
        RedisConf redisConf = DataSourceInfoUtil.getRedisConf(profile);

        //获取flink的运行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //checkpoint配置
        env.enableCheckpointing(1000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setCheckpointTimeout(1000);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));
        env.setParallelism(1);

        DataStreamSource<DIMOperatorInfo> operatorInfoDataStreamSource = env.addSource(new RichSourceFunction<DIMOperatorInfo>() {
            private MysqlConf mysqlConf;
            PreparedStatement preparedStatement;
            private Connection connection;
            private String searchDate;

            @Override
            public void open(Configuration parameters) throws Exception {
                super.open(parameters);
                //初始化ck参数
                initParameters();
                //创建连接
                initConnection();
                String sql = "select * from t_operator;";
                preparedStatement = connection.prepareStatement(sql);
            }

            private void initConnection() throws Exception {
                Class.forName(mysqlConf.getDriver());
                connection = DriverManager.getConnection(mysqlConf.getUrl(), mysqlConf.getUsername(), mysqlConf.getPassword());
            }

            private void initParameters() {
                ExecutionConfig config = getRuntimeContext().getExecutionConfig();
                String profile = config.getGlobalJobParameters().toMap().get("profile");
                searchDate = config.getGlobalJobParameters().toMap().get("searchDate");
                mysqlConf = DataSourceInfoUtil.getMysqlConf(profile, "runcd_member");
            }

            @Override
            public void close() throws Exception {
                super.close();
                if (connection != null) {
                    connection.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            }

            @Override
            public void run(SourceContext<DIMOperatorInfo> sourceContext) throws Exception {
                try {
                    ResultSet resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        DIMOperatorInfo operatorInfo = new DIMOperatorInfo();
                        operatorInfo.setOpId(resultSet.getString("id"));
                        operatorInfo.setOpName(resultSet.getString("name"));
                        operatorInfo.setOpCreateDate(resultSet.getString("create_date"));
                        operatorInfo.setState(resultSet.getString("state"));
                        sourceContext.collect(operatorInfo);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void cancel() {
            }
        });

//        SingleOutputStreamOperator<DIMOperatorInfo> outputStreamOperator = operatorInfoDataStreamSource.process(new ProcessFunction<DIMOperatorInfo, DIMOperatorInfo>() {
//            private Jedis jedis;
//            private transient int errorCount = 0;
//            private RedisConf redisConf;
//
//            @Override
//            public void open(Configuration configuration) throws Exception {
//                super.open(configuration);
//                //初始化redis参数
//                initParameters();
//                // 创建JedisPool连接池
//                JedisPoolConfig config = new JedisPoolConfig();
//                JedisPool jedisPool = new JedisPool(config, redisConf.getHost(), redisConf.getPort(), 3 * 1000, redisConf.getPassword());
//                jedis = jedisPool.getResource();
//            }
//
//            private void initParameters() {
//                ExecutionConfig config = getRuntimeContext().getExecutionConfig();
//                String profile = config.getGlobalJobParameters().toMap().get("profile");
//                redisConf = DataSourceInfoUtil.getRedisConf(profile);
//            }
//
//
//            @Override
//            public void close() throws Exception {
//                if (jedis != null) {
//                    jedis.close();
//                }
//            }
//
//            @Override
//            public void processElement(DIMOperatorInfo operatorInfo, ProcessFunction<DIMOperatorInfo, DIMOperatorInfo>.Context ctx, Collector<DIMOperatorInfo> out) throws Exception {
////                String province = jedis.get(String.format(RedisKeyEnum.DIM_ADDRESS_DETAIL_DF.getKeyPrefix(), operatorInfo.getOpProvince()));
////                String city = jedis.get(String.format(RedisKeyEnum.DIM_ADDRESS_DETAIL_DF.getKeyPrefix(), operatorInfo.getOpCity()));
////                String area = jedis.get(String.format(RedisKeyEnum.DIM_ADDRESS_DETAIL_DF.getKeyPrefix(), operatorInfo.getOpArea()));
////                operatorInfo.setOpProvince(JSON.parseObject(province, DIMAddressInfo.class).getName());
////                operatorInfo.setOpCity(JSON.parseObject(city, DIMAddressInfo.class).getName());
////                operatorInfo.setOpArea(JSON.parseObject(area, DIMAddressInfo.class).getName());
//                out.collect(operatorInfo);
//            }
//        });

        operatorInfoDataStreamSource.print();

        FlinkJedisPoolConfig config = new FlinkJedisPoolConfig.Builder()
                .setHost(redisConf.getHost())
                .setPassword(redisConf.getPassword())
                .setPort(redisConf.getPort())
                .build();
        operatorInfoDataStreamSource.addSink(new RedisSink<>(config, new RedisMapper<DIMOperatorInfo>() {
            @Override
            public RedisCommandDescription getCommandDescription() {
                return new RedisCommandDescription(RedisCommand.SET);
            }

            @Override
            public String getKeyFromData(DIMOperatorInfo operatorInfo) {
                return String.format(RedisKeyEnum.DIM_OPERATOR_DETAIL_DF.getKeyPrefix(), operatorInfo.getOpId());
            }

            @Override
            public String getValueFromData(DIMOperatorInfo operatorInfo) {
                return JSONObject.toJSONString(operatorInfo);
            }
        })).name("数据存入redis");

        String jobName = DIMOperatorJob.class.getSimpleName();
        env.execute(jobName);
    }
}
