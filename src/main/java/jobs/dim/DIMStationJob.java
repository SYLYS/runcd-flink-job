package jobs.dim;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import config.DataSourceInfoUtil;
import config.conf.MysqlConf;
import config.conf.RedisConf;
import dto.dim.DIMOperatorInfo;
import dto.dim.DIMStationInfo;
import enums.RedisKeyEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;
import org.apache.flink.streaming.connectors.redis.RedisSink;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommand;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommandDescription;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisMapper;
import org.apache.flink.util.Collector;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import utli.EnumUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;

@Slf4j
public class DIMStationJob {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
        RedisConf redisConf = DataSourceInfoUtil.getRedisConf(profile);

        //获取flink的运行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //checkpoint配置
        env.enableCheckpointing(1000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setCheckpointTimeout(1000);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));
        env.setParallelism(1);

        DataStreamSource<DIMStationInfo> stationInfoDataStreamSource = env.addSource(new RichSourceFunction<DIMStationInfo>() {
            private MysqlConf mysqlConf;
            PreparedStatement preparedStatement;
            private Connection connection;
            private String searchDate;

            @Override
            public void open(Configuration parameters) throws Exception {
                super.open(parameters);
                //初始化ck参数
                initParameters();
                //创建连接
                initConnection();
                String sql = "select * from t_station;";
                preparedStatement = connection.prepareStatement(sql);
            }

            private void initConnection() throws Exception {
                Class.forName(mysqlConf.getDriver());
                connection = DriverManager.getConnection(mysqlConf.getUrl(), mysqlConf.getUsername(), mysqlConf.getPassword());
            }

            private void initParameters() {
                ExecutionConfig config = getRuntimeContext().getExecutionConfig();
                String profile = config.getGlobalJobParameters().toMap().get("profile");
//                searchDate = config.getGlobalJobParameters().toMap().get("searchDate");
                mysqlConf = DataSourceInfoUtil.getMysqlConf(profile, "runcd_res");
            }

            @Override
            public void close() throws Exception {
                super.close();
                if (connection != null) {
                    connection.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            }

            @Override
            public void run(SourceContext<DIMStationInfo> sourceContext) throws Exception {
                int rowCount = 0;
                try {
                    ResultSet resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        rowCount++;
                        DIMStationInfo stationInfo = new DIMStationInfo();
                        stationInfo.setStId(resultSet.getInt("id"));
                        stationInfo.setStCode(resultSet.getString("station_code"));
                        stationInfo.setStName(resultSet.getString("station_name"));
                        stationInfo.setOpId(resultSet.getString("operator_id"));
                        stationInfo.setStProvince(resultSet.getString("province"));
                        stationInfo.setStCity(resultSet.getString("city"));
                        stationInfo.setStArea(resultSet.getString("area"));
                        stationInfo.setStCreateDate(resultSet.getString("create_date"));
                        stationInfo.setIsDeleted(resultSet.getString("deleted"));
                        log.info("查询出的stationInfo信息：{}", JSONObject.toJSONString(stationInfo));
                        stationInfo.setStStatus(resultSet.getString("station_status"));
                        stationInfo.setStAddressType(resultSet.getString("address_type"));
                        stationInfo.setStBusinessType(resultSet.getString("business_type"));
                        sourceContext.collect(stationInfo);
                    }
                    log.info("总条数:{}", rowCount);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.info("出现异常，总条数:{}", rowCount);
                    log.error("查询站点信息异常{}", e.getMessage());
                }
            }

            @Override
            public void cancel() {
            }
        });
        stationInfoDataStreamSource.print();
        SingleOutputStreamOperator<DIMStationInfo> outputStreamOperator = stationInfoDataStreamSource.process(new ProcessFunction<DIMStationInfo, DIMStationInfo>() {
            private Jedis jedis;
            private transient int errorCount = 0;
            private RedisConf redisConf;

            @Override
            public void open(Configuration configuration) throws Exception {
                super.open(configuration);
                //初始化redis参数
                initParameters();
                // 创建JedisPool连接池
                JedisPoolConfig config = new JedisPoolConfig();
                JedisPool jedisPool = new JedisPool(config, redisConf.getHost(), redisConf.getPort(), 3 * 1000, redisConf.getPassword());
                jedis = jedisPool.getResource();
            }

            private void initParameters() {
                ExecutionConfig config = getRuntimeContext().getExecutionConfig();
                String profile = config.getGlobalJobParameters().toMap().get("profile");
                redisConf = DataSourceInfoUtil.getRedisConf(profile);
            }

            @Override
            public void close() throws Exception {
                if (jedis != null) {
                    jedis.close();
                }
            }

            @Override
            public void processElement(DIMStationInfo stationInfo, ProcessFunction<DIMStationInfo, DIMStationInfo>.Context ctx, Collector<DIMStationInfo> out) throws Exception {
                stationInfo.setStStatus(EnumUtil.queryStationStatus(stationInfo.getStStatus()));
                stationInfo.setStAddressType(EnumUtil.queryStationAddressType(stationInfo.getStAddressType()));
                stationInfo.setStBusinessType(EnumUtil.queryStationBusinessType(stationInfo.getStBusinessType()));

                String opNameObj = jedis.get(String.format(RedisKeyEnum.DIM_OPERATOR_DETAIL_DF.getKeyPrefix(), stationInfo.getOpId()));
                if (opNameObj != null) {
                    stationInfo.setOpName(JSON.parseObject(opNameObj, DIMOperatorInfo.class).getOpName());
                    out.collect(stationInfo);
                }
            }
        });

        outputStreamOperator.print();

        FlinkJedisPoolConfig config = new FlinkJedisPoolConfig.Builder()
                .setHost(redisConf.getHost())
                .setPassword(redisConf.getPassword())
                .setPort(redisConf.getPort())
                .build();
        outputStreamOperator.addSink(new RedisSink<>(config, new RedisMapper<DIMStationInfo>() {
            @Override
            public RedisCommandDescription getCommandDescription() {
                return new RedisCommandDescription(RedisCommand.SET);
            }

            @Override
            public String getKeyFromData(DIMStationInfo stationInfo) {
                return String.format(RedisKeyEnum.DIM_STATION_DETAIL_DF.getKeyPrefix(),stationInfo.getStCode());
            }

            @Override
            public String getValueFromData(DIMStationInfo stationInfo) {
                return JSONObject.toJSONString(stationInfo);
            }
        })).name("数据存入redis");

        outputStreamOperator.addSink(new RedisSink<>(config, new RedisMapper<DIMStationInfo>() {
            @Override
            public RedisCommandDescription getCommandDescription() {
                return new RedisCommandDescription(RedisCommand.SET);
            }

            @Override
            public String getKeyFromData(DIMStationInfo stationInfo) {
                return String.format(RedisKeyEnum.DIM_STATION_ID_DETAIL_DF.getKeyPrefix(),stationInfo.getStId());
            }

            @Override
            public String getValueFromData(DIMStationInfo stationInfo) {
                return JSONObject.toJSONString(stationInfo);
            }
        })).name("数据存入redis");

        String jobName = DIMStationJob.class.getSimpleName();
        env.execute(jobName);
    }

    public static String queryStationStatus(String stationStatusId){
        String stationStatus = "";
        switch(stationStatusId){
            case "0":stationStatus = "未知";break;
            case "1":stationStatus = "建设中";break;
            case "5":stationStatus = "关闭下线";break;
            case "6":stationStatus = "维护中";break;
            case "50":stationStatus = "正常使用";break;
            default:stationStatus = "未知";break;
        }
        return stationStatus;
    }

    public static String queryStationAddressType(String stationStatusId){
        String stationStatus = "";
        switch(stationStatusId){
            case "1":stationStatus = "居民区";break;
            case "2":stationStatus = "公共机构";break;
            case "3":stationStatus = "企业事业单位";break;
            case "4":stationStatus = "写字楼";break;
            case "5":stationStatus = "工业园区";break;
            case "6":stationStatus = "交通枢纽";break;
            case "7":stationStatus = "大型文体设施";break;
            case "8":stationStatus = "城市绿地";break;
            case "9":stationStatus = "大型建筑配建停车场";break;
            case "10":stationStatus = "路边停车位";break;
            case "11":stationStatus = "城际高速服务区";break;
            case "100":stationStatus = "新能源充电站";break;
            case "201":stationStatus = "测试用站点";break;
            default:stationStatus = "未知";break;
        }
        return stationStatus;
    }

    public static String queryStationBusinessType(String stationStatusId){
        String stationStatus = "";
        switch(stationStatusId){
            case "OWN":stationStatus = "自营";break;
            case "AGENT":stationStatus = "代理运营";break;
            case "OTHER":stationStatus = "独立运营";break;
            case "REPLACE":stationStatus = "代收款";break;
            default:stationStatus = "未知";break;
        }
        return stationStatus;
    }
}
