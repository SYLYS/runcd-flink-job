package jobs.dim;

import com.alibaba.fastjson.JSONObject;
import config.DataSourceInfoUtil;
import config.conf.MysqlConf;
import config.conf.RedisConf;
import dto.dim.DIMPileTypeInfo;
import enums.RedisKeyEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;
import org.apache.flink.streaming.connectors.redis.RedisSink;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommand;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommandDescription;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisMapper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;

@Slf4j
public class DIMPileTypeJob {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
        RedisConf redisConf = DataSourceInfoUtil.getRedisConf(profile);

        //获取flink的运行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //checkpoint配置
        env.enableCheckpointing(1000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setCheckpointTimeout(1000);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));
        env.setParallelism(1);

        DataStreamSource<DIMPileTypeInfo> pileTypeInfoDataStreamSource = env.addSource(new RichSourceFunction<DIMPileTypeInfo>() {
            private MysqlConf mysqlConf;
            PreparedStatement preparedStatement;
            private Connection connection;
            private String searchDate;

            @Override
            public void open(Configuration parameters) throws Exception {
                super.open(parameters);
                //初始化ck参数
                initParameters();
                //创建连接
                initConnection();
                String sql = "select * from t_pile_type;";
                preparedStatement = connection.prepareStatement(sql);
            }

            private void initConnection() throws Exception {
                Class.forName(mysqlConf.getDriver());
                connection = DriverManager.getConnection(mysqlConf.getUrl(), mysqlConf.getUsername(), mysqlConf.getPassword());
            }

            private void initParameters() {
                ExecutionConfig config = getRuntimeContext().getExecutionConfig();
                String profile = config.getGlobalJobParameters().toMap().get("profile");
                searchDate = config.getGlobalJobParameters().toMap().get("searchDate");
                mysqlConf = DataSourceInfoUtil.getMysqlConf(profile, "runcd_res");
            }

            @Override
            public void close() throws Exception {
                super.close();
                if (connection != null) {
                    connection.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            }

            @Override
            public void run(SourceContext<DIMPileTypeInfo> sourceContext) throws Exception {
                try {
                    ResultSet resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        DIMPileTypeInfo pileTypeInfo = new DIMPileTypeInfo();
                        pileTypeInfo.setPileTypeId(resultSet.getInt("id"));
                        pileTypeInfo.setTypeModel(resultSet.getString("type_model"));
                        pileTypeInfo.setManufacturer(resultSet.getString("manufactor"));
                        pileTypeInfo.setCurrentType(getCurrentType(resultSet.getString("current_type")));
                        pileTypeInfo.setRatedPower(resultSet.getBigDecimal("rated_power"));
                        pileTypeInfo.setRatedV(resultSet.getBigDecimal("output_voltage_range"));
                        pileTypeInfo.setRatedI(resultSet.getBigDecimal("output_current_range"));
                        pileTypeInfo.setPileProtocol(resultSet.getString("protocol"));
                        pileTypeInfo.setIsDeleted(resultSet.getString("deleted"));
                        sourceContext.collect(pileTypeInfo);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void cancel() {
            }
        });
        pileTypeInfoDataStreamSource.print();

        FlinkJedisPoolConfig config = new FlinkJedisPoolConfig.Builder()
                .setHost(redisConf.getHost())
                .setPassword(redisConf.getPassword())
                .setPort(redisConf.getPort())
                .build();
        pileTypeInfoDataStreamSource.addSink(new RedisSink<>(config, new RedisMapper<DIMPileTypeInfo>() {
            @Override
            public RedisCommandDescription getCommandDescription() {
                return new RedisCommandDescription(RedisCommand.SET);
            }

            @Override
            public String getKeyFromData(DIMPileTypeInfo pileTypeInfo) {
                return String.format(RedisKeyEnum.DIM_PILE_TYPE_DETAIL_DF.getKeyPrefix(),pileTypeInfo.getPileTypeId());
            }

            @Override
            public String getValueFromData(DIMPileTypeInfo pileTypeInfo) {
                return JSONObject.toJSONString(pileTypeInfo);
            }
        })).name("数据存入redis");

        String jobName = DIMPileTypeJob.class.getSimpleName();
        env.execute(jobName);
    }

    public static String getCurrentType(String currentTypeId){
        String currentType = "";
        switch(currentTypeId){
            case "1":currentType = "直流";break;
            case "2":currentType = "交流";break;
            case "3":currentType = "电单车";break;
            case "4":currentType = "换电柜";break;
            case "5":currentType = "分体桩";break;
            case "6":currentType = "三相交流";break;
            case "7":currentType = "液冷桩";break;
            default:currentType="未知";break;
        }
        return currentType;
    }
}
