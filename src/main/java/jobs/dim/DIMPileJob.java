package jobs.dim;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import config.DataSourceInfoUtil;
import config.conf.KafkaConf;
import config.conf.MysqlConf;
import config.conf.RedisConf;
import dto.dim.DIMOperatorInfo;
import dto.dim.DIMPileInfo;
import dto.dim.DIMPileTypeInfo;
import dto.dim.DIMStationInfo;
import enums.RedisKeyEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.map.HashedMap;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.base.DeliveryGuarantee;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;
import org.apache.flink.streaming.connectors.redis.RedisSink;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommand;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommandDescription;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisMapper;
import org.apache.flink.util.Collector;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;

@Slf4j
public class DIMPileJob {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
        RedisConf redisConf = DataSourceInfoUtil.getRedisConf(profile);
        KafkaConf kafkaConf = DataSourceInfoUtil.getKafkaConf(profile);
        kafkaConf.setTopic("dwd_device_pile_info_df");

        Map<String, String> singletonMap = new HashedMap();
        singletonMap.put("profile", profile);
        if (args.length > 1) {
            singletonMap.put("searchDate", args[1]);
        }

        //获取flink的运行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //checkpoint配置
        env.enableCheckpointing(30000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setCheckpointTimeout(1000);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(singletonMap));
//        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("searchDate", searchDate)));
        env.setParallelism(1);

        DataStreamSource<DIMPileInfo> pileInfoDataStreamSource = env.addSource(new RichSourceFunction<DIMPileInfo>() {
            private MysqlConf mysqlConf;
            PreparedStatement preparedStatement;
            private Connection connection;
            private String searchDate;

            @Override
            public void open(Configuration parameters) throws Exception {
                super.open(parameters);
                //初始化ck参数
                initParameters();
                //创建连接
                initConnection();
                String sql = "select * from t_pile";
                if (searchDate != null) {
                    sql = sql + " where date_format(create_date,'%Y-%m-%d') = '" + searchDate + "'";
                }
                preparedStatement = connection.prepareStatement(sql);
            }

            private void initConnection() throws Exception {
                Class.forName(mysqlConf.getDriver());
                connection = DriverManager.getConnection(mysqlConf.getUrl(), mysqlConf.getUsername(), mysqlConf.getPassword());
            }

            private void initParameters() {
                ExecutionConfig config = getRuntimeContext().getExecutionConfig();
                String profile = config.getGlobalJobParameters().toMap().get("profile");
                searchDate = config.getGlobalJobParameters().toMap().get("searchDate");
                mysqlConf = DataSourceInfoUtil.getMysqlConf(profile, "runcd_res");
            }

            @Override
            public void close() throws Exception {
                super.close();
                if (connection != null) {
                    connection.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            }

            @Override
            public void run(SourceContext<DIMPileInfo> sourceContext) throws Exception {
                try {
                    ResultSet resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        DIMPileInfo pileInfo = new DIMPileInfo();
                        pileInfo.setId(resultSet.getLong("id"));
                        pileInfo.setPileCode(resultSet.getString("pile_code"));
                        pileInfo.setPileProtocol(resultSet.getString("protocol"));
                        pileInfo.setPileName(resultSet.getString("name"));
                        pileInfo.setOpId(resultSet.getString("operator_id"));
                        pileInfo.setStId(resultSet.getInt("station_id"));
                        pileInfo.setPileTypeId(resultSet.getInt("pile_type_id"));
                        pileInfo.setPileCreateDate(resultSet.getString("create_date"));
                        pileInfo.setIsDeleted(resultSet.getString("deleted"));
                        sourceContext.collect(pileInfo);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void cancel() {
            }
        });

        SingleOutputStreamOperator<DIMPileInfo> outputStreamOperator = pileInfoDataStreamSource.flatMap(new RichFlatMapFunction<DIMPileInfo, DIMPileInfo>() {
            private Jedis jedis;
            private transient int errorCount = 0;
            private RedisConf redisConf;

            @Override
            public void open(Configuration configuration) throws Exception {
                super.open(configuration);
                //初始化redis参数
                initParameters();
                // 创建JedisPool连接池
                JedisPoolConfig config = new JedisPoolConfig();
                JedisPool jedisPool = new JedisPool(config, redisConf.getHost(), redisConf.getPort(), 3 * 1000, redisConf.getPassword());
                jedis = jedisPool.getResource();
            }

            private void initParameters() {
                ExecutionConfig config = getRuntimeContext().getExecutionConfig();
                String profile = config.getGlobalJobParameters().toMap().get("profile");
                redisConf = DataSourceInfoUtil.getRedisConf(profile);
            }

            @Override
            public void close() throws Exception {
                if (jedis != null) {
                    jedis.close();
                }
            }

            @Override
            public void flatMap(DIMPileInfo pileInfo, Collector<DIMPileInfo> out) throws Exception {
                String opObj = jedis.get(String.format(RedisKeyEnum.DIM_OPERATOR_DETAIL_DF.getKeyPrefix(), pileInfo.getOpId()));
                String stObj = jedis.get(String.format(RedisKeyEnum.DIM_STATION_ID_DETAIL_DF.getKeyPrefix(), pileInfo.getStId()));
                String pileTypeObj = jedis.get(String.format(RedisKeyEnum.DIM_PILE_TYPE_DETAIL_DF.getKeyPrefix(), pileInfo.getPileTypeId()));
                if (opObj != null && stObj != null && pileTypeObj != null) {
                    pileInfo.setOpName(JSON.parseObject(opObj, DIMOperatorInfo.class).getOpName());
                    pileInfo.setStCode(JSON.parseObject(stObj, DIMStationInfo.class).getStCode());
                    pileInfo.setStName(JSON.parseObject(stObj, DIMStationInfo.class).getStName());
                    pileInfo.setStProvince(JSON.parseObject(stObj, DIMStationInfo.class).getStProvince());
                    pileInfo.setStCity(JSON.parseObject(stObj, DIMStationInfo.class).getStCity());
                    pileInfo.setStArea(JSON.parseObject(stObj, DIMStationInfo.class).getStArea());
                    pileInfo.setCurrentType(JSON.parseObject(pileTypeObj, DIMPileTypeInfo.class).getCurrentType());
                    pileInfo.setRatedPower(JSON.parseObject(pileTypeObj, DIMPileTypeInfo.class).getRatedPower());
                    pileInfo.setRatedV(JSON.parseObject(pileTypeObj, DIMPileTypeInfo.class).getRatedV());
                    pileInfo.setRatedI(JSON.parseObject(pileTypeObj, DIMPileTypeInfo.class).getRatedI());
                    out.collect(pileInfo);
                } else {
                    errorCount++;
                    log.info("数据缺失：," + JSON.toJSONString(pileInfo));
                    log.info("当前总条数:" + errorCount);
                }
            }
        }).name("数据清洗补全");

//        outputStreamOperator.print();

        FlinkJedisPoolConfig config = new FlinkJedisPoolConfig.Builder()
                .setHost(redisConf.getHost())
                .setPassword(redisConf.getPassword())
                .setPort(redisConf.getPort())
                .build();
        outputStreamOperator.addSink(new RedisSink<>(config, new RedisMapper<DIMPileInfo>() {
            @Override
            public RedisCommandDescription getCommandDescription() {
                return new RedisCommandDescription(RedisCommand.SET);
            }

            @Override
            public String getKeyFromData(DIMPileInfo pileInfo) {
                return String.format(RedisKeyEnum.DIM_PILE_DETAIL_DF.getKeyPrefix(), pileInfo.getPileCode());
            }

            @Override
            public String getValueFromData(DIMPileInfo pileInfo) {
                return JSONObject.toJSONString(pileInfo);
            }
        })).name("数据存入redis");

        KafkaSink<String> kafkaSink = KafkaSink.<String>builder()
                .setBootstrapServers(kafkaConf.getBrokers())
                .setRecordSerializer(KafkaRecordSerializationSchema.builder()
                        .setTopic(kafkaConf.getTopic())
                        .setValueSerializationSchema(new SimpleStringSchema())
                        .build()
                )
                .setDeliveryGuarantee(DeliveryGuarantee.AT_LEAST_ONCE)
                .build();

        //将设备信息放入kafka中进行后续消费
        outputStreamOperator.map(JSONObject::toJSONString).sinkTo(kafkaSink);

        String jobName = DIMPileJob.class.getSimpleName();
        env.execute(jobName);
    }

}
