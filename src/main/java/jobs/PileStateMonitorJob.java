package jobs;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import config.EnvHolder;
import dto.PileStateDTO;
import enums.EnvEnum;
import enums.RedisKeyEnum;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import utli.MessageAnalysisUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author 黄浩
 * 枪各状态的持续时间&利用率（充电中+插枪状态的持续时间占一天的多少）
 */
@Slf4j
public class PileStateMonitorJob {
    private static String brokers;
    private static String topic;
    private static String group;
    private static String redisHost;
    private static String redisPassword;

    private static void init(String profile) {
        if (EnvEnum.PROD.toString().equals(profile)) {
            brokers = "172.16.16.4:9092";
            group = "pile_state_monitor_prod_job";
            redisHost = "172.16.32.12";
            redisPassword = "Vp6Ghb7Z9MS7H59MaGRjbFfF%Jkzkex&";
        } else if (EnvEnum.UAT.toString().equals(profile)) {
            group = "pile_state_monitor_uat_job";
            brokers = "172.16.16.4:9092";
            redisHost = "172.16.48.8";
            redisPassword = "nA8xgK&GBPuWHHjgaAQaB%2$qBnZ#K";
        } else {
            group = "pile_state_monitor_test_job";
            brokers = "106.52.85.243:9092";
//            redisHost = "119.29.194.213";
            redisHost = "192.168.1.74";
            redisPassword = "JSSbDE4b#DufZg*&k%jVKCvse4BQ2!";
        }
        EnvHolder.setCurrentEnv(EnvEnum.getEnum(profile));
        topic = "realtime-event";
        group = "pile_state_monitor_job";
        log.info("当前 kafka 配置 brokers:{} topic:{} group:{},redisHost:{},redisPassword:{}", brokers, topic, group, redisHost, redisPassword);
    }


    public static PileStateDTO wrapperData(PileStateDTO value, Jedis jedis, Long id) {
        value.setId(id);

        String pileJson = jedis.get(String.format(RedisKeyEnum.DIM_PILE_DETAIL_DF.getKeyPrefix(), value.getPileCode()));

        if (StringUtils.isEmpty(pileJson)) {
//            log.info("pile 缓存不存在|dim|{}", String.format(RedisKeyEnum.DIM_PILE_DETAIL_DF.getKeyPrefix(), value.getPileCode()));

            // dim 缓存没有则使用老缓存
            pileJson = jedis.get("pile:detail:" + value.getPileCode());
            if (StringUtils.isEmpty(pileJson)) {
//                log.info("pile 缓存不存在|db|{}", "pile:detail:" + value.getPileCode());
                return null;
            }

            JSONObject pile = JSONObject.parseObject(pileJson);
            value.setOperatorId(pile.getString("operatorId"));
            value.setPileCode(pile.getString("pileCode"));
            value.setProtocol(pile.getString("protocol"));
            value.setStationId(pile.getInteger("stationId"));

            String operatorJson = jedis.get("operator:detail:" + value.getOperatorId());
            String stationJson = jedis.get(String.format(RedisKeyEnum.DIM_STATION_ID_DETAIL_DF.getKeyPrefix(), value.getStationId()));
            String pileTypeJson = jedis.get(String.format(RedisKeyEnum.DIM_PILE_TYPE_DETAIL_DF.getKeyPrefix(), pile.getString("pileTypeId")));

            if (StringUtils.isNotEmpty(operatorJson)) {
                JSONObject operator = JSONObject.parseObject(operatorJson);
                value.setOperatorName(operator.getString("name"));
            }
            if (StringUtils.isNotEmpty(stationJson)) {
                JSONObject station = JSONObject.parseObject(stationJson);
                value.setStationName(station.getString("stationName"));
                value.setStationCode(station.getString("stationCode"));
            }
            if (StringUtils.isNotEmpty(pileTypeJson)) {
                JSONObject pileType = JSONObject.parseObject(pileTypeJson);
                value.setCurrentType(pileType.getString("currentType"));
            }

            if (StringUtils.isNotEmpty(value.getLogTime()) && value.getLogTime().split("-")[0].length() < 2) {
                value.setLogTime("2" + value.getLogTime());
            }
            value.setLogTime(StringUtils.isEmpty(value.getLogTime()) ? DateUtil.now() : "20" + value.getLogTime());
            value.setStateMessage(value.getState() == 98 ? "在线" : value.getState() == 99 ? "离线" : value.getStateMessage());
            return value;
        }

        JSONObject pile = JSONObject.parseObject(pileJson);
        value.setOperatorId(pile.getString("opId"));
        value.setCurrentType(pile.getString("currentType"));
        value.setOperatorName(pile.getString("opName"));
        value.setPileCode(pile.getString("pileCode"));
        value.setProtocol(pile.getString("pileProtocol"));
        value.setStationId(pile.getInteger("stId"));
        value.setStationName(pile.getString("stName"));
        value.setStationCode(pile.getString("stCode"));
        if (StringUtils.isNotEmpty(value.getLogTime()) && value.getLogTime().split("-")[0].length() < 2) {
            value.setLogTime("2" + value.getLogTime());
        }
        value.setStateMessage(value.getState() == 98 ? "在线" : value.getState() == 99 ? "离线" : value.getStateMessage());
        value.setLogTime(StringUtils.isEmpty(value.getLogTime()) ? DateUtil.now() : "20" + value.getLogTime());
        return value;
    }

    private static class OperatorDetailInfoFlatMapFunction extends RichFlatMapFunction<PileStateDTO, PileStateDTO> {
        private transient ValueState<Integer> gunState;
        private transient Snowflake snowflake;
        private JedisPool jedisPool;
        private Jedis jedis;

        @Override
        public void flatMap(PileStateDTO value, Collector<PileStateDTO> out) throws IOException {
            Integer state = gunState.value();
            // 适配心跳
            if (value.getState() == 100) {
                PileStateDTO pileStateDTO = wrapperData(value, jedis, snowflake.nextId());
                if (pileStateDTO != null) {
                    out.collect(pileStateDTO);
                }
                return;
            }

            if (state != null && state.equals(value.getState())) {
                return;
            }

            gunState.update(value.getState());
            PileStateDTO pileStateDTO = wrapperData(value, jedis, snowflake.nextId());
            if (pileStateDTO != null) {
                out.collect(pileStateDTO);
            }
        }

        @Override
        public void open(Configuration configuration) {
            ValueStateDescriptor<Integer> descriptor = new ValueStateDescriptor<>("gunState", TypeInformation.of(new TypeHint<Integer>() {
            }));
            gunState = getRuntimeContext().getState(descriptor);
            snowflake = IdUtil.getSnowflake(3, 1);

            JedisPoolConfig config = new JedisPoolConfig();
            config.setTestWhileIdle(true);
            config.setMinEvictableIdleTimeMillis(60000);
            config.setTimeBetweenEvictionRunsMillis(30000);
            config.setNumTestsPerEvictionRun(-1);

            ExecutionConfig ev = getRuntimeContext().getExecutionConfig();
            String host = ev.getGlobalJobParameters().toMap().get("redisHost");
            String pwd = ev.getGlobalJobParameters().toMap().get("redisPassword");
            log.info("初始化redis 连接池 {} {}", host, pwd);
            jedisPool = new JedisPool(config, host, 6379, 3 * 1000, pwd);
            jedis = jedisPool.getResource();
        }

        @Override
        public void close() {
            if (jedisPool != null) {
                jedisPool.close();
            }
            if (gunState != null) {
                gunState.clear();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
        init(profile);

        //1.创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.enableCheckpointing(30000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(5000);
        env.getCheckpointConfig().setCheckpointTimeout(120000);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);

        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(new HashMap<String, String>() {{
            put("profile", profile);
            put("redisHost", redisHost);
            put("redisPassword", redisPassword);
        }}));

        //2.添加数据源
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, group + "_" + RandomUtil.randomNumbers(8) + "_" + Instant.now().getEpochSecond());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "120000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "10000");
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "10000");
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, "30000");
        props.put(ConsumerConfig.FETCH_MIN_BYTES_CONFIG, "5000000");
        props.put(ConsumerConfig.FETCH_MAX_WAIT_MS_CONFIG, "5000");

        KafkaSource<String> source = KafkaSource.<String>builder()
                .setBootstrapServers(brokers)
                .setTopics(topic)
                .setGroupId(group)
                .setProperties(props)
                .setDeserializer(KafkaRecordDeserializationSchema.valueOnly(StringDeserializer.class))
                .setStartingOffsets(OffsetsInitializer.latest())
                .build();

        DataStreamSource<String> dataStreamSource = env
                .fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");
        WatermarkStrategy<PileStateDTO> watermarkStrategy = WatermarkStrategy.<PileStateDTO>forMonotonousTimestamps().withTimestampAssigner((event, timestamp) -> System.currentTimeMillis());

        DataStream<PileStateDTO> accessEventDataStream = dataStreamSource.map(new MapFunction<String, List<PileStateDTO>>() {
            @Override
            public List<PileStateDTO> map(String jsonvalue) {
                return MessageAnalysisUtil.dataMap(jsonvalue);
            }
        }).name("数据解析").filter(new FilterFunction<List<PileStateDTO>>() {
            @Override
            public boolean filter(List<PileStateDTO> value) {
                return value != null && !value.isEmpty();
            }
        }).name("非空过滤").flatMap(new FlatMapFunction<List<PileStateDTO>, PileStateDTO>() {
            @Override
            public void flatMap(List<PileStateDTO> value, Collector<PileStateDTO> out) {
                value.forEach(out::collect);
            }
        }).assignTimestampsAndWatermarks(watermarkStrategy).name("枪报文数据转换");

        KeyedStream<PileStateDTO, String> gunStateKeyBy = accessEventDataStream
                .filter(v -> StringUtils.isNotEmpty(v.getGunCode()))
                .keyBy(PileStateDTO::getGunCode);

        KeyedStream<PileStateDTO, String> pileStateKeyBy = accessEventDataStream
                .filter(v -> (v.getState() == 98 || v.getState() == 99) && StringUtils.isNotEmpty(v.getPileCode()))
                .keyBy(PileStateDTO::getPileCode);

        KeyedStream<PileStateDTO, String> heartBeatKeyBy = accessEventDataStream
                .filter(v -> (v.getState() == 100) && StringUtils.isNotEmpty(v.getPileCode()))
                .keyBy(PileStateDTO::getPileCode);

        ProcessAllWindowFunction<PileStateDTO, Object, GlobalWindow> processAllWindowFunction = new ProcessAllWindowFunction<PileStateDTO, Object, GlobalWindow>() {
            private transient Connection connection;

            @Override
            public void open(Configuration parameters) throws Exception {
                super.open(parameters);
                log.info("dws_gun_state_duration 数据库连接");
                initConnection("rcd_dw_iot");
            }

            @Override
            public void process(ProcessAllWindowFunction<PileStateDTO, Object, GlobalWindow>.Context context, Iterable<PileStateDTO> elements, Collector<Object> out) throws SQLException {
                List<PileStateDTO> elList = (List<PileStateDTO>) elements;
                saveGunStateDuration(elements);
                log.info("写入 dws_gun_state_duration | 数据量 {}", elList.size());
            }

            private void saveGunStateDuration(Iterable<PileStateDTO> elements) throws SQLException {
                String sql = "INSERT INTO dws_gun_state_duration (id, gun_code,pile_code ,state, created_at,state_desc , station_name , operator_name ,operator_id,station_code,protocol,current_type ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(sql);
                for (PileStateDTO next : elements) {
                    String logTime = DateUtil.now();
                    try {
                        logTime = DateUtil.parse(next.getLogTime()).toString();
                    } catch (Exception ignored) {
                        log.error("桩数据日志时间异常{}", next.getLogTime());
                    }

                    preparedStatement.setLong(1, next.getId());
                    preparedStatement.setString(2, next.getGunCode());
                    preparedStatement.setString(3, next.getPileCode());
                    preparedStatement.setInt(4, next.getState());
                    preparedStatement.setString(5, logTime);
                    preparedStatement.setString(6, next.getStateMessage());
                    preparedStatement.setString(7, next.getStationName());
                    preparedStatement.setString(8, next.getOperatorName());
                    preparedStatement.setString(9, next.getOperatorId());
                    preparedStatement.setString(10, next.getStationCode());
                    preparedStatement.setString(11, next.getProtocol());
                    preparedStatement.setString(12, next.getCurrentType());

                    preparedStatement.addBatch();
                }
                preparedStatement.executeBatch();
            }

            public void initConnection(String dataBase) throws ClassNotFoundException, SQLException {
                ExecutionConfig config = getRuntimeContext().getExecutionConfig();
                String profile = config.getGlobalJobParameters().toMap().get("profile");

                if ("prod".equals(profile)) {
                    Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
                    String url = "jdbc:clickhouse://172.16.16.4:8123/" + dataBase;
                    connection = DriverManager.getConnection(url, "default", "34p9KgvXDBG!DmEhsn%F6cmAUgf$JS");
                } else if ("uat".equals(profile)) {
                    Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
                    String url = "jdbc:clickhouse://172.16.16.5:8123/" + dataBase;
                    connection = DriverManager.getConnection(url, "default", "d9jE!r4UPZE3t7XWLUMPQQa9!xtNdV");
                } else {
                    Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
                    String url = "jdbc:clickhouse://119.29.28.134:8123/" + dataBase;
                    connection = DriverManager.getConnection(url, "default", "runcdcktest1234!@#$");
                }
            }

            @Override
            public void close() throws Exception {
                connection.close();
                super.close();
            }
        };

        gunStateKeyBy
                .window(TumblingEventTimeWindows.of(Time.minutes(3)))
                .process(new ProcessWindowFunction<PileStateDTO, Object, String, TimeWindow>() {
                    private transient Connection connection;
                    private transient Snowflake snowflake;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        initConnection("rcd_dw_iot");
                        log.info("dws_gun_state_frequent 数据库连接");
                        snowflake = IdUtil.getSnowflake(1, 2);
                        super.open(parameters);
                    }

                    @Override
                    public void close() throws Exception {
                        connection.close();
                        super.close();
                    }

                    @Override
                    public void process(String s, ProcessWindowFunction<PileStateDTO, Object, String, TimeWindow>.Context context, Iterable<PileStateDTO> elements, Collector<Object> out) throws Exception {
                        List<PileStateDTO> valList = (List) elements;
                        int counts = -1;
                        int currentState = -1;
                        for (PileStateDTO e : valList) {
                            if (currentState != e.getState()) {
                                counts++;
                            }
                            currentState = e.getState();
                        }

                        if (counts >= 5) {
                            Map<Integer, Long> stateMap = valList.stream().map(PileStateDTO::getState)
                                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

                            saveGunStateDuration(valList.get(0).getGunCode(), counts, JSON.toJSONString(stateMap));
                            log.info("写入 dws_gun_state_frequent | 枪号:{} 切换次数:{} 状态次数合集:{}", valList.get(0).getGunCode(), counts, stateMap);

                        }
                    }

                    private void saveGunStateDuration(String gunCode, Integer times, String stateMapJson) throws SQLException {
                        String sql = " INSERT INTO dws_gun_state_frequent (id, gun_code,times,state_data,created_at) VALUES (?,?,?,?,?)";
                        @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(sql);

                        preparedStatement.setLong(1, snowflake.nextId());
                        preparedStatement.setString(2, gunCode);
                        preparedStatement.setInt(3, times);
                        preparedStatement.setString(4, stateMapJson);
                        preparedStatement.setString(5, DateTime.now().toString());

                        preparedStatement.executeUpdate();
                    }

                    public void initConnection(String dataBase) throws ClassNotFoundException, SQLException {
                        ExecutionConfig config = getRuntimeContext().getExecutionConfig();
                        String profile = config.getGlobalJobParameters().toMap().get("profile");
                        if ("prod".equals(profile)) {
                            Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
                            String url = "jdbc:clickhouse://172.16.16.4:8123/" + dataBase;
                            connection = DriverManager.getConnection(url, "default", "34p9KgvXDBG!DmEhsn%F6cmAUgf$JS");
                        } else if ("uat".equals(profile)) {
                            Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
                            String url = "jdbc:clickhouse://172.16.16.5:8123/" + dataBase;
                            connection = DriverManager.getConnection(url, "default", "d9jE!r4UPZE3t7XWLUMPQQa9!xtNdV");
                        } else {
                            Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
                            String url = "jdbc:clickhouse://119.29.28.134:8123/" + dataBase;
                            connection = DriverManager.getConnection(url, "default", "runcdcktest1234!@#$");
                        }
                    }
                })
                .name("枪状态切换频率监测 | 3 分钟内频繁切换超出 5 次");

        gunStateKeyBy
                .flatMap(new OperatorDetailInfoFlatMapFunction())
                .name("枪状态对象转换")
                .countWindowAll("prod".equals(profile) ? 1000L : 10L)
                .process(processAllWindowFunction).name("枪状态数据写入")
        ;

        pileStateKeyBy
                .flatMap(new OperatorDetailInfoFlatMapFunction())
                .name("桩状态对象转换")
                .countWindowAll("prod".equals(profile) ? 50L : 1L)
                .process(processAllWindowFunction).name("桩状态数据写入");

        heartBeatKeyBy
                .flatMap(new OperatorDetailInfoFlatMapFunction())
                .name("桩心跳对象转换")
                .countWindowAll("prod".equals(profile) ? 3000L : 1L)
                .process(processAllWindowFunction).name("桩心跳数据写入");


        String envName = "枪状态持续时间检测" + profile;
        env.execute(envName);
    }


}
