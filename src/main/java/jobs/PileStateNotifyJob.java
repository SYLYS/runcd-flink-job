package jobs;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import config.EnvHolder;
import dto.PileStateDTO;
import enums.EnvEnum;
import enums.NotifyGroupEnum;
import lombok.extern.slf4j.Slf4j;
import mapper.PileStateMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.cep.CEP;
import org.apache.flink.cep.PatternFlatSelectFunction;
import org.apache.flink.cep.PatternFlatTimeoutFunction;
import org.apache.flink.cep.PatternStream;
import org.apache.flink.cep.pattern.Pattern;
import org.apache.flink.cep.pattern.conditions.SimpleCondition;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.AsyncDataStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.ProcessingTimeSessionWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.CountTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import utli.MessageAnalysisUtil;
import utli.NotifyUtil;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 桩离线监控钉钉通知
 */
@Slf4j
public class PileStateNotifyJob {
    private static String brokers;
    private static String topic;
    private static String group;

    private static void init(String profile) {
        if (EnvEnum.PROD.toString().equals(profile)) {
            brokers = "172.16.16.4:9092";
        } else {
            brokers = "106.52.85.243:9092";
        }
        EnvHolder.setCurrentEnv(EnvEnum.getEnum(profile));
        topic = "realtime-event";
        group = "pile_state_notify_job";
        log.info("当前 kafka 配置 brokers:{} topic:{} group:{}", brokers, topic, group);
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            log.error("请配置启动环境");
            return;
        }
        String profile = args[0];
        init(profile);

        //1.创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.enableCheckpointing(120000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(5000);
        env.getCheckpointConfig().setCheckpointTimeout(120000);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.setParallelism(1);

        env.getConfig().setGlobalJobParameters(ParameterTool.fromMap(Collections.singletonMap("profile", profile)));

        //2.添加数据源
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, group + "_" + RandomUtil.randomNumbers(8) + "_" + Instant.now().getEpochSecond());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, "120000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "60000");

        KafkaSource<String> source = KafkaSource.<String>builder()
                .setBootstrapServers(brokers)
                .setTopics(topic)
                .setGroupId(group)
                .setProperties(props)
                .setDeserializer(KafkaRecordDeserializationSchema.valueOnly(StringDeserializer.class))
                .setStartingOffsets(OffsetsInitializer.latest())
                .build();
        DataStreamSource<String> dataStreamSource = env.fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");


        WatermarkStrategy<PileStateDTO> watermarkStrategy = WatermarkStrategy
                .<PileStateDTO>forMonotonousTimestamps()
                .withTimestampAssigner((event, timestamp) -> System.currentTimeMillis());

        DataStream<PileStateDTO> accessEventDataStream = dataStreamSource
                .map(new MapFunction<String, List<PileStateDTO>>() {
                    @Override
                    public List<PileStateDTO> map(String jsonvalue) {
                        return new MessageAnalysisUtil().dataMap(jsonvalue);
                    }
                })
                .filter(new FilterFunction<List<PileStateDTO>>() {
                    @Override
                    public boolean filter(List<PileStateDTO> value) {
                        return value != null;
                    }
                })
                .flatMap(new FlatMapFunction<List<PileStateDTO>, PileStateDTO>() {
                    @Override
                    public void flatMap(List<PileStateDTO> value, Collector<PileStateDTO> out) {
                        value.forEach(out::collect);
                    }
                }).assignTimestampsAndWatermarks(watermarkStrategy);

//        accessEventDataStream.print();

//        SingleOutputStreamOperator<PileStateDTO> outputStreamOperator = accessEventDataStream.map(new KafkaDataToDeviceStateDWDFunction()).filter(new FilterFunction<PileStateDTO>() {
//            @Override
//            public boolean filter(PileStateDTO value) throws Exception {
//                return value != null;
//            }
//        });

//        String sql = "INSERT INTO dwd_device_state_log (`operator_id`,`operator_name`,`station_id`,`station_name`, `pile_code` ,`gun_code`,`state`,`state_message`,`logTime`,`gmt_create`) " +
//                "VALUES (?,?,?,?,?,?,?,?,?,?)";
//
//        outputStreamOperator.addSink(new DeviceStateToClickhouseFunction(sql));

//        outputStreamOperator.print();


        DataStream<PileStateDTO> dataStreamKeyBy = accessEventDataStream.keyBy(PileStateDTO::getPileCode);

        //定义一个事件模式（Pattern）
        Pattern<PileStateDTO, PileStateDTO> warningPattern = Pattern.<PileStateDTO>begin("start")
                .where(new SimpleCondition<PileStateDTO>() {
                    private static final long serialVersionUID = -6847788055093903603L;

                    @Override
                    public boolean filter(PileStateDTO pileStateDTO) throws Exception {
                        return pileStateDTO.getState() == 99;
                    }
                })
                .next("end").where(new SimpleCondition<PileStateDTO>() {
                    @Override
                    public boolean filter(PileStateDTO pileStateDTO) throws Exception {
                        return pileStateDTO != null;
                    }
                })
                .within(Time.minutes(5)).times(1);

        PatternStream<PileStateDTO> accessEventPatternStream = CEP.pattern(dataStreamKeyBy, warningPattern);

//        streamOperator.print();

        OutputTag<PileStateDTO> outputTag = new OutputTag<PileStateDTO>("timedout") {
            private static final long serialVersionUID = 773503794597666247L;
        };
        SingleOutputStreamOperator<PileStateDTO> timeout = accessEventPatternStream.flatSelect(
                outputTag,
                new AccessTimedOut(),
                new FlatSelect()
        );
        //打印输出超时的AccessEvent
//        timeout.getSideOutput(outputTag).print();
        timeout.getSideOutput(outputTag);

        DataStream<PileStateDTO> sideOutput = timeout.getSideOutput(outputTag);

        SingleOutputStreamOperator<PileStateDTO> singleOutputStreamOperatorSync = AsyncDataStream.unorderedWait(sideOutput, new PileStateMapper(), 3, TimeUnit.SECONDS, 1000);

        singleOutputStreamOperatorSync
                .filter(e -> e != null && StringUtils.isNotEmpty(e.getStationName()) && StringUtils.isNotEmpty(e.getOperatorName()))
                .keyBy(PileStateDTO::getStationName)
                .window(ProcessingTimeSessionWindows.withGap(Time.minutes(5)))
                .trigger(CountTrigger.of(5))
                .apply(new WindowFunction<PileStateDTO, Object, String, TimeWindow>() {
                    @Override
                    public void apply(String s, TimeWindow window, Iterable<PileStateDTO> input, Collector<Object> out) throws Exception {
                        List<PileStateDTO> list = new ArrayList<>();
                        input.forEach(list::add);
                        NotifyUtil.send(NotifyGroupEnum.PILE_STATE_OFFLINE, JSON.toJSONString(wrapperMsg(list)));
                        out.collect(list);
                    }
                });


        env.execute("设备故障离线告警" + profile);
    }

    /**
     * 把超时的事件收集起来
     */
    public static class AccessTimedOut implements PatternFlatTimeoutFunction<PileStateDTO, PileStateDTO> {
        private static final long serialVersionUID = -4214641891396057732L;

        @Override
        public void timeout(Map<String, List<PileStateDTO>> pattern, long timeStamp, Collector<PileStateDTO> out) throws Exception {
            if (null != pattern.get("start")) {
                for (PileStateDTO accessEvent : pattern.get("start")) {
                    out.collect(accessEvent);
                }
            }
            if (null == pattern.get("end")) {
                List<PileStateDTO> stateDTOList = pattern.get("start");
            }
        }
    }

    private static List<String> wrapperMsg(List<PileStateDTO> stateDTOList) {
        List<String> result = new ArrayList<>();
        stateDTOList.forEach(e -> {
            String operatorName = e.getOperatorName();
            String stationName = e.getStationName();
            String pileCode = e.getPileCode();
            String logTime = e.getLogTime();
            if (StringUtils.isNoneEmpty(operatorName) && StringUtils.isNotEmpty(stationName)) {
                String msg = "告警名称：离线故障告警\n" +
                        "运营名称：" + operatorName + "\n" +
                        "站点名称：" + stationName + "\n" +
                        "离线桩号：" + pileCode + "\n" +
                        "告警时间：" + "20" + logTime + "\n" +
                        "告警详情：设备离线超时五分钟";
                result.add(msg);
            }
        });
        return result;
    }

    /**
     * 未超时的事件
     */
    public static class FlatSelect implements PatternFlatSelectFunction<PileStateDTO, PileStateDTO> {
        private static final long serialVersionUID = -3029589950677623844L;

        @Override
        public void flatSelect(Map<String, List<PileStateDTO>> pattern, Collector<PileStateDTO> collector) throws Exception {
            collector.collect(new PileStateDTO());
        }
    }


}
