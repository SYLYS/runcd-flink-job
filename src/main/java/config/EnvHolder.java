package config;

import enums.EnvEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * @author xiaokun
 * @date 2023/4/20 17:38:14
 */
@Slf4j
public class EnvHolder {

    private static volatile EnvEnum currentEnv = null;

    public static void setCurrentEnv(EnvEnum env) {
        currentEnv = env;
        log.info("当前环境:{}", currentEnv);
    }

    public static EnvEnum getCurrentEnv() {
        return currentEnv;
    }
}
