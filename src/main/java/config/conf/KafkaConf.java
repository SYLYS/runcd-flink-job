package config.conf;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KafkaConf {
    private String brokers;
    private String topic;
    private String group;
    private String username;
    private String password;
}