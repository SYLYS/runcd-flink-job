package config;

import config.conf.ClickHouseConf;
import config.conf.KafkaConf;
import config.conf.MysqlConf;
import config.conf.RedisConf;
import enums.EnvEnum;

public class DataSourceInfoUtil {

    public static KafkaConf getTencentKafkaConf(String profile) {
        if (EnvEnum.PROD.toString().equals(profile)) {
            return new KafkaConf("guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129", "topic-subs-dfco31hqn8-cdb-fw6ajmnb", "consumer-grp-subs-dfco31hqn8-device-info", "account-subs-dfco31hqn8-device-info", "device-info");
        } else if (EnvEnum.UAT.toString().equals(profile)) {
            return new KafkaConf("guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129", "topic-subs-dfco31hqn8-cdb-fw6ajmnb", "consumer-grp-subs-dfco31hqn8-device-info", "account-subs-dfco31hqn8-device-info", "device-info");
        } else if (EnvEnum.TEST.toString().equals(profile)) {
            return new KafkaConf("guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129", "topic-subs-7impax4gci-cdb-f3as145r", "consumer-grp-subs-7impax4gci-device-info", "account-subs-7impax4gci-device-info", "device-info");
        } else if (EnvEnum.DEV.toString().equals(profile)) {
            return new KafkaConf("guangzhou-kafka-1.cdb-dts.tencentcs.com.cn:32129", "topic-subs-7impax4gci-cdb-f3as145r", "consumer-grp-subs-7impax4gci-device-info", "account-subs-7impax4gci-device-info", "device-info");
        }
        return new KafkaConf();
    }

    public static KafkaConf getKafkaConf(String profile) {
        String brokers = "";
        String topic = "";
        String group = "";
        String username = "";
        String password = "";
        if (EnvEnum.PROD.toString().equals(profile)) {
            brokers = "172.16.16.4:9092";
        } else if (EnvEnum.UAT.toString().equals(profile)) {
            brokers = "172.16.16.4:9092";
        } else if (EnvEnum.TEST.toString().equals(profile)) {
            brokers = "106.52.85.243:9092";
        } else if (EnvEnum.DEV.toString().equals(profile)) {
            brokers = "106.52.85.243:9092";
        }
        return new KafkaConf(brokers, topic, group, username, password);
    }

    public static ClickHouseConf getClickHouseConf(String profile,String dataBase) {
        if (EnvEnum.PROD.toString().equals(profile)) {
            return new ClickHouseConf("jdbc:clickhouse://172.16.16.11:18123/rcd"+dataBase,"default", "d82hbBR9yTTj$MT!H4sfNY%G9Cpdkn");
        } else if (EnvEnum.UAT.toString().equals(profile)) {
            return new ClickHouseConf("jdbc:clickhouse://119.29.28.134:18123/uat"+dataBase,"default", "d82hbBR9yTTj$MT!H4sfNY%G9Cpdkn");
        } else if (EnvEnum.TEST.toString().equals(profile)) {
            return new ClickHouseConf("jdbc:clickhouse://119.29.28.134:18123/test"+dataBase, "default", "H6kamt7Fhr");
        } else if (EnvEnum.DEV.toString().equals(profile)) {
            return new ClickHouseConf("jdbc:clickhouse://119.29.28.134:18123/dev"+dataBase, "default", "H6kamt7Fhr");
        }
        return new ClickHouseConf();
    }

    public static ClickHouseConf getClickHouseSingleConf(String profile,String dataBase) {
        if (EnvEnum.PROD.toString().equals(profile)) {
            return new ClickHouseConf("jdbc:clickhouse://172.16.16.4:8123/runcd"+dataBase,"default", "34p9KgvXDBG!DmEhsn%F6cmAUgf$JS");
        } else if (EnvEnum.UAT.toString().equals(profile)) {
            return new ClickHouseConf("jdbc:clickhouse://172.16.16.5:8123/runcd"+dataBase,"default", "d82hbBR9yTTj$MT!H4sfNY%G9Cpdkn");
        } else if (EnvEnum.TEST.toString().equals(profile)) {
            return new ClickHouseConf("jdbc:clickhouse://119.29.28.134:8123/runcd"+dataBase, "default", "runcdcktest1234!@#$");
        } else if (EnvEnum.DEV.toString().equals(profile)) {
            return new ClickHouseConf("jdbc:clickhouse://119.29.28.134:8123/runcd"+dataBase, "default", "runcdcktest1234!@#$");
        }
        return new ClickHouseConf();
    }

    public static MysqlConf getMysqlConf(String profile,String dataBase) {
        if (EnvEnum.PROD.toString().equals(profile)) {
            String url = "jdbc:mysql://gz-cdb-6116c2xb.sql.tencentcdb.com:57730/"+dataBase+"?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&allowMultiQueries=true";
            return new MysqlConf("com.mysql.cj.jdbc.Driver", url, "slave_read_mysql", "QUyFO1rfgyixAuMoDFjB");
        } else if (EnvEnum.UAT.toString().equals(profile)) {
            String url = "jdbc:mysql://gz-cdb-6116c2xb.sql.tencentcdb.com:57730/"+dataBase+"?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&allowMultiQueries=true";
            return new MysqlConf("com.mysql.cj.jdbc.Driver", url, "slave_read_mysql", "QUyFO1rfgyixAuMoDFjB");
        } else if (EnvEnum.TEST.toString().equals(profile)) {
            String url = "jdbc:mysql://gz-cdb-f3as145r.sql.tencentcdb.com:58965/"+dataBase+"?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&allowMultiQueries=true";
            return new MysqlConf("com.mysql.cj.jdbc.Driver", url, "runcd", "rcdtest2021");
        } else if (EnvEnum.DEV.toString().equals(profile)) {
            String url = "jdbc:mysql://gz-cdb-f3as145r.sql.tencentcdb.com:58965/"+dataBase+"?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&allowMultiQueries=true";
            return new MysqlConf("com.mysql.cj.jdbc.Driver", url, "runcd", "rcdtest2021");
        }
        return new MysqlConf();
    }

    public static RedisConf getRedisConf(String profile) {
        if (EnvEnum.PROD.toString().equals(profile)) {
            return new RedisConf("gz-crs-05cor0zn.sql.tencentcdb.com", 23197, "Vp6Ghb7Z9MS7H59MaGRjbFfF%Jkzkex&");
        } else if (EnvEnum.UAT.toString().equals(profile)) {
            return new RedisConf("172.16.32.12", 6379, "Vp6Ghb7Z9MS7H59MaGRjbFfF%Jkzkex&");
        } else if (EnvEnum.TEST.toString().equals(profile)) {
            return new RedisConf("gz-crs-ee4uj12i.sql.tencentcdb.com", 27702, "JSSbDE4b#DufZg*&k%jVKCvse4BQ2!");
        } else if (EnvEnum.DEV.toString().equals(profile)) {
            return new RedisConf("gz-crs-ee4uj12i.sql.tencentcdb.com", 27702, "JSSbDE4b#DufZg*&k%jVKCvse4BQ2!");
        }
        return new RedisConf();
    }

}
