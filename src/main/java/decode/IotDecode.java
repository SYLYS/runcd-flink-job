package decode;

import java.util.Arrays;
import java.util.List;

public class IotDecode {
    /**
     * 直流订单
     */
    public static final List<ProtocolProperty> DC_ORDER_PROPERTIES = Arrays.asList(
            new ProtocolProperty("起始", 2, 0, "byte2int"),
            new ProtocolProperty("版本号", 1, 2, "byte1int"),
            new ProtocolProperty("充电桩编号", 16, 3, "String"),
            new ProtocolProperty("充电枪类型", 1, 19, "byte1int"),
            new ProtocolProperty("充电枪序号", 1, 20, "byte1int"),
            new ProtocolProperty("帧类型标识", 2, 21, "byte2int"),
            new ProtocolProperty("数据区域长度", 2, 23, "byte2int"),
            new ProtocolProperty("业务流水号", 32, 25, "String"),
            new ProtocolProperty("用户账户", 32, 57, "String"),
            new ProtocolProperty("消费总金额", 4, 89, "byte4int"),
            new ProtocolProperty("充电电量", 4, 91 + 2, "byte4int"),
            new ProtocolProperty("充电停止原因", 1, 95 + 2, "byte1int"),
            new ProtocolProperty("交易方式", 1, 96 + 2, "byte1int"),
            new ProtocolProperty("充电类型", 1, 97 + 2, "byte1int"),
            new ProtocolProperty("充电参数", 2, 98 + 2, "byte2int"),
            new ProtocolProperty("开始充电时间", 7, 102, "timeScale"),
            new ProtocolProperty("结束充电时间", 7, 109, "timeScale"),
            new ProtocolProperty("充电时长", 2, 116, "byte2int"),
            new ProtocolProperty("尖时段电量", 4, 118, "byte4int"),
            new ProtocolProperty("峰时段电量", 4, 122, "byte4int"),
            new ProtocolProperty("平时段电量", 4, 126, "byte4int"),
            new ProtocolProperty("谷时段电量", 4, 130, "byte4int"),
            new ProtocolProperty("电费总金额", 4, 134, "byte4int"),
            new ProtocolProperty("服务费总金额", 4, 138, "byte4int"),
            new ProtocolProperty("充电前SOC", 1, 142, "byte1int"),
            new ProtocolProperty("充电后SOC", 1, 143, "byte1int"),
            new ProtocolProperty("BMS电源类型", 1, 144, "byte1int"),
            new ProtocolProperty("充电前电表度数", 4, 145, "byte4int"),
            new ProtocolProperty("充电后电表度数", 4, 149, "byte4int")
    );
    /**
     * 交流订单
     */
    public static final List<ProtocolProperty> AC_ORDER_PROPERTIES = Arrays.asList(
            new ProtocolProperty("起始", 2, 0, "byte2int"),
            new ProtocolProperty("版本号", 1, 2, "byte1int"),
            new ProtocolProperty("充电桩编号", 16, 3, "String"),
            new ProtocolProperty("充电枪类型", 1, 19, "byte1int"),
            new ProtocolProperty("充电枪序号", 1, 20, "byte1int"),
            new ProtocolProperty("帧类型标识", 2, 21, "byte2int"),
            new ProtocolProperty("数据区域长度", 2, 23, "byte2int"),
            new ProtocolProperty("业务流水号", 32, 25, "String"),
            new ProtocolProperty("用户账户", 32, 57, "String"),
            new ProtocolProperty("消费总金额", 4, 89, "byte4int"),
            new ProtocolProperty("充电电量", 4, 91 + 2, "byte4int"),
            new ProtocolProperty("充电停止原因", 1, 95 + 2, "byte1int"),
            new ProtocolProperty("交易方式", 1, 96 + 2, "byte1int"),
            new ProtocolProperty("充电类型", 1, 97 + 2, "byte1int"),
            new ProtocolProperty("充电参数", 2, 98 + 2, "byte2int"),
            new ProtocolProperty("开始充电时间", 7, 102, "timeScale"),
            new ProtocolProperty("结束充电时间", 7, 109, "timeScale"),
            new ProtocolProperty("充电时长", 2, 116, "byte2int"),
            new ProtocolProperty("尖时段电量", 4, 118, "byte4int"),
            new ProtocolProperty("峰时段电量", 4, 122, "byte4int"),
            new ProtocolProperty("平时段电量", 4, 126, "byte4int"),
            new ProtocolProperty("谷时段电量", 4, 130, "byte4int"),
            new ProtocolProperty("电费总金额", 4, 134, "byte4int"),
            new ProtocolProperty("服务费总金额", 4, 138, "byte4int")
//                new ProtocolProperty("充电前电表度数", 4, 142, "byte4int")
//                new ProtocolProperty("充电后电表度数", 4, 146, "byte4int")
    );

    /**
     * 心跳
     */
    public static final List<ProtocolProperty> GUN_HEARTBEAT_STATE = Arrays.asList(
            new ProtocolProperty("起始", 2, 0, "byte2int"),
            new ProtocolProperty("版本号", 1, 2, "byte1int"),
            new ProtocolProperty("充电桩编号", 16, 3, "String"),
            new ProtocolProperty("充电枪类型", 1, 19, "byte1int"),
            new ProtocolProperty("充电枪序号", 1, 20, "byte1int"),
            new ProtocolProperty("帧类型标识", 2, 21, "byte2int"),
            new ProtocolProperty("数据区域长度", 2, 23, "byte2int"),
            new ProtocolProperty("枪数量", 1, 25, "byte1int")
            //...
    );

    /**
     * 交流实时数据
     */
    public static final List<ProtocolProperty> PILE_AC_REAL_DATA = Arrays.asList(
            new ProtocolProperty("起始", 2, 0, "byte2int"),
            new ProtocolProperty("版本号", 1, 2, "byte1int"),
            new ProtocolProperty("充电桩编号", 16, 3, "String"),
            new ProtocolProperty("充电枪类型", 1, 19, "byte1int"),
            new ProtocolProperty("充电枪序号", 1, 20, "byte1int"),
            new ProtocolProperty("帧类型标识", 2, 21, "byte2int"),
            new ProtocolProperty("数据区域长度", 2, 23, "byte2int"),

            new ProtocolProperty("电费金额", 4, 25, "byte4int"),
            new ProtocolProperty("服务费金额", 4, 29, "byte4int"),
            new ProtocolProperty("消费金额", 4, 33, "byte4int"),
            new ProtocolProperty("充电电量", 4, 37, "byte4int"),
            new ProtocolProperty("尖时段电量", 4, 41, "byte4int"),
            new ProtocolProperty("峰时段电量", 4, 45, "byte4int"),
            new ProtocolProperty("平时段电量", 4, 49, "byte4int"),
            new ProtocolProperty("谷时段电量", 4, 53, "byte4int"),
            new ProtocolProperty("累计充电时间", 2, 57, "byte2int"),
            new ProtocolProperty("充电功率", 2, 59, "byte2int"),
            new ProtocolProperty("充电电压", 2, 61, "byte2int"),
            new ProtocolProperty("充电电流", 2, 63, "byte2int")

    );

    /**
     * 直流实时数据1
     */
    public static final List<ProtocolProperty> PILE_DC_REAL_DATA_1 = Arrays.asList(
            new ProtocolProperty("起始", 2, 0, "byte2int"),
            new ProtocolProperty("版本号", 1, 2, "byte1int"),
            new ProtocolProperty("充电桩编号", 16, 3, "String"),
            new ProtocolProperty("充电枪类型", 1, 19, "byte1int"),
            new ProtocolProperty("充电枪序号", 1, 20, "byte1int"),
            new ProtocolProperty("帧类型标识", 2, 21, "byte2int"),
            new ProtocolProperty("数据区域长度", 2, 23, "byte2int"),
            new ProtocolProperty("soc", 1, 25, "byte1int"),
            new ProtocolProperty("电费金额", 4, 26, "byte4int"),
            new ProtocolProperty("服务费金额", 4, 30, "byte4int"),
            new ProtocolProperty("消费金额", 4, 34, "byte4int"),
            new ProtocolProperty("充电电量", 4, 38, "byte4int"),
            new ProtocolProperty("尖时段电量", 4, 42, "byte4int"),
            new ProtocolProperty("峰时段电量", 4, 46, "byte4int"),
            new ProtocolProperty("平时段电量", 4, 50, "byte4int"),
            new ProtocolProperty("谷时段电量", 4, 54, "byte4int"),
            new ProtocolProperty("累计充电时间", 2, 58, "byte2int"),
            new ProtocolProperty("剩余时间", 2, 60, "byte2int"),
            new ProtocolProperty("充电功率", 2, 62, "byte2int"),
            new ProtocolProperty("充电电压", 2, 64, "byte2int"),
            new ProtocolProperty("充电电流", 2, 66, "byte2int"),
            new ProtocolProperty("充电枪正极温度", 2, 68, "byte2int"),
            new ProtocolProperty("充电枪负极温度", 2, 70, "byte2int")
    );


}
