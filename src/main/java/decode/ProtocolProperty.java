package decode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProtocolProperty {
    private String field;
    private Integer length;
    private Integer index;
    private String method;
}
