package enums;

import lombok.Getter;

/**
 * @author zengwq
 */
@Getter
public enum RedisHashNameEnum {

    /************************************************   数仓维表数据start  ************************************************/


    /************************************************   数仓维表数据end  ************************************************/



    /************************************************   平台实时数据  start  ************************************************/
    GUN_CMD_REAL_TIME_ORDER_PROCCESING("gun:cmd:real-time-order-proccesing", "gun:cmd:real-time-order-proccesing:%s", "枪实时订单数据"),

    // %s = gunCode(枪号)  value = JSONObject.toJSONString(DCBmsVO)
    GUN_CMD_DC_REAL_TIME_BMS_DATA("gun:cmd:dc-real-time-bms-data-three", "gun:cmd:dc-real-time-bms-data:%s", "直流枪订单实时BMS数据"),
    //%s = chargingNo(订单号)  key = gun:cmd:real-time-order-charge-quantity:202101151610304350589 value = 1000
    GUN_CMD_REAL_TIME_ORDER_CHARGE_QUANTITY("gun:cmd:real-time-order-charge-quantity", "gun:cmd:real-time-order-charge-quantity:%s", "订单实时充电量数据"),
    GUN_CMD_REAL_TIME_STATE("gun:cmd:real-time-state", "gun:cmd:real-time-state:%s", "枪实时状态数据"),
    // 数据格式（枪号：启动充电参数）
    PC_CMD_INVOKE_MACHINE_TO_CHARGE("pc:cmd:invoke-machine-to-charge", "pc:cmd:invoke-machine-to-charge:%s", "桩启动充电订单数据"),
    // 数据格式（桩号：启动充电参数）
    PILE_CMD_REAL_TIME_STATE("pile:cmd:real-time-state", "pile:cmd:real-time-state:%s", "桩实时状态"),

    //    GUN_CMD_DC_REAL_TIME_BMS_DATA_TWO("gun:cmd:dc-real-time-bms-data-two", "直流枪订单实时BMS数据2Hash名称"),
    GUN_REAL_TIME_DATA2_SINGLE_POWER_BATTERY("gun:cmd:dc-real-time-bms-data-two", "singlePowerBattery:%s", "单体动力蓄电池状态"),
    GUN_REAL_TIME_DATA2_VEHICLE_POWER_BATTERY("gun:cmd:dc-real-time-bms-data-two", "vehiclePowerBattery:%s", "整车动力蓄电池荷电状态"),
    GUN_REAL_TIME_DATA2_POWER_BATTERY_CHARGING("gun:cmd:dc-real-time-bms-data-two", "powerBatteryCharging:%s", "动力蓄电池充电过电流状态"),
    GUN_REAL_TIME_DATA2_POWER_BATTERY_TEMPERATURE("gun:cmd:dc-real-time-bms-data-two", "powerBatteryTemperature:%s", "动力蓄电池温度状态"),
    GUN_REAL_TIME_DATA2_POWER_BATTERY_INSULATION("gun:cmd:dc-real-time-bms-data-two", "powerBatteryInsulation:%s", "动力蓄电池绝缘状态"),
    GUN_REAL_TIME_DATA2_POWER_BATTERY_OUTPUT_CONNECTOR("gun:cmd:dc-real-time-bms-data-two", "powerBatteryOutputConnector:%s", "动力蓄电池输出连接器连接状态"),
    GUN_REAL_TIME_DATA2_CHARGING_PERMISSION("gun:cmd:dc-real-time-bms-data-two", "chargingPermission:%s", "充电允许状态"),


    // 数据格式（桩号：登录信息）
    GUN_LOGIN("gun:login", "gun:login:%s", "桩登录数据"),
    PILE_MACHINE_STATE("pile:machine-state", "pile:machine-state:%s", "桩设备状态数据Hash名称"),
    //    PILE_MACHINE_COOL_STATE("pile:machine-cool-state", "液冷设备状态数据Hash名称"),
//    PILE_WEB_SOCKET("pile:web-socket", "桩webSocket数据Hash名称"),
//    PILE_FISSION_REAL_TIME_DATA("pile:fission_real_time_data", "分体桩主机实时数据数据Hash名称"),
//
    PILE_DISABLED_CONNECT("pile:disabled-connect", "pile:disabled-connect:%s", "桩连接禁用数据Hash名称"),
    //%s = gunCode(枪号)  key = gun:state-change-sett-order:900007172375500101001 value = 1
    GUN_STATE_CHANGE_SETT_ORDER("gun:state-change-sett-order", "gun:state-change-sett-order:%s", "枪状态变更定时结算订单计时器"),
    //
//    GUN_GROUND_LOCK("gun:ground-lock", "枪地锁数据Hash名称"),
//    GUN_GROUND_LOCK_RECORD("gun:ground-lock-record", "枪地锁记录数据Hash名称"),
//    GUN_GROUND_LOCK_ORDER("gun:ground-lock-order", "枪地锁订单数据Hash名称"),
//    GUN_GROUND_LOCK_STATUS("gun:ground-lock-status", "枪地锁状态数据Hash名称"),
//    GUN_GROUND_LOCK_CONTROL("gun:ground-lock-control", "枪地锁操作数据"),
//
//    GUN_APPOINT("gun:appoint", "枪预约数据Hash名称"),
//
    CAMERA_STATE("camera:state", "camera:state:%s", "摄像头状态"),

    MACHINE_STATE("machine_state", "%s", ""),

//    /************************************************   平台实时数据  end    ************************************************/

    /************************************************   平台充电流程  start  ************************************************/
    // 数据格式（地锁订单: 充电订单）
    GROUND_LOCK_ORDER_RELATION("GroundLockOrderRelation", "%s", "地锁订单号与充电订单号对应关系"),
    // 数据格式（枪号: 充电订单）
    GROUND_LOCK_ORDER("GroundLockOrder", "%s", "充电枪对应的订单"),
    // 数据格式（订单号: 下发指令结果）
    SEND_CABINET_RESULT("sendCabinetResult", "%s", "我要没看懂存的啥"),
    // 数据格式（枪号: 启动充电参数cmd7）
    CMD7("cmd_7", "%s", "下发开启充电指令"),

    START_CHARGE_MESSAGE("startChargeMessage", "%s", ""),
    /************************************************   平台充电流程  end    ************************************************/

//
//    /************************************************   平台基础数据  start  ************************************************/
//    RATE_BIKE("rate:bike", "电单车协议费率"),
//    RATE_CAR("rate:car", "汽车协议费率"),
//    RATE_TIME_SHARING("rate:time-sharing", "汽车分时收费规则"),
//
//    PILE_ID_CODE("pile:id-code", "pile:id-code:%s", "桩Id-桩Code关系"),
//    STATION_ID_CODE("station:id-code", "站Id-站Code关系"),
//
//    OPERATOR_STATION("operator-station", "运营商-站点关系"),
//    STATION_PILE("station-pile", "站点-桩关系"),
//    PILE_GUN("pile-gun", "桩-枪关系"),

    GATE_NO_SHUT("gateNoShut", "%s", "一个看不懂的地方拿过来的，我也不知道有什么用"),
    // %s 表示订单号
    OPENID_FORMID("openid_formId", "%s", "小程序消息formId"),
    // %s 表示订单号
    OPENID_USERID("openid_userId", "%s", "小程序消息userId"),

    /************************************************   平台基础数据  end    ************************************************/


    /************************************************   移动端  start  ************************************************/
    // key = 手机号; value = token
    CUSTOMER_IOS_DEVICE_TOKEN("customer:phone", "customer-ios-device-token:%s", "用户ios设备token"),
    CUSTOMER_ANDROID_DEVICE_TOKEN("customer:phone", "customer-android-device-token:%s", "用户安卓设备token"),
    ANDROID_DEVICE_TOKEN("customer:phone", "customer-android-device-token:%s", "用户安卓设备token"),
    IOS_DEVICE_TOKEN("customer:phone", "customer-ios-device-token:%s", "用户安卓设备token"),

//    DEVICE_INFO("device:info", "ac:pile:device:info:%s", "交流桩设备信息"),
    /************************************************   移动端  end    ************************************************/


    /************************************************   异常  start  ************************************************/
    RCD_EXCEPTION("rcd-exception", "%s", "润诚达汽车异常数据"),
    BIKE_EXCEPTION("bike-exception", "%s", "单车异常数据"),
    HJL_EXCEPTION("hjl-exception", "%s", "宏嘉利异常数据"),
    /************************************************   异常  end    ************************************************/
    ;


    private String key;
    private String hashNamePrefix;
    private String desc;

    RedisHashNameEnum(String key, String hashNamePrefix, String desc) {
        this.key = key;
        this.hashNamePrefix = hashNamePrefix;
        this.desc = desc;
    }
}
