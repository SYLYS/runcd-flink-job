package enums;

import lombok.Getter;

/**
 * @author xiaokun
 * @date 2023/4/20 17:35:34
 */
@Getter
public enum NotifyGroupEnum {
    /**
     *
     */
    PILE_STATE_OFFLINE("桩设备离线告警"),
    GUN_ORDER_REPEAT_UPLOAD("枪订单重复上传告警"),
    ;

    private final String name;

    NotifyGroupEnum(String name) {
        this.name = name;
    }
}
