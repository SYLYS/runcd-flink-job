package enums;

import lombok.Getter;

/**
 * redis key前缀
 *
 * @author zengwq
 */
@Getter
public enum RedisKeyEnum {

    /************************************************   资源信息 start  ************************************************/
    DIM_OPERATOR_DETAIL_DF("dim:operator:detail:%s", "运营商全量维度数据"),
    DIM_STATION_DETAIL_DF("dim:station:detail:%s", "站点全量维度数据"),
    DIM_STATION_ID_DETAIL_DF("dim:station:id:detail:%s", "站点全量维度数据"),
    DIM_PILE_DETAIL_DF("dim:pile:detail:%s", "充电桩全量维度数据"),
    DIM_GUN_DETAIL_DF("dim:gun:detail:%s", "充电枪全量维度数据"),
    DIM_DATE_DETAIL_DF("dim:date:detail:%s", "时间全量维度数据"),
    DIM_ADDRESS_DETAIL_DF("dim:address:detail:%s", "地址全量维度数据"),
    DIM_PILE_TYPE_DETAIL_DF("dim:pile:type:detail:%s", "设备型号全量维度数据"),

    /************************************************   资源信息 end  ************************************************/


    /************************************************   订单统计key  start  ************************************************/
    //运营商订单数据
    OPERATOR_ORDER_TOTAL("operator:order:total:%s:%s", "运营商订单历史年总数据"),
    OPERATOR_ORDER_YEAR_NOW("operator:order:year:%s:%s", "运营商订单年数据"),
    OPERATOR_ORDER_MONTH_NOW("operator:order:month:%s:%s", "运营商订单月数据"),
    OPERATOR_ORDER_DAY_NOW("operator:order:day:%s:%s", "运营商订单日数据"),

    OPERATOR_ORDER_DAY_MONTH("operator:order:day:month:%s", "运营商订单日数据(最近一月)"),

    //运营商站点订单数据
    OPERATOR_STATION_ORDER_TOTAL("operator:station:order:total:%s:%s", "运营商OrderStatisticalVO站点订单历史年总数据"),
    OPERATOR_STATION_ORDER_YEAR_NOW("operator:station:order:year:%s:%s:%s", "运营商站点订单本年数据"),
    OPERATOR_STATION_STATION_ORDER_MONTH_NOW("operator:station:order:month:%s:%s:%s", "运营商站点订单本月数据"),
    OPERATOR_STATION_STATION_ORDER_DAY_NOW("operator:station:order:day:%s:%s:%s", "运营商站点订单本日数据"),

    OPERATOR_STATION_STATION_ORDER_DAY_MONTH("operator:station:order:day:month:%s:%s", "运营商站点订单日数据(最近一月)"),

    //第三方订单数据
    OPERATOR_COMPANY_ORDER_TOTAL("operator:company:order:total:%s:%s", "第三方订单历史年总数据"),
    OPERATOR_COMPANY_ORDER_YEAR_NOW("operator:company:order:year:%s:%s:%s", "第三方订单本年数据"),
    OPERATOR_COMPANY_ORDER_MONTH_NOW("operator:company:order:month:%s:%s:%s", "第三方订单本月数据"),
    OPERATOR_COMPANY_ORDER_DAY_NOW("operator:company:order:day:%s:%s:%s", "第三方订单本日数据"),

    OPERATOR_COMPANY_ORDER_DAY_MONTH("operator:company:order:day:month:%s:%s", "第三方订单日数据(最近一月)");

    /************************************************   订单统计key  end  ************************************************/

    private final String keyPrefix;
    private final String desc;

    RedisKeyEnum(String keyPrefix, String desc) {
        this.keyPrefix = keyPrefix;
        this.desc = desc;
    }
}
