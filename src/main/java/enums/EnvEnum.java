package enums;

import java.util.Arrays;

/**
 * @author xiaokun
 * @date 2023/4/20 17:28:21
 */
public enum EnvEnum {
    DEV("dev"),
    TEST("test"),
    UAT("uat"),
    PROD("prod"),
    ;

    private final String profile;

    EnvEnum(String profile) {
        this.profile = profile;
    }

    @Override
    public String toString() {
        return this.profile;
    }


    /**
     * 未找到默认Test
     */
    public static EnvEnum getEnum(String profile) {
        return Arrays.stream(EnvEnum.values()).filter(e -> e.toString().equals(profile)).findFirst().orElse(TEST);
    }
}
