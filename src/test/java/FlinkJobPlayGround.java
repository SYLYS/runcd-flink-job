import com.alibaba.fastjson.JSONObject;
import decode.ProtocolProperty;
import utli.DecodeUtil;
import utli.EncodeUtil;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FlinkJobPlayGround {

    @SuppressWarnings("all")
    public static void main(String[] args) {
        直流订单();
        交流订单();
    }

    private static void 心跳() {
        String code = "";

    }

    private static void 直流订单() {
        String code = "68 01 0D 35 32 34 33 30 30 30 31 30 31 30 37 30 32 31 30 02 01 02 40 00 80 4D 41 30 30 35 44 42 57 31 32 33 30 34 31 38 31 35 31 31 37 37 32 30 33 31 37 36 00 00 00 00 00 4D 41 30 30 35 44 42 57 31 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 07 29 00 00 2E 36 0B 01 00 00 00 0D 7D 0B 0F 12 04 17 CE D9 37 0F 12 04 17 00 2B 00 00 2E 36 00 00 00 00 00 00 00 00 00 00 00 00 00 00 06 B3 00 00 00 76 21 5F 00 01 7A 71 F4 01 7A A0 2A B9 C4";
        byte[] bytes = EncodeUtil.hexStringToByteArray(code);
        List<ProtocolProperty> protocolPropertyList = Arrays.asList(
                new ProtocolProperty("start", 2, 0, "byte2int"),
                new ProtocolProperty("verson", 1, 2, "byte1int"),
                new ProtocolProperty("充电枪序号", 16, 3, "String"),
                new ProtocolProperty("充电枪类型", 1, 19, "byte1int"),
                new ProtocolProperty("充电枪序号", 1, 20, "byte1int"),
                new ProtocolProperty("帧类型标识", 2, 21, "byte2int"),
                new ProtocolProperty("数据区域长度", 2, 23, "byte2int"),
                new ProtocolProperty("业务流水号", 32, 25, "String"),
                new ProtocolProperty("用户账户", 32, 57, "String"),
                new ProtocolProperty("消费总金额", 4, 89, "byte4int"),
                new ProtocolProperty("充电电量", 4, 91 + 2, "byte4int"),
                new ProtocolProperty("充电停止原因", 1, 95 + 2, "byte1int"),
                new ProtocolProperty("交易方式", 1, 96 + 2, "byte1int"),
                new ProtocolProperty("充电类型", 1, 97 + 2, "byte1int"),
                new ProtocolProperty("充电参数", 2, 98 + 2, "byte2int"),
                new ProtocolProperty("开始充电时间", 7, 102, "timeScale"),
                new ProtocolProperty("结束充电时间", 7, 109, "timeScale"),
                new ProtocolProperty("充电时长", 2, 116, "byte2int"),
                new ProtocolProperty("尖时段电量", 4, 118, "byte4int"),
                new ProtocolProperty("峰时段电量", 4, 122, "byte4int"),
                new ProtocolProperty("平时段电量", 4, 126, "byte4int"),
                new ProtocolProperty("谷时段电量", 4, 130, "byte4int"),
                new ProtocolProperty("电费总金额", 4, 134, "byte4int"),
                new ProtocolProperty("服务费总金额", 4, 138, "byte4int"),
                new ProtocolProperty("充电前SOC", 1, 142, "byte1int"),
                new ProtocolProperty("充电后SOC", 1, 143, "byte1int"),
                new ProtocolProperty("BMS电源类型", 1, 144, "byte1int"),
                new ProtocolProperty("充电前电表度数", 4, 145, "byte4int"),
                new ProtocolProperty("充电后电表度数", 4, 149, "byte4int")
        );


        Map<String, Object> stringObjectMap = protocol2Map(protocolPropertyList, bytes);
        System.out.println(JSONObject.toJSON(stringObjectMap));
    }

    private static void 交流订单() {
        String code = "68 01 18 35 32 34 33 30 30 30 31 30 31 30 37 30 30 30 31 01 01 01 40 00 75 33 38 32 30 32 33 30 34 31 39 31 36 38 31 38 35 38 31 30 30 31 34 34 32 30 34 34 38 36 30 32 34 31 33 35 30 31 35 35 34 31 38 30 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 02 25 00 00 11 B8 11 01 02 00 C8 60 6D B0 06 13 04 17 38 4A 2E 08 13 04 17 00 75 00 00 00 00 00 00 00 00 00 00 03 59 00 00 0E 5F 00 00 00 BA 00 00 01 6B 1A E4";
        byte[] bytes = EncodeUtil.hexStringToByteArray(code);

        List<ProtocolProperty> protocolPropertyList = Arrays.asList(
                new ProtocolProperty("start", 2, 0, "byte2int"),
                new ProtocolProperty("verson", 1, 2, "byte1int"),
                new ProtocolProperty("充电枪序号", 16, 3, "String"),
                new ProtocolProperty("充电枪类型", 1, 19, "byte1int"),
                new ProtocolProperty("充电枪序号", 1, 20, "byte1int"),
                new ProtocolProperty("帧类型标识", 2, 21, "byte2int"),
                new ProtocolProperty("数据区域长度", 2, 23, "byte2int"),
                new ProtocolProperty("业务流水号", 32, 25, "String"),
                new ProtocolProperty("用户账户", 32, 57, "String"),
                new ProtocolProperty("消费总金额", 4, 89, "byte4int"),
                new ProtocolProperty("充电电量", 4, 91 + 2, "byte4int"),
                new ProtocolProperty("充电停止原因", 1, 95 + 2, "byte1int"),
                new ProtocolProperty("交易方式", 1, 96 + 2, "byte1int"),
                new ProtocolProperty("充电类型", 1, 97 + 2, "byte1int"),
                new ProtocolProperty("充电参数", 2, 98 + 2, "byte2int"),
                new ProtocolProperty("开始充电时间", 7, 102, "timeScale"),
                new ProtocolProperty("结束充电时间", 7, 109, "timeScale"),
                new ProtocolProperty("充电时长", 2, 116, "byte2int"),
                new ProtocolProperty("尖时段电量", 4, 118, "byte4int"),
                new ProtocolProperty("峰时段电量", 4, 122, "byte4int"),
                new ProtocolProperty("平时段电量", 4, 126, "byte4int"),
                new ProtocolProperty("谷时段电量", 4, 130, "byte4int"),
                new ProtocolProperty("电费总金额", 4, 134, "byte4int"),
                new ProtocolProperty("服务费总金额", 4, 138, "byte4int")
//                new ProtocolProperty("充电前电表度数", 4, 142, "byte4int")
//                new ProtocolProperty("充电后电表度数", 4, 146, "byte4int")
        );
        Map<String, Object> stringObjectMap = protocol2Map(protocolPropertyList, bytes);
        System.out.println(JSONObject.toJSON(stringObjectMap));
    }


    private static Map<String, Object> protocol2Map(List<ProtocolProperty> protocolProperties, byte[] bytes) {
        Map<String, Object> result = new HashMap<>();

        protocolProperties.forEach(p -> {
            Object byte2int = decode(bytes, p.getIndex(), p.getLength(), p.getMethod());
            result.put(p.getField(), byte2int);
        });
        return result;
    }


    private static Object decode(byte[] bytes, Integer index, Integer length, String method) {
        byte[] bytes19 = new byte[length];
        System.arraycopy(bytes, index, bytes19, 0, length);
        if (method == null) {
            return new String(bytes19).trim();
        }

        return DecodeUtil.decodeMap.get(method).apply(bytes19);
    }


}
